<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BoardMembers;

/**
 * BoardMembersSearch represents the model behind the search form about `app\models\BoardMembers`.
 */
class BoardMembersSearch extends BoardMembers
{
    public function rules()
    {
        return [
            [['board_members_id', 'school_board_id', 'user_id', 'is_active'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = BoardMembers::find();
        $query->andWhere($condition);
 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'board_members_id' => $this->board_members_id,
            'school_board_id' => $this->school_board_id,
            'user_id' => $this->user_id,
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
