<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_classes".
 *
 * @property integer $sc_id
 * @property integer $cy_id
 * @property string $primary_student_year_of_study_id
 * @property integer $class_year_of_study_number
 * @property integer $student_class_status
 *
 * @property PrimaryStudentYearOfStudy $primaryStudentYearOfStudy
 * @property ClassYear $cy
 */
class StudentClasses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_classes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cy_id', 'primary_student_year_of_study_id', 'class_year_of_study_number', 'student_class_status'], 'required'],
            [['cy_id', 'primary_student_year_of_study_id', 'class_year_of_study_number', 'student_class_status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sc_id' => 'Sc ID',
            'cy_id' => 'Cy ID',
            'primary_student_year_of_study_id' => 'Primary Student Year Of Study ID',
            'class_year_of_study_number' => 'Class Year Of Study Number',
            'student_class_status' => 'Student Class Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrimaryStudentYearOfStudy()
    {
        return $this->hasOne(PrimaryStudentYearOfStudy::className(), ['primary_student_year_of_study_id' => 'primary_student_year_of_study_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCy()
    {
        return $this->hasOne(ClassYear::className(), ['cy_id' => 'cy_id']);
    }
}
