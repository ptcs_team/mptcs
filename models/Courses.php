<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property integer $course_id
 * @property string $course_code
 * @property string $course_title
 * @property integer $school_type_id
 * @property integer $is_active
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * 
     * 
     */
    
   public $course_offering_id;
    public $course_and_title;
    
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_title', 'school_type_id', 'is_active'], 'required'],
            [['school_type_id', 'is_active'], 'integer'],
            [['course_code', 'course_title'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'course_id' => 'Course',
            'course_code' => 'Course Code',
            'course_title' => 'Course Title',
            'school_type_id' => 'School Type',
            'is_active' => 'Is Active',
        ];
    }
    
    

    
    
       public static function getCurrentVersionSecondaryCourses($school_id,$fy_id) {
    $school_type_id = Schools::findOne($school_id)->school_type_id;
 $query = " SELECT courses.course_code AS course_code, courses.course_title AS course_title,CONCAT(ifnull(courses.course_code,''),'-',courses.course_title) AS course_and_title,course_offering.course_offering_id AS course_offering_id  FROM courses INNER JOIN course_offering 
ON course_offering.course_id = courses.course_id INNER JOIN course_offering_versions
ON course_offering_versions.course_offering_version_id = course_offering.course_offering_version_id
WHERE course_offering_versions.status = 1 AND course_offering_versions.school_type_id = {$school_type_id} AND course_offering.admistered_by = {$school_id} AND course_offering.course_offering_id NOT IN (SELECT course_offering_id FROM form_year_of_study_courses WHERE school_id = {$school_id}  AND fy_id = {$fy_id})";
        return static::findBySql($query)->all();
    }
    
     
 public static function getCurrentVersionPrimaryCourses($school_id,$cy_id) {
    $school_type_id = Schools::findOne($school_id)->school_type_id;
 $query = " SELECT courses.course_code AS course_code, courses.course_title AS course_title,CONCAT(ifnull(courses.course_code,''),'-',courses.course_title) AS course_and_title,course_offering.course_offering_id AS course_offering_id  FROM courses INNER JOIN course_offering 
ON course_offering.course_id = courses.course_id INNER JOIN course_offering_versions
ON course_offering_versions.course_offering_version_id = course_offering.course_offering_version_id
WHERE course_offering_versions.status = 1 AND course_offering_versions.school_type_id = {$school_type_id} AND course_offering.admistered_by = {$school_id} AND course_offering.course_offering_id NOT IN (SELECT course_offering_id FROM class_year_of_study_courses WHERE school_id = {$school_id}  AND cy_id = {$cy_id})";
        return static::findBySql($query)->all();
    }
    
    
    
    
    
}
