<?php

namespace app\models;

use Yii;
use kartik\password\StrengthValidator;

/**
 * This is the model class for table "users".
 *
 * @property string $user_id
 * @property integer $student_status_id
 * @property string $registration_number
 * @property string $surname
 * @property string $firstname
 * @property string $middlename
 * @property integer $pintake_id
 * @property string $sex
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $mailing_address
 * @property string $telephone_no
 * @property string $email_address
 * @property string $admission_date
 * @property string $sponsorship_type
 * @property string $status_comments
 * @property string $completion_date
 * @property string $username
 * @property string $password
 * @property integer $is_active
 * @property integer $login_counts
 * @property string $last_login
 * @property integer $user_type
 * @property integer $staff_position_id
 * @property string $salutation
 *
 * @property StudentYearOfStudy[] $studentYearOfStudies
 */
class PrimaryStudents extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
  
    public $image;
    public $academic_year;
    public $year_of_study;
    public $repeatnewpass, $newpass,$passwdrepeat;

    public static function tableName() {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
             [['student_status_id', 'is_active', 'login_counts', 'user_type', 'staff_position_id', 'pintake_id', 'session_id', 'school_id'], 'integer'],
            [['completion_date', 'last_login', 'newpass'], 'safe'],
            [['username', 'login_counts', 'last_login', 'newpass', 'repeatnewpass', 'firstname', 'surname', 'sex', 'telephone_no', 'email_address', 'password', 'newpass', 'passwdrepeat', 'user_type','school_id','registration_number','academic_year','year_of_study'], 'required'],
            [['registration_number', 'date_of_birth', 'admission_date', 'status_comments', 'password'], 'string', 'max' => 255],
            [['surname', 'firstname', 'middlename', 'salutation'], 'string', 'max' => 20],
            [['sex', 'sponsorship_type'], 'string', 'max' => 1],
            [['place_of_birth'], 'string', 'max' => 50],
            [['mailing_address', 'telephone_no'], 'string', 'max' => 150],
            [['email_address'], 'string', 'max' => 100],
            [['username', 'student_id'], 'string', 'max' => 200],
            [['username'], 'unique'],
            [['registration_number', 'email_address', 'telephone_no'], 'unique'],
            ['email_address', 'unique', 'message' => 'This email address has already been taken.'],
            [['email_address'], 'email'],
            ['username', 'unique', 'message' => 'This username has already been taken.'],
            //['image', 'file'],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
            [['passwdrepeat'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            [['password'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            [['newpass'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            ['repeatnewpass', 'compare', 'compareAttribute' => 'newpass', 'message' => "Passwords don't match"],
            [['is_default_password'], 'boolean',],
            ['passwdrepeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
            ['captcha', 'captcha'],
        ];
    }

    public static function RegisterStudent($model, $school_id, $year_of_study, $academic_year_id) {
        $model->username = $model->registration_number;
        $model->school_id = $school_id;
        if (!$model->save(false)) {
            return $model;
        }

        $return = static::AddStudyYears($model->user_id, $school_id, $year_of_study, $academic_year_id);
        if ($return !== true) {
            $model->addError('registration_number', $return);
        }
        return $model;
    }

    public static function AddStudyYears($student_id, $school_id, $year_of_study, $academic_year_id) {
        $model_school = Schools::findOne($school_id);
        if ($model_school == NULL) {
            return "Invalid school";
        }
        $model_acy = AcademicYears::findOne($academic_year_id);
        if ($model_acy == NULL) {
            return "Invalid academic year";
        }
        //array index will represent the corresponding study years
        $acad_years = array();
        $acad_years[$year_of_study] = $academic_year_id;
        $no_years = 7;
        $lower_steps = $year_of_study - 1;
        $upper_steps = $no_years - $year_of_study;

        $lower_acad_years = AcademicYears::find()
                ->orderBy(" `order` DESC ")
                ->limit($lower_steps)
                ->where(" `order` < {$model_acy->order}")
                ->all();

        foreach ($lower_acad_years as $year) {
            $acad_years[$lower_steps] = $year->academic_year_id;
            --$lower_steps;
        }


        $upper_acad_years = AcademicYears::find()
                ->orderBy(" `order` ASC ")
                ->limit($upper_steps)
                ->where(" `order` > {$model_acy->order}")
                ->all();

        foreach ($upper_acad_years as $year) {
            ++$year_of_study;
            $acad_years[$year_of_study] = $year->academic_year_id;
        }

        foreach ($acad_years as $key => $value) {

            $model_cy = ClassYear::find()
                    ->where("school_id = {$school_id} AND academic_year_id = {$value} and study_year = {$key} ")
                    ->one();
            if ($model_cy == NULL)
                return " Class in " . AcademicYears::findOne($value)->academic_year . " "."is not configured yet";
               $cy_id = $model_cy->cy_id;

            $return = static::AddStudyYear($student_id, $cy_id,$school_id);

            if ($return !== true) {
                return $return;
            }
        }


        return true;
    }

    public static function AddStudyYear($student_id, $cy_id,$school_id) {

        if (PrimaryStudentYearOfStudy::find()->where("cy_id = {$cy_id} and user_id = {$student_id}")->count() > 0) {

            return "This class year already exist";
        }
        $classes = ClassYear::find()->where("cy_id = {$cy_id}  AND school_id = {$school_id}")->all();
        if (count($classes) == 0) {
            return "Class year not configured";
        }
        foreach ($classes as $class) {
            if (ClassYearOfStudyCourses::find()->where("cy_id = {$class->cy_id}")->count() == 0) {
                return "One or more classes dont have courses";
            }
        }

        $model_cy = ClassYear::findOne($cy_id);
        $model_sy = new PrimaryStudentYearOfStudy();
        $model_sy->user_id = $student_id;
        $model_sy->cy_id = $model_cy->cy_id;
        $model_sy->year_of_study = $model_cy->study_year;
        $model_sy->student_year_status_id = 1;
        if (!$model_sy->save(false)) {
            return "Error saving student year of study";
        }
        
           foreach ($classes as $class) {
            $model_syc = new StudentClasses();
            $model_syc->cy_id = $class->cy_id;
            $model_syc->primary_student_year_of_study_id = $model_sy->primary_student_year_of_study_id;
            $model_syc->class_year_of_study_number = $class->year_number;
            $model_syc->student_class_status = 1;
            if (!$model_syc->save(false)) {
                return "Error saving student Classes";
            }
            
            $ccourses = ClassYearOfStudyCourses::find()->where("is_core = 1 AND cy_id = {$class->cy_id}  AND school_id = {$school_id}")->all();
            foreach ($ccourses as $ccourse) {
                $model_sycc = new StudentClassCourses();
                $model_sycc->sc_id = $model_syc->sc_id;
                $model_sycc->cy_id = $ccourse->cy_id;
                $model_sycc->cyos_course_id = $ccourse->cyos_course_id;
                $model_sycc->student_class_course_status_id = 1; //set default semester course status to Inprogress
                if (!$model_sycc->save(false)) {
                    return "#5: Error saving student class courses";
                }
            }
        
           }
        return true;
    }

    /**
     * This function adds all the study years of a student in a given in a given programme
     * 
     * @param type $student_id
     * @param type $pintake_id
     * @param type $year_of_study
     * @param type $academic_year_id
     * @return boolean true if all the study years are successfully added, otherwise an error message string is returned
     */

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
             'user_id' => 'User ID',
            'student_status_id' => 'Student Status ID',
            'registration_number' => 'Registration Number',
            'surname' => 'Surname',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'pintake_id' => 'Programme Intake',
            'session_id' => 'Programme Session',
            'sex' => 'Sex',
            'profile_picture' => 'Profile Picture',
            'date_of_birth' => 'Date Of Birth',
            'place_of_birth' => 'Place Of Birth',
            'mailing_address' => 'Mailing Address',
            'telephone_no' => 'Telephone No',
            'email_address' => 'Email Address',
            'admission_date' => 'Admission Date',
            'sponsorship_type' => 'Sponsorship Type',
            'status_comments' => 'Status Comments',
            'completion_date' => 'Completion Date',
            'username' => 'Username',
            'password' => 'Password',
            'is_default_password' => 'is_default_password',
            'is_active' => 'Is Active',
            'login_counts' => 'Login Counts',
            'last_login' => 'Last Login',
            'user_type' => 'User Type',
            'staff_position_id' => 'Staff Position',
            'salutation' => 'Salutation',
            'last_password_update_date' => 'Last Password Update Date',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'passwdrepeat' => 'Re-Type Password',
            'school_id' => 'School',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentYearOfStudies() {
        return $this->hasMany(StudentYearOfStudy::className(), ['user_id' => 'user_id']);
    }


    public static function getStudentSemesterGPA($ss_id) {
        $credit_value_sum = 0;
        $sum_product_points_credit = 0;
        $py_semester_id = StudentSemesters::findOne($ss_id)->programme_year_semester_id;
        $ssc_model = StudentSemesterCourses::find()->where("ss_id={$ss_id}")->all();
        foreach ($ssc_model AS $ssc_model_) {
            $grade_point = GradingSystem::findOne($ssc_model_->grade_scored)->grade_points_formula;
            $credit_value = ProgrammeYearSemesterCourses::find()->where("py_semester_course_id={$ssc_model_->py_semester_course_id} and py_semester_id={$py_semester_id}")->one()->units;
            $sum_product_points_credit+=($grade_point * $credit_value);
            $credit_value_sum+=$credit_value;
        }
        $GPA = $sum_product_points_credit / $credit_value_sum;
        $semester_gpa = round($GPA, 1, PHP_ROUND_HALF_DOWN);
        return $semester_gpa;
    }

    public static function getStudentYearGPA($student_year_of_study_id) {
        $credit_value_sum = 0;
        $sum_product_points_credit = 0;
        $student_semesters = StudentSemesters::find()->where("student_year_of_study_id={$student_year_of_study_id}")->all();
        foreach ($student_semesters AS $student_semester) {
            $ssc_model = StudentSemesterCourses::find()->where("ss_id={$student_semester->ss_id}")->all();
            foreach ($ssc_model AS $ssc_model_) {
                $grade_point = GradingSystem::findOne($ssc_model_->grade_scored)->grade_points_formula;
                $credit_value = ProgrammeYearSemesterCourses::find()->where("py_semester_course_id={$ssc_model_->py_semester_course_id}")->one()->units;
                $sum_product_points_credit+=($grade_point * $credit_value);
                $credit_value_sum+=$credit_value;
            }
        }
        $GPA = $sum_product_points_credit / $credit_value_sum;
        $year_gpa = round($GPA, 1, PHP_ROUND_HALF_DOWN);
        return $year_gpa;
    }

    public static function getStudentGPA($user_id) {
        $credit_value_sum = 0;
        $sum_product_points_credit = 0;
        $student_year_of_study_model = StudentYearOfStudy::find()->where("user_id={$user_id}")->all();
        foreach ($student_year_of_study_model AS $student_year_of_study_model_) {
            $student_semesters_model = StudentSemesters::find()->where("student_year_of_study_id={$student_year_of_study_model_->student_year_of_study_id}")->all();
            foreach ($student_semesters_model AS $student_semesters_model_) {
                $ssc_model = StudentSemesterCourses::find()->where("ss_id={$student_semesters_model_->ss_id}")->all();
                foreach ($ssc_model AS $ssc_model_) {
                    $grade_point = GradingSystem::findOne($ssc_model_->grade_scored)->grade_points_formula;
                    $credit_value = ProgrammeYearSemesterCourses::find()->where("py_semester_course_id={$ssc_model_->py_semester_course_id}")->one()->units;
                    $sum_product_points_credit+=($grade_point * $credit_value);
                    $credit_value_sum+=$credit_value;
                }
            }
        }

        $GPA = $sum_product_points_credit / $credit_value_sum;
        $student_gpa = round($GPA, 1, PHP_ROUND_HALF_DOWN);
        return $student_gpa;
    }

    public static function getStudentClassOfAward($programme_type, $overallGPA) {

        $query = " SELECT classification_award_id,classification_description,`order`
                    FROM classification_of_awards
                    WHERE programme_type_id = $programme_type
                    AND 
                    min_overallgpa <= $overallGPA AND max_overallgpa >= $overallGPA";

        $class_of_awards = ClassificationOfAwards::findBySql($query)->one();

        $class_of_award_description = $class_of_awards->classification_description;

        return $class_of_award_description;
    }
    
    
    
    public function getStudentStatus($student_status_id) {
        switch ($student_status_id){
            case 1:
                return 'InProgress';
                break;
            
            case 2:
                return 'Graduated';
                
            case 3:
                return 'Graduated';
                
            default:
                return '--';
                break;
        }
        
    }

}
