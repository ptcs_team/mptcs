<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentClasses;

/**
 * StudentClassesSearch represents the model behind the search form about `app\models\StudentClasses`.
 */
class StudentClassesSearch extends StudentClasses
{
    public function rules()
    {
        return [
            [['sc_id', 'cy_id', 'primary_student_year_of_study_id', 'class_year_of_study_number', 'student_class_status'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentClasses::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sc_id' => $this->sc_id,
            'cy_id' => $this->cy_id,
            'primary_student_year_of_study_id' => $this->primary_student_year_of_study_id,
            'class_year_of_study_number' => $this->class_year_of_study_number,
            'student_class_status' => $this->student_class_status,
        ]);

        return $dataProvider;
    }
}
