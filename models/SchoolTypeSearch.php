<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SchoolType;

/**
 * SchoolTypeSearch represents the model behind the search form about `app\models\SchoolType`.
 */
class SchoolTypeSearch extends SchoolType
{
    public function rules()
    {
        return [
            [['school_type_id', 'is_active'], 'integer'],
            [['school_type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = SchoolType::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_type_id' => $this->school_type_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'school_type', $this->school_type]);

        return $dataProvider;
    }
}
