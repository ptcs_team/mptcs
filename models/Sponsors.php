<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sponsors".
 *
 * @property integer $sponsor_id
 * @property string $sponsor_name
 * @property string $sponsor_description
 * @property string $address
 * @property string $e_mail
 * @property string $phone_no
 * @property string $fax
 * @property string $logo
 * @property integer $is_active
 */
class Sponsors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sponsors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sponsor_name', 'sponsor_description', 'address', 'e_mail', 'phone_no', 'is_active'], 'required'],
            [['sponsor_description'], 'string'],
            [['is_active'], 'integer'],
            [['sponsor_name', 'address', 'e_mail', 'phone_no', 'fax', 'logo'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sponsor_id' => 'Sponsor ID',
            'sponsor_name' => 'Sponsor Name',
            'sponsor_description' => 'Sponsor Description',
            'address' => 'Address',
            'e_mail' => 'E Mail',
            'phone_no' => 'Phone No',
            'fax' => 'Fax',
            'logo' => 'Logo',
            'is_active' => 'Is Active',
        ];
    }
}
