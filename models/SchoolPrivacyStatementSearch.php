<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SchoolPrivacyStatement;

/**
 * SchoolPrivacyStatementSearch represents the model behind the search form about `app\models\SchoolPrivacyStatement`.
 */
class SchoolPrivacyStatementSearch extends SchoolPrivacyStatement
{
    public function rules()
    {
        return [
            [['school_privacy_statement_id', 'school_id', 'created_by', 'is_active'], 'integer'],
            [['statement', 'date_created'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = SchoolPrivacyStatement::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_privacy_statement_id' => $this->school_privacy_statement_id,
            'school_id' => $this->school_id,
            'date_created' => $this->date_created,
            'created_by' => $this->created_by,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'statement', $this->statement]);

        return $dataProvider;
    }
}
