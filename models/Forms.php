<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forms".
 *
 * @property integer $form_id
 * @property string $form_name
 * @property string $form_acronym
 * @property integer $admistered_by
 * @property integer $is_active
 */
class Forms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_name', 'admistered_by', 'is_active'], 'required'],
            [['admistered_by', 'is_active'], 'integer'],
            [['form_name'], 'string', 'max' => 250],
            [['form_acronym'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'form_id' => 'Form ID',
            'form_name' => 'Form Name',
            'form_acronym' => 'Form Acronym',
            'admistered_by' => 'Admistered By',
            'is_active' => 'Is Active',
        ];
    }
}
