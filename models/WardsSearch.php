<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Wards;

/**
 * WardsSearch represents the model behind the search form about `app\models\Wards`.
 */
class WardsSearch extends Wards
{
    public function rules()
    {
        return [
            [['ward_id', 'district_id', 'is_active'], 'integer'],
            [['ward'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Wards::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ward_id' => $this->ward_id,
            'district_id' => $this->district_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'ward', $this->ward]);

        return $dataProvider;
    }
}
