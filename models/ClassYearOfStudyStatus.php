<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class_year_of_study_status".
 *
 * @property integer $class_year_of_study_status_id
 * @property string $status
 */
class ClassYearOfStudyStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_year_of_study_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_year_of_study_status_id' => 'Class Year Of Study Status ID',
            'status' => 'Status',
        ];
    }
}
