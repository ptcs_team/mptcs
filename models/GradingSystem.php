<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grading_system".
 *
 * @property integer $grading_system_id
 * @property string $grade
 * @property integer $order
 * @property double $min_score
 * @property double $max_score
 * @property string $definition
 * @property string $grade_points_formula
 * @property integer $grading_system_version_id
 *
 * @property GradingSystemVersions $gradingSystemVersion
 */
class GradingSystem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grading_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade', 'order', 'min_score', 'max_score', 'definition', 'grade_points_formula', 'grading_system_version_id'], 'required'],
            [['order', 'grading_system_version_id'], 'integer'],
            [['min_score', 'max_score'], 'number'],
            [['grade'], 'string', 'max' => 2],
            [['definition'], 'string', 'max' => 50],
            [['grade_points_formula'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grading_system_id' => 'Grading System ID',
            'grade' => 'Grade',
            'order' => 'Order',
            'min_score' => 'Min Score',
            'max_score' => 'Max Score',
            'definition' => 'Definition',
            'grade_points_formula' => 'Grade Points Formula',
            'grading_system_version_id' => 'Grading System Version ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradingSystemVersion()
    {
        return $this->hasOne(GradingSystemVersions::className(), ['grading_system_version_id' => 'grading_system_version_id']);
    }
}
