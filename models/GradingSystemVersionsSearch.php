<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GradingSystemVersions;

/**
 * GradingSystemVersionsSearch represents the model behind the search form about `app\models\GradingSystemVersions`.
 */
class GradingSystemVersionsSearch extends GradingSystemVersions {

    public function rules() {
        return [
            [['grading_system_version_id', 'school_type_id'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params) {
        $query = GradingSystemVersions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'grading_system_version_id' => $this->grading_system_version_id,
            'school_type_id' => $this->school_type_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }

}
