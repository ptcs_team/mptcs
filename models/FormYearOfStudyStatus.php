<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_year_of_study_status".
 *
 * @property integer $form_year_of_study_status_id
 * @property string $status
 *
 * @property FormYearOfStudy[] $formYearOfStudies
 */
class FormYearOfStudyStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_year_of_study_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'form_year_of_study_status_id' => 'Form Year Of Study Status ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormYearOfStudies()
    {
        return $this->hasMany(FormYearOfStudy::className(), ['fyos_status_id' => 'form_year_of_study_status_id']);
    }
}
