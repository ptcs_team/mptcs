<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Districts;

/**
 * DistrictsSearch represents the model behind the search form about `app\models\Districts`.
 */
class DistrictsSearch extends Districts
{
    public function rules()
    {
        return [
            [['district_id', 'region_id', 'is_active'], 'integer'],
            [['district'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Districts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'district_id' => $this->district_id,
            'region_id' => $this->region_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'district', $this->district]);

        return $dataProvider;
    }
}
