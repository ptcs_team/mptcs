<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "donors".
 *
 * @property integer $donor_id
 * @property string $donor_name
 * @property string $donor_description
 * @property string $address
 * @property string $e_mail
 * @property string $phone_no
 * @property string $fax
 * @property string $logo
 * @property integer $is_active
 */
class Donors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['donor_name', 'donor_description', 'address', 'e_mail', 'phone_no', 'is_active'], 'required'],
            [['donor_description'], 'string'],
            [['is_active'], 'integer'],
            [['donor_name', 'address', 'e_mail', 'phone_no', 'fax', 'logo'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'donor_id' => 'Donor ID',
            'donor_name' => 'Donor Name',
            'donor_description' => 'Donor Description',
            'address' => 'Address',
            'e_mail' => 'E Mail',
            'phone_no' => 'Phone No',
            'fax' => 'Fax',
            'logo' => 'Logo',
            'is_active' => 'Is Active',
        ];
    }
}
