<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AuthAssignment;

/**
 * AuthAssignmentSearch represents the model behind the search form about `app\models\AuthAssignment`.
 */
class AuthAssignmentSearch extends AuthAssignment {

    public function rules() {
        return [
            [['assignment_id', 'institution_structure_id', 'created_at'], 'integer'],
            [['item_name', 'user_id'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition) {
        $query = AuthAssignment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($condition != NULL) {
            $query->andWhere($condition);
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'assignment_id' => $this->assignment_id,
            'institution_structure_id' => $this->institution_structure_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
                ->andFilterWhere(['like', 'user_id', $this->user_id]);

        return $dataProvider;
    }

}
