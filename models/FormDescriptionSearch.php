<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FormDescription;

/**
 * FormDescriptionSearch represents the model behind the search form about `app\models\FormDescription`.
 */
class FormDescriptionSearch extends FormDescription
{
    public function rules()
    {
        return [
            [['form_description_id', 'form_number'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = FormDescription::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'form_description_id' => $this->form_description_id,
            'form_number' => $this->form_number,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
