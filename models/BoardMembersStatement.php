<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "board_members_statement".
 *
 * @property integer $board_members_statement_id
 * @property integer $school_board_id
 * @property string $statement
 * @property string $date_created
 * @property integer $is_active
 */
class BoardMembersStatement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'board_members_statement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_board_id', 'statement', 'is_active'], 'required'],
            [['school_board_id', 'is_active'], 'integer'],
            [['statement'], 'string'],
            [['date_created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'board_members_statement_id' => 'Board Members Statement ID',
            'school_board_id' => 'School Board ID',
            'statement' => 'Statement',
            'date_created' => 'Date Created',
            'is_active' => 'Is Active',
        ];
    }
}
