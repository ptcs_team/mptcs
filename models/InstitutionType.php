<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institution_type".
 *
 * @property integer $institution_type_id
 * @property string $institution_type_name
 * @property integer $order
 *
 * @property InstitutionStructure[] $institutionStructures
 */
class InstitutionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institution_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['institution_type_name', 'order'], 'required'],
            [['order'], 'integer'],
            [['institution_type_name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'institution_type_id' => 'Institution Type ',
            'institution_type_name' => 'Institution Type Name',
            'order' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionStructures()
    {
        return $this->hasMany(InstitutionStructure::className(), ['institution_type_id' => 'institution_type_id']);
    }
}
