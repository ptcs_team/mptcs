<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassYearOfStudyStatus;

/**
 * ClassYearOfStudyStatusSearch represents the model behind the search form about `app\models\ClassYearOfStudyStatus`.
 */
class ClassYearOfStudyStatusSearch extends ClassYearOfStudyStatus
{
    public function rules()
    {
        return [
            [['class_year_of_study_status_id'], 'integer'],
            [['status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ClassYearOfStudyStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'class_year_of_study_status_id' => $this->class_year_of_study_status_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
