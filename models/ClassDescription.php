<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class_description".
 *
 * @property integer $class_description_id
 * @property integer $class_number
 * @property string $description
 */
class ClassDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_number', 'description'], 'required'],
            [['class_number'], 'integer'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_description_id' => 'Class Description ID',
            'class_number' => 'Class Number',
            'description' => 'Description',
        ];
    }
    
    
       public static function getStudyYears($academic_year) {
        $data = self::findBySql(" SELECT class_number AS id, description as name  FROM  class_description  WHERE  class_number IN  ( SELECT study_year FROM class_year WHERE academic_year_id = {$academic_year}) ")->asArray()->all();
        $value = (count($data) == 0) ? ['' => ''] : $data;
        return $value;
    }
    
    
}
