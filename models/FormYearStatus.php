<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_year_status".
 *
 * @property integer $form_year_status_id
 * @property string $status
 */
class FormYearStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_year_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'form_year_status_id' => 'Form Year Status ID',
            'status' => 'Status',
        ];
    }
}
