<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "login_attempts".
 *
 * @property integer $login_attempts_id
 * @property integer $user_id
 * @property string $ip_address
 * @property integer $attempt
 * @property string $last_login
 */
class LoginAttempts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login_attempts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_attempts_id', 'user_id', 'ip_address'], 'required'],
            [['login_attempts_id', 'user_id', 'successfully_attempt'], 'integer'],
            [['last_login'], 'safe'],
            [['ip_address'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login_attempts_id' => 'Login Attempts ID',
            'user_id' => 'User ID',
            'ip_address' => 'Ip Address',
            'successfully_attempt' => 'successfully_attempt',
            'last_login' => 'Last Login',
        ];
    }
}
