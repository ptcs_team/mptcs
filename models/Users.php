<?php

namespace app\models;

use yii\helpers\Security;
use yii\web\IdentityInterface;
use Yii;
use kartik\password\StrengthValidator;

/**
 * This is the model class for table "users".
 *
 * @property string $user_id
 * @property integer $student_status_id
 * @property string $registration_number
 * @property string $surname
 * @property string $firstname
 * @property string $middlename
 * @property integer $ps_id
 * @property string $sex
 * @property string $profile_picture
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $mailing_address
 * @property string $telephone_no
 * @property string $email_address
 * @property string $admission_date
 * @property string $sponsorship_type
 * @property string $status_comments
 * @property string $completion_date
 * @property string $username
 * @property string $password
 * @property string $password_hash
 * @property integer $is_active
 * @property integer $login_counts
 * @property string $last_login
 * @property integer $user_type
 * @property integer $staff_position_id
 * @property string $salutation
 * @property string $last_password_update_date
 * @property string $auth_key
 * @property string $password_reset_token
 * @property string $created_at
 * @property integer $created_by
 *
 * @property StudentYearOfStudy[] $studentYearOfStudies
 * @property UserAuditTrail[] $userAuditTrails
 * @property UserLevels[] $userLevels
 */
class Users extends \yii\db\ActiveRecord implements IdentityInterface {

    public $my_details;
    public $re_password;
    public $student_id, $image;
    public $oldpass;
    public $repeatnewpass, $newpass, $passwdrepeat;
    public $captcha;

//    const SCENARIO_UPDATEIDENTIFICATION = 'updateidentification';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'users';
    }

//    public function scenarios() {
//        return [
//            self::SCENARIO_UPDATEIDENTIFICATION => ['sex', 'salutation', 'date_of_birth', 'place_of_birth', 'mailing_address', 'email_address', 'telephone_no'],
//        ];
//    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['student_status_id', 'is_active', 'login_counts', 'user_type', 'staff_position_id', 'pintake_id', 'session_id', 'school_id'], 'integer'],
            [['completion_date', 'last_login', 'newpass'], 'safe'],
            [['username', 'login_counts', 'last_login', 'newpass', 'repeatnewpass', 'firstname', 'surname', 'sex', 'telephone_no', 'email_address', 'password', 'newpass', 'passwdrepeat', 'user_type'], 'required'],
            [['registration_number', 'date_of_birth', 'admission_date', 'status_comments', 'password'], 'string', 'max' => 255],
            [['surname', 'firstname', 'middlename', 'salutation'], 'string', 'max' => 20],
            [['sex', 'sponsorship_type'], 'string', 'max' => 1],
            [['place_of_birth'], 'string', 'max' => 50],
            [['mailing_address', 'telephone_no'], 'string', 'max' => 150],
            [['email_address'], 'string', 'max' => 100],
            [['username', 'student_id'], 'string', 'max' => 200],
            [['username'], 'unique'],
            [['registration_number', 'email_address', 'telephone_no'], 'unique'],
            ['email_address', 'unique', 'message' => 'This email address has already been taken.'],
            [['email_address'], 'email'],
            ['username', 'unique', 'message' => 'This username has already been taken.'],
            //['image', 'file'],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
            [['passwdrepeat'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            [['password'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            [['newpass'], StrengthValidator::className(), 'preset' => 'normal', 'userAttribute' => 'username'],
            ['repeatnewpass', 'compare', 'compareAttribute' => 'newpass', 'message' => "Passwords don't match"],
            [['is_default_password'], 'boolean',],
            ['passwdrepeat', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
            ['captcha', 'captcha'],
//            [['sex', 'salutation', 'date_of_birth', 'place_of_birth', 'mailing_address', 'email_address', 'telephone_no'], 'required', 'on' => self::SCENARIO_UPDATEIDENTIFICATION],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'User ID',
            'student_status_id' => 'Student Status ID',
            'registration_number' => 'Registration Number',
            'surname' => 'Surname',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'pintake_id' => 'Programme Intake',
            'session_id' => 'Programme Session',
            'sex' => 'Sex',
            'profile_picture' => 'Profile Picture',
            'date_of_birth' => 'Date Of Birth',
            'place_of_birth' => 'Place Of Birth',
            'mailing_address' => 'Mailing Address',
            'telephone_no' => 'Telephone No',
            'email_address' => 'Email Address',
            'admission_date' => 'Admission Date',
            'sponsorship_type' => 'Sponsorship Type',
            'status_comments' => 'Status Comments',
            'completion_date' => 'Completion Date',
            'username' => 'Username',
            'password' => 'Password',
            'is_default_password' => 'is_default_password',
            'is_active' => 'Is Active',
            'login_counts' => 'Login Counts',
            'last_login' => 'Last Login',
            'user_type' => 'User Type',
            'staff_position_id' => 'Staff Position',
            'salutation' => 'Salutation',
            'last_password_update_date' => 'Last Password Update Date',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'passwdrepeat' => 'Re-Type Password',
            'school_id' => 'School',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentYearOfStudies() {
        return $this->hasMany(StudentYearOfStudy::className(), ['user_id' => 'user_id']);
    }

    /** INCLUDE USER LOGIN VALIDATION FUNCTIONS* */

    /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    /* modified */
    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    /* removed
      public static function findIdentityByAccessToken($token)
      {
      throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
      }
     */

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param  string      $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {

        //return $this->password === sha1($password);
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Security::generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Security::generateRandomKey();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Security::generateRandomKey() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /** EXTENSION MOVIE * */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function init() {
        parent::init();
        $this->my_details = [1 => Yii::$app->user->identity->username, 2 => Yii::$app->user->identity->password];
    }

    public static function getSex($sex) {
        switch ($sex) {

            case 'F':
                return 'FEMALE';
                break;

            case 'M':
                return 'MALE';
                break;

            default:
                break;
        }
    }

    public static function getUserType($user_type) {
        switch ($user_type) {

            case '1':
                return 'Admin';
                break;

            case '2':
                return 'Garage Owner';
                break;

            case '3':
                return 'Customer';
                break;

            default:
                break;
        }
    }

    public static function getRolesGivenPermission($permission) {
        $roles = array();
        $parents = AuthItemChild::find()->select("parent")->where("child = '{$permission}' ")->asArray()->all();
        foreach ($parents as $parent) {
            if (AuthItem::find()->where("description = '" . $parent['parent'] . "' ")->one()->type == 1) {
                $roles[] = $parent['parent'];
            }

            if (AuthItemChild::find()->where("child = '{$parent['parent']}' ")->count() != 0) {
                $roles = array_merge($roles, static::getRolesGivenPermission($parent['parent']));
            }
        }
        return $roles;
    }

    /**
     * 
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param type $permission
     * @param type $user_id
     * @return type
     */
    public static function getLevelsGivenPermission($permission, $user_id) {
        $roles = static::getRolesGivenPermission($permission);

        $levels_all = array();
        foreach ($roles as $role) {

            $role_levels = AuthAssignment::find()->select("institution_structure_id")->where("item_name = '{$role}' AND user_id = {$user_id} ")->asArray()->all();
            foreach ($role_levels as $level) {
                $child_level_ids = InstitutionStructure::getChildLevels($level['institution_structure_id']);
                $levels_all = array_merge($levels_all, $child_level_ids);
            }
        }
        return $levels_all;
    }

    /**
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param type $permission
     * @param type $user_id
     * @return type
     */
    public static function getPermissLevelsInClause($permission, $user_id) {
        $levels = static::getLevelsGivenPermission($permission, $user_id);
        return "(" . implode(",", $levels) . ")";
    }

    /**
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param String $item_name
     * @return Array of routes|actions
     */
    public static function getRoutesGivenItemName($item_name) {
        $child_items = array();
        $items = AuthItemChild::find()->where("parent = '{$item_name}' ")->all();
        if (count($items) == 0) {
            $child_items[] = $item_name;
            return $child_items;
        }
        foreach ($items as $item) {
            if (AuthItemChild::find()->where("parent = '{$item->child}'")->count() > 0) {
                $child_items = array_merge($child_items, static::getRoutesGivenItemName($item->child));
            } else {
                $child_items[] = $item->child;
            }
        }
        return $child_items;
    }

    /**
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param type $user_id
     * @return type
     */
    public static function getUserRoutes($user_id) {
        $routes = array();
        $roles = AuthAssignment::find()->where("user_id = {$user_id} ")->all();
        foreach ($roles as $role) {
            $routes = array_merge($routes, static::getRoutesGivenItemName($role->item_name));
        }
        return $routes;
    }

    /**
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param type $user_id
     * @return type
     */
    public static function getUserRoutesWithLevelBased($user_id) {
        $routes_with_level_based = array();
        $routes = static::getUserRoutes($user_id);
        foreach ($routes as $route) {
            if (AuthItem::find()->where("name = '{$route}'")->one()->level_based == 1) {
                $routes_with_level_based[] = $route;
            }
        }
        return $routes_with_level_based;
    }

    public static function getItemRoutesWithLevelBased($item_name) {
        $routes_with_level_based = array();
        $routes = static::getRoutesGivenItemName($item_name);
        foreach ($routes as $route) {
            if (AuthItem::find()->where("name = '{$route}'")->one()->level_based == 1) {
                $routes_with_level_based[] = $route;
            }
        }
        return $routes_with_level_based;
    }

}
