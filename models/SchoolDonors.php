<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_donors".
 *
 * @property integer $school_donor_id
 * @property integer $school_id
 * @property integer $donor_id
 * @property string $what_donated
 * @property double $amount
 * @property string $date_created
 * @property string $date_donated
 * @property integer $is_visible
 */
class SchoolDonors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_donors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'donor_id', 'what_donated', 'amount', 'date_donated', 'is_visible'], 'required'],
            [['school_id', 'donor_id', 'is_visible'], 'integer'],
            [['what_donated'], 'string'],
            [['amount'], 'number'],
            [['date_created', 'date_donated'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_donor_id' => 'School Donor ID',
            'school_id' => 'School ID',
            'donor_id' => 'Donor ID',
            'what_donated' => 'Description',
            'amount' => 'Amount',
            'date_created' => 'Date Created',
            'date_donated' => 'Date Donated',
            'is_visible' => 'Is Visible',
        ];
    }
}
