<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_sponsors".
 *
 * @property integer $school_sponsor_id
 * @property integer $school_id
 * @property integer $sponsor_id
 * @property string $what_sponsored
 * @property double $amount
 * @property string $date_created
 * @property string $date_sponsored
 * @property integer $is_visible
 */
class SchoolSponsors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_sponsors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'sponsor_id', 'what_sponsored', 'amount', 'date_sponsored', 'is_visible'], 'required'],
            [['school_id', 'sponsor_id', 'is_visible'], 'integer'],
            [['what_sponsored'], 'string'],
            [['amount'], 'number'],
            [['date_created', 'date_sponsored'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_sponsor_id' => 'School Sponsor ID',
            'school_id' => 'School ID',
            'sponsor_id' => 'Sponsor ID',
            'what_sponsored' => 'Description',
            'amount' => 'Amount',
            'date_created' => 'Date Created',
            'date_sponsored' => 'Date Sponsored',
            'is_visible' => 'Is Visible',
        ];
    }
}
