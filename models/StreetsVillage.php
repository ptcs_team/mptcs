<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "streets_village".
 *
 * @property integer $street_id
 * @property integer $ward_id
 * @property string $street
 * @property integer $is_active
 */
class StreetsVillage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'streets_village';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ward_id', 'street', 'is_active'], 'required'],
            [['ward_id', 'is_active'], 'integer'],
            [['street'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'street_id' => 'Street ID',
            'ward_id' => 'Ward',
            'street' => 'Street',
            'is_active' => 'Is Active',
        ];
    }
    
         public static function getStreets($ward_id) {
       $data = self::findBySql(" SELECT streets_village.street_id AS id, streets_village.street AS name FROM streets_village  WHERE  streets_village.ward_id = {$ward_id}")->asArray()->all();
       $value = (count($data) == 0) ? ['' => ''] : $data;
       return $value;
   }
}
