<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class_year_of_study".
 *
 * @property integer $cyos_id
 * @property integer $cy_id
 * @property string $cyos_description
 * @property integer $cyos_number
 * @property integer $cyos_status_id
 * @property integer $approval_status_id
 * @property integer $publish_status_id
 *
 * @property ClassYear $cy
 */
class ClassYearOfStudy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_year_of_study';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cy_id', 'cyos_description', 'cyos_number', 'cyos_status_id', 'approval_status_id', 'publish_status_id'], 'required'],
            [['cy_id', 'cyos_number', 'cyos_status_id', 'approval_status_id', 'publish_status_id'], 'integer'],
            [['cyos_description'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cyos_id' => 'Cyos ID',
            'cy_id' => 'Cy ID',
            'cyos_description' => 'Cyos Description',
            'cyos_number' => 'Cyos Number',
            'cyos_status_id' => 'Cyos Status ID',
            'approval_status_id' => 'Approval Status ID',
            'publish_status_id' => 'Publish Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCy()
    {
        return $this->hasOne(ClassYear::className(), ['cy_id' => 'cy_id']);
    }
}
