<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schools".
 *
 * @property integer $school_id
 * @property integer $school_type_id
 * @property integer $region_id
 * @property integer $district_id
 * @property integer $ward_id
 * @property integer $street_id
 * @property string $school_name
 * @property string $school_description
 * @property string $address
 * @property string $phone_no
 * @property string $e_mail
 * @property string $fax
 * @property string $logo
 * @property integer $start_year_id
 * @property string $google_location
 * @property integer $school_privacy_statement_id
 * @property integer $school_board_id
 * @property integer $school_donor_id
 * @property integer $school_sponsor_id
 * @property integer $school_gallery_id
 * @property string $school_motto
 * @property integer $headmaster_statement_id
 * @property integer $msg_frm_teacher_id
 * @property integer $msg_frm_parent_id
 * @property integer $inbox_id
 * @property integer $is_active
 *
 * @property Regions $region
 * @property Districts $district
 * @property Wards $ward
 * @property StreetsVillage $street
 * @property Years $startYear
 */
class Schools extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
     public $image;
    
    public static function tableName()
    {
        return 'schools';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_type_id', 'region_id', 'district_id', 'ward_id', 'street_id', 'school_name', 'address', 'phone_no', 'e_mail', 'fax', 'logo', 'start_year_id', 'google_location', 'school_privacy_statement_id', 'school_board_id', 'school_donor_id', 'school_sponsor_id', 'school_gallery_id', 'school_motto', 'headmaster_statement_id', 'msg_frm_teacher_id', 'msg_frm_parent_id', 'inbox_id', 'is_active'], 'required'],
            [['school_type_id', 'region_id', 'district_id', 'ward_id', 'street_id', 'start_year_id', 'school_privacy_statement_id', 'school_board_id', 'school_donor_id', 'school_sponsor_id', 'school_gallery_id', 'headmaster_statement_id', 'msg_frm_teacher_id', 'msg_frm_parent_id', 'inbox_id', 'is_active'], 'integer'],
            [['school_description', 'google_location'], 'string'],
            [['school_name', 'school_motto'], 'string', 'max' => 300],
            [['address', 'phone_no', 'e_mail', 'fax', 'logo'], 'string', 'max' => 250],
            [['phone_no'], 'unique'],
            [['e_mail'], 'unique'],
            [['fax'], 'unique'],
            ['image', 'file', 'extensions' => ['png', 'jpg', 'gif', 'jpeg'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_id' => 'School ID',
            'school_type_id' => 'School Type ID',
            'region_id' => 'Region ID',
            'district_id' => 'District ID',
            'ward_id' => 'Ward ID',
            'street_id' => 'Street ID',
            'school_name' => 'School Name',
            'school_description' => 'School Description',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'e_mail' => 'E Mail',
            'fax' => 'Fax',
            'logo' => 'Logo',
            'start_year_id' => 'Start Year ID',
            'google_location' => 'Google Location',
            'school_privacy_statement_id' => 'School Privacy Statement ID',
            'school_board_id' => 'School Board ID',
            'school_donor_id' => 'School Donor ID',
            'school_sponsor_id' => 'School Sponsor ID',
            'school_gallery_id' => 'School Gallery ID',
            'school_motto' => 'School Motto',
            'headmaster_statement_id' => 'Headmaster Statement ID',
            'msg_frm_teacher_id' => 'Msg Frm Teacher ID',
            'msg_frm_parent_id' => 'Msg Frm Parent ID',
            'inbox_id' => 'Inbox ID',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['district_id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWard()
    {
        return $this->hasOne(Wards::className(), ['ward_id' => 'ward_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(StreetsVillage::className(), ['street_id' => 'street_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartYear()
    {
        return $this->hasOne(Years::className(), ['year_id' => 'start_year_id']);
    }
}
