<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vwOlevelStudents".
 *
 * @property string $user_id
 * @property string $profile_picture
 * @property integer $student_status_id
 * @property string $registration_number
 * @property string $surname
 * @property string $firstname
 * @property string $middlename
 * @property integer $school_id
 * @property string $sex
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $mailing_address
 * @property string $telephone_no
 * @property string $email_address
 * @property string $admission_date
 * @property string $status_comments
 * @property string $completion_date
 * @property string $username
 * @property string $password
 * @property integer $is_active
 * @property integer $login_counts
 * @property string $last_login
 * @property integer $user_type
 * @property integer $staff_position_id
 * @property string $salutation
 * @property string $fullname
 * @property string $olevel_student_year_of_study_id
 * @property integer $fy_id
 * @property integer $student_year_status_id
 * @property integer $year_of_study
 * @property double $results
 * @property integer $year_units
 * @property double $year_grade_points
 * @property integer $status
 */
class VwOlevelStudents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwOlevelStudents';
    }
    
    
   public static function primaryKey()
    {
        return ['user_id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'student_status_id', 'school_id', 'is_active', 'login_counts', 'user_type', 'staff_position_id', 'olevel_student_year_of_study_id', 'fy_id', 'student_year_status_id', 'year_of_study', 'year_units', 'status','academic_year_id'], 'integer'],
            [['student_status_id', 'registration_number', 'admission_date', 'status_comments', 'completion_date', 'username', 'login_counts', 'last_login', 'staff_position_id', 'salutation', 'fy_id', 'student_year_status_id', 'year_of_study', 'results', 'year_units', 'year_grade_points', 'status','academic_year_id'], 'required'],
            [['admission_date', 'completion_date', 'last_login'], 'safe'],
            [['results', 'year_grade_points'], 'number'],
            [['profile_picture', 'username'], 'string', 'max' => 200],
            [['registration_number', 'date_of_birth', 'status_comments', 'password'], 'string', 'max' => 255],
            [['surname', 'firstname', 'middlename', 'salutation'], 'string', 'max' => 20],
            [['sex'], 'string', 'max' => 1],
            [['place_of_birth'], 'string', 'max' => 50],
            [['mailing_address', 'telephone_no'], 'string', 'max' => 150],
            [['email_address'], 'string', 'max' => 100],
            [['fullname'], 'string', 'max' => 62]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'profile_picture' => 'Profile Picture',
            'student_status_id' => 'Student Status ID',
            'registration_number' => 'Registration Number',
            'surname' => 'Surname',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'school_id' => 'School ID',
            'sex' => 'Sex',
            'date_of_birth' => 'Date Of Birth',
            'place_of_birth' => 'Place Of Birth',
            'mailing_address' => 'Mailing Address',
            'telephone_no' => 'Telephone No',
            'email_address' => 'Email Address',
            'admission_date' => 'Admission Date',
            'status_comments' => 'Status Comments',
            'completion_date' => 'Completion Date',
            'username' => 'Username',
            'password' => 'Password',
            'is_active' => 'Is Active',
            'login_counts' => 'Login Counts',
            'last_login' => 'Last Login',
            'user_type' => 'User Type',
            'staff_position_id' => 'Staff Position ID',
            'salutation' => 'Salutation',
            'fullname' => 'Fullname',
            'olevel_student_year_of_study_id' => 'Olevel Student Year Of Study ID',
            'fy_id' => 'Fy ID',
            'student_year_status_id' => 'Student Year Status ID',
            'year_of_study' => 'Year Of Study',
            'results' => 'Results',
            'year_units' => 'Year Units',
            'year_grade_points' => 'Year Grade Points',
            'status' => 'Status',
            'academic_year_id'=>'Academic Year'
        ];
    }
}
