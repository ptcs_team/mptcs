<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class_year".
 *
 * @property integer $cy_id
 * @property integer $academic_year_id
 * @property integer $year_number
 * @property integer $grading_system_version_id
 * @property integer $study_year
 * @property integer $required_year_units
 * @property integer $class_year_status_id
 * @property integer $approval_status_id
 * @property integer $publish_status_id
 * @property integer $school_id
 *
 * @property AcademicYears $academicYear
 * @property GradingSystemVersions $gradingSystemVersion
 */
class ClassYear extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_year';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year_id', 'year_number', 'grading_system_version_id', 'study_year', 'class_year_status_id', 'approval_status_id', 'publish_status_id', 'school_id'], 'required'],
            [['academic_year_id', 'year_number', 'grading_system_version_id', 'study_year', 'required_year_units', 'class_year_status_id', 'approval_status_id', 'publish_status_id', 'school_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cy_id' => 'Cy ID',
            'academic_year_id' => 'Academic Year ID',
            'year_number' => 'Year Number',
            'grading_system_version_id' => 'Grading System Version ID',
            'study_year' => 'Study Year',
            'required_year_units' => 'Required Year Units',
            'class_year_status_id' => 'Class Year Status ID',
            'approval_status_id' => 'Approval Status ID',
            'publish_status_id' => 'Publish Status ID',
            'school_id' => 'School ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(AcademicYears::className(), ['academic_year_id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradingSystemVersion()
    {
        return $this->hasOne(GradingSystemVersions::className(), ['grading_system_version_id' => 'grading_system_version_id']);
    }
    
    
    
        
     static public function getClassYearDesc($study_year) {
        
        switch ($study_year) {
            case '1':
                return 'Class One';

                break;
            
              case '2':
                return 'Class Two';

                break;
            
              case '3':
                return 'Class Three';

                break;
            
              case '4':
                return 'Class Four';

                break;
            
              case '5':
                return 'Class Five';

                break;
            
             case '6':
                return 'Class Six';

                break;
            
             case '7':
                return 'Class Seven';

                break;
            
             

        }
        
    }
    
    
     static public function getClass($study_year) {
        
        switch ($study_year) {
            case '1':
                return 'Class One';

                break;
            
              case '2':
                return 'Class Two';

                break;
            
              case '3':
                return 'Class Three';

                break;
            
              case '4':
                return 'Class Four';

                break;
            
              case '5':
                return 'Class Five';

                break;
            
             case '6':
                return 'Class Six';

                break;
            
             

        }
        
    }
    
    
}
