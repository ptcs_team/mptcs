<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logins".
 *
 * @property integer $login_id
 * @property integer $user_id
 * @property string $ip_address
 * @property string $details
 * @property string $datecreated
 */
class Logins extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'logins';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'ip_address', 'details'], 'required'],
            [['user_id'], 'integer'],
            [['datecreated'], 'safe'],
            [['ip_address'], 'string', 'max' => 50],
            [['details'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'login_id' => 'Login ID',
            'user_id' => 'User',
            'ip_address' => 'Ip Address',
            'details' => 'Details',
            'datecreated' => 'Datecreated',
        ];
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

}
