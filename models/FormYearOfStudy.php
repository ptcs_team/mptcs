<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "form_year_of_study".
 *
 * @property integer $fyos_id
 * @property integer $fy_id
 * @property string $fyos_description
 * @property integer $fyos_number
 * @property integer $fyos_status_id
 * @property integer $approval_status_id
 * @property integer $publish_status_id
 *
 * @property FormYear $fy
 * @property FormYearOfStudyStatus $fyosStatus
 */
class FormYearOfStudy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_year_of_study';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fy_id', 'fyos_description', 'fyos_number', 'fyos_status_id', 'approval_status_id', 'publish_status_id'], 'required'],
            [['fy_id', 'fyos_number', 'fyos_status_id', 'approval_status_id', 'publish_status_id'], 'integer'],
            [['fyos_description'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fyos_id' => 'Fyos ID',
            'fy_id' => 'Fy ID',
            'fyos_description' => 'Fyos Description',
            'fyos_number' => 'Fyos Number',
            'fyos_status_id' => 'Fyos Status ID',
            'approval_status_id' => 'Approval Status ID',
            'publish_status_id' => 'Publish Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFy()
    {
        return $this->hasOne(FormYear::className(), ['fy_id' => 'fy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFyosStatus()
    {
        return $this->hasOne(FormYearOfStudyStatus::className(), ['form_year_of_study_status_id' => 'fyos_status_id']);
    }
}
