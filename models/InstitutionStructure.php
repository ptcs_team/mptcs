<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institution_structure".
 *
 * @property integer $institution_structure_id
 * @property string $institution_name
 * @property string $institution_acronym
 * @property integer $parent_institution_structure_id
 * @property integer $institution_type_id
 * @property integer $status
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property string $post_office_box
 * @property string $region
 * @property string $country
 * @property string $logo
 *
 * @property InstitutionType $institutionType
 */
class InstitutionStructure extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'institution_structure';
    }

    public $logo_image, $logo2_image;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['institution_name', 'institution_acronym', 'parent_institution_structure_id', 'institution_type_id', 'status', 'phone', 'fax', 'email', 'website', 'post_office_box', 'region', 'country'], 'required'],
            [['parent_institution_structure_id', 'institution_type_id', 'status'], 'integer'],
            [['institution_name'], 'string', 'max' => 150],
            [['logo'], 'string', 'max' => 200],
            [['institution_acronym'], 'string', 'max' => 70],
            [['phone', 'fax', 'email', 'website', 'post_office_box', 'region', 'country'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['institution_acronym'], 'unique'],
            [['logo', 'logo2'], 'safe'],
//             [['logo_image'], 'file'],
            ['logo_image', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024 * 1024 * 2],
            ['logo2_image', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @Author Emmanuel Natalis <matnatalis@gmail.com>
     * @Date 30 November 2015
     * @param int $parent_level_id: The institution_structure_id of the parent which we want to get their child levels
     * @return Array of child levels
     */
    static public function getChildLevels($parent_level_id, $include_parent = true) {
        $childLevelsInArray = array();
        if($parent_level_id == NULL){
          $childLevelsInArray[] = -1;
          return $childLevelsInArray;  
        }
        if ($include_parent) {
            $childLevelsInArray[] = $parent_level_id;
        }
        $childLevels = static::find()->select("institution_structure_id")->where("parent_institution_structure_id = {$parent_level_id} ")->asArray()->all();
        foreach ($childLevels as $childLevel) {
            $childLevelsInArray[] = $childLevel['institution_structure_id'];
            if (static::find()->where("parent_institution_structure_id = {$childLevel['institution_structure_id']} ")->count() > 0) {
                $childLevelsInArray2 = static::getChildLevels($childLevel['institution_structure_id'], false);
                $childLevelsInArray = array_merge($childLevelsInArray, $childLevelsInArray2);
            }
        }
        return $childLevelsInArray;
    }

    /**
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param Array  - Array of parent IDs
     * @return Array  - array of child levels
     */
    public static function getChildLevelsGivenArrayOfParents($parents) {
        $childLevels = array();
        foreach ($parents as $parent) {
            $childLevels = array_merge($childLevels, static::getChildLevels($parent));
        }
        return $childLevels;
    }

    /**
     * @author Emmanuel Natalis <matnatalis@gmail.com>
     * @param Array $parents - Parent IDs in Array
     * @return String - IN Clause
     */
    public static function getInClauseForChildLevels($parents) {
        $childLevels = static::getChildLevelsGivenArrayOfParents($parents);
        return "(" . implode(",", $childLevels) . ")";
    }

    public static function getInstitutionTree($parent_level = 0, $steps = 0, $include_parent = true){
        $separator = "= = = ";
        $alevels = array();
        if($include_parent){
         $modelp = InstitutionStructure::findOne($parent_level);
         $alevels[$parent_level] =  $modelp->institution_name . " [".InstitutionType::findOne($modelp->institution_type_id)->institution_type_name."]";  
         //++$steps;
        }
        
        $levels = static::find()->where("parent_institution_structure_id = {$parent_level}")->all(); 
        foreach ($levels as $level) {
          $lavel_name = $level->institution_name." [".InstitutionType::findOne($level->institution_type_id)->institution_type_name."]";
          
          for($i=0; $i<=$steps; ++$i){
            $lavel_name = $separator.$lavel_name;
          }
          $alevels[$level->institution_structure_id] = $lavel_name;
          if(InstitutionStructure::find()->where("parent_institution_structure_id = {$level->institution_structure_id}")->count() > 0){
             $lower_levels1 = static::getInstitutionTree($level->institution_structure_id, ++$steps, false);
             foreach ($lower_levels1 as $key => $value) {
                 $alevels[$key] = $value;
             }
             --$steps;
          }
        }
        
        return $alevels;
        
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'institution_structure_id' => 'Institution Structure',
            'institution_name' => 'Institution Name',
            'institution_acronym' => 'Institution Acronym',
            'parent_institution_structure_id' => 'Parent Institution',
            'institution_type_id' => 'Institution Type',
            'status' => 'Status',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'email' => 'Email',
            'website' => 'Website',
            'post_office_box' => 'Post Office Box',
            'region' => 'Region',
            'country' => 'Country',
            'logo' => 'Logo',
            'logo2' => 'Logo 2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionType() {
        return $this->hasOne(InstitutionType::className(), ['institution_type_id' => 'institution_type_id']);
    }

}
