<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_forms".
 *
 * @property integer $sf_id
 * @property integer $fy_id
 * @property integer $olevel_student_year_of_study_id
 * @property integer $form_year_of_study_number
 * @property integer $student_form_status
 *
 * @property StudentFormCourses[] $studentFormCourses
 * @property OlevelStudentYearOfStudy $olevelStudentYearOfStudy
 * @property FormYear $fy
 */
class StudentForms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_forms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fy_id', 'olevel_student_year_of_study_id', 'form_year_of_study_number', 'student_form_status'], 'required'],
            [['fy_id', 'olevel_student_year_of_study_id', 'form_year_of_study_number', 'student_form_status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sf_id' => 'Sf ID',
            'fy_id' => 'Fy ID',
            'olevel_student_year_of_study_id' => 'Olevel Student Year Of Study ID',
            'form_year_of_study_number' => 'Form Year Of Study Number',
            'student_form_status' => 'Student Form Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentFormCourses()
    {
        return $this->hasMany(StudentFormCourses::className(), ['sf_id' => 'sf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOlevelStudentYearOfStudy()
    {
        return $this->hasOne(OlevelStudentYearOfStudy::className(), ['olevel_student_year_of_study_id' => 'olevel_student_year_of_study_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFy()
    {
        return $this->hasOne(FormYear::className(), ['fy_id' => 'fy_id']);
    }
}
