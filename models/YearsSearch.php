<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Years;

/**
 * YearsSearch represents the model behind the search form about `app\models\Years`.
 */
class YearsSearch extends Years
{
    public function rules()
    {
        return [
            [['year_id', 'is_active'], 'integer'],
            [['year'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Years::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'year_id' => $this->year_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'year', $this->year]);

        return $dataProvider;
    }
}
