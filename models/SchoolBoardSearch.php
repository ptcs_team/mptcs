<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SchoolBoard;

/**
 * SchoolBoardSearch represents the model behind the search form about `app\models\SchoolBoard`.
 */
class SchoolBoardSearch extends SchoolBoard
{
    public function rules()
    {
        return [
            [['school_board_id', 'school_id', 'is_active'], 'integer'],
            [['name', 'description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = SchoolBoard::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_board_id' => $this->school_board_id,
            'school_id' => $this->school_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
