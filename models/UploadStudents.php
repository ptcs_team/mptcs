<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $user_id
 * @property integer $student_status_id
 * @property string $registration_number
 * @property string $surname
 * @property string $firstname
 * @property string $middlename
 * @property integer $ps_id
 * @property string $sex
 * @property string $profile_picture
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property string $mailing_address
 * @property string $telephone_no
 * @property string $email_address
 * @property string $admission_date
 * @property string $sponsorship_type
 * @property string $status_comments
 * @property string $completion_date
 * @property string $username
 * @property string $password
 * @property integer $is_active
 * @property integer $login_counts
 * @property string $last_login
 * @property integer $user_type
 * @property integer $staff_position_id
 * @property string $salutation
 *
 * @property StudentYearOfStudy[] $studentYearOfStudies
 */
class UploadStudents extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['file'], 'required'],
            //[['completion_date', 'last_login'], 'safe'],
            //[['registration_number', 'date_of_birth', 'admission_date', 'status_comments', 'password'], 'string', 'max' => 255],
            //[['surname', 'firstname', 'middlename', 'salutation'], 'string', 'max' => 20],
           // [['sex', 'sponsorship_type'], 'string', 'max' => 1],
           // [['file'],'file','extensions'=>'xls, xlsx, ods'],
           // [['profile_picture', 'username'], 'string', 'max' => 200],
            //[['place_of_birth'], 'string', 'max' => 50],
           // [['mailing_address', 'telephone_no'], 'string', 'max' => 150],
           // [['email_address'], 'string', 'max' => 100],
           // [['username'], 'unique'],
           // [['registration_number'], 'unique'],
           // [['email_address'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Please browse an Excel file (xls, xlsx, ods)',
//            'user_id' => 'User ID',
//            'student_status_id' => 'Student Status ID',
//            'registration_number' => 'Registration Number',
//            'surname' => 'Surname',
//            'firstname' => 'Firstname',
//            'middlename' => 'Middlename',
//            'session_id' => 'Session',
//            'sex' => 'Sex',
//            'profile_picture' => 'Profile Picture',
//            'date_of_birth' => 'Date Of Birth',
//            'place_of_birth' => 'Place Of Birth',
//            'mailing_address' => 'Mailing Address',
//            'telephone_no' => 'Telephone No',
//            'email_address' => 'Email Address',
//            'admission_date' => 'Admission Date',
//            'sponsorship_type' => 'Sponsorship Type',
//            'status_comments' => 'Status Comments',
//            'completion_date' => 'Completion Date',
//            'username' => 'Username',
//            'password' => 'Password',
//            'is_active' => 'Is Active',
//            'login_counts' => 'Login Counts',
//            'last_login' => 'Last Login',
//            'user_type' => 'User Type',
//            'staff_position_id' => 'Staff Position ID',
//            'salutation' => 'Salutation',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentYearOfStudies()
    {
        return $this->hasMany(StudentYearOfStudy::className(), ['user_id' => 'user_id']);
    }
}
