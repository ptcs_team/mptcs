<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_class_courses".
 *
 * @property integer $scc_id
 * @property integer $cy_id
 * @property integer $cyos_course_id
 * @property integer $score_type
 * @property double $result
 * @property integer $grade_scored
 * @property integer $student_class_course_status_id
 * @property string $remarks
 * @property double $grade_points
 * @property string $changes_log
 * @property integer $last_updated
 * @property integer $sc_id
 *
 * @property ClassYearOfStudyCourses $cyosCourse
 * @property StudentClasses $sc
 */
class StudentClassCourses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_class_courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cy_id', 'cyos_course_id', 'score_type', 'result', 'grade_scored', 'student_class_course_status_id', 'remarks', 'grade_points', 'changes_log', 'last_updated', 'sc_id'], 'required'],
            [['cy_id', 'cyos_course_id', 'score_type', 'grade_scored', 'student_class_course_status_id', 'last_updated', 'sc_id'], 'integer'],
            [['result', 'grade_points'], 'number'],
            [['changes_log'], 'string'],
            [['remarks'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'scc_id' => 'Scc ID',
            'cy_id' => 'Cy ID',
            'cyos_course_id' => 'Cyos Course ID',
            'score_type' => 'Score Type',
            'result' => 'Result',
            'grade_scored' => 'Grade Scored',
            'student_class_course_status_id' => 'Student Class Course Status ID',
            'remarks' => 'Remarks',
            'grade_points' => 'Grade Points',
            'changes_log' => 'Changes Log',
            'last_updated' => 'Last Updated',
            'sc_id' => 'Sc ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCyosCourse()
    {
        return $this->hasOne(ClassYearOfStudyCourses::className(), ['cyos_course_id' => 'cyos_course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSc()
    {
        return $this->hasOne(StudentClasses::className(), ['sc_id' => 'sc_id']);
    }
}
