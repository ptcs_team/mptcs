<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SchoolGallery;

/**
 * SchoolGallerySearch represents the model behind the search form about `app\models\SchoolGallery`.
 */
class SchoolGallerySearch extends SchoolGallery
{
    public function rules()
    {
        return [
            [['school_gallery_id', 'school_id', 'is_visible'], 'integer'],
            [['gallery', 'date_uploaded'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SchoolGallery::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_gallery_id' => $this->school_gallery_id,
            'school_id' => $this->school_id,
            'is_visible' => $this->is_visible,
            'date_uploaded' => $this->date_uploaded,
        ]);

        $query->andFilterWhere(['like', 'gallery', $this->gallery]);

        return $dataProvider;
    }
}
