<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Schools;

/**
 * SchoolsSearch represents the model behind the search form about `app\models\Schools`.
 */
class SchoolsSearch extends Schools
{
    public function rules()
    {
        return [
            [['school_id', 'school_type_id', 'region_id', 'district_id', 'ward_id', 'street_id', 'start_year_id', 'school_privacy_statement_id', 'school_board_id', 'school_donor_id', 'school_sponsor_id', 'school_gallery_id', 'headmaster_statement_id', 'msg_frm_teacher_id', 'msg_frm_parent_id', 'inbox_id','is_active'], 'integer'],
            [['school_name', 'school_description', 'address', 'phone_no', 'e_mail', 'fax', 'logo', 'google_location','school_motto'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = Schools::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'school_id' => $this->school_id,
            'school_type_id' => $this->school_type_id,
            'is_active'=>  $this->is_active,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'ward_id' => $this->ward_id,
            'street_id' => $this->street_id,
            'start_year_id' => $this->start_year_id,
            'school_privacy_statement_id' => $this->school_privacy_statement_id,
            'school_board_id' => $this->school_board_id,
            'school_donor_id' => $this->school_donor_id,
            'school_sponsor_id' => $this->school_sponsor_id,
            'school_gallery_id' => $this->school_gallery_id,
            'school_motto' => $this->school_motto,
            'headmaster_statement_id' => $this->headmaster_statement_id,
            'msg_frm_teacher_id' => $this->msg_frm_teacher_id,
            'msg_frm_parent_id' => $this->msg_frm_parent_id,
            'inbox_id' => $this->inbox_id,
        ]);

        $query->andFilterWhere(['like', 'school_name', $this->school_name])
            ->andFilterWhere(['like', 'school_description', $this->school_description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone_no', $this->phone_no])
            ->andFilterWhere(['like', 'e_mail', $this->e_mail])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'google_location', $this->google_location]);

        return $dataProvider;
    }
}
