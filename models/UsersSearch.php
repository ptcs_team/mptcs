<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users {

    public function rules() {
        return [
            [['user_id', 'is_active', 'login_counts', 'user_type', 'created_by', 'staff_position_id', 'school_id', 'student_status_id'], 'integer'],
            [['surname', 'firstname', 'middlename', 'registration_number', 'sex', 'profile_picture', 'date_of_birth', 'place_of_birth', 'mailing_address', 'telephone_no', 'email_address', 'username', 'password', 'is_default_password', 'last_login', 'salutation', 'last_password_update_date', 'auth_key', 'password_reset_token', 'created_at', 'completion_date', 'admission_date'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition) {
        $query = Users::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if ($condition != NULL) {
            $query->andWhere($condition);
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'is_active' => $this->is_active,
            'login_counts' => $this->login_counts,
            'last_login' => $this->last_login,
            'user_type' => $this->user_type,
            'staff_position_id' => $this->staff_position_id,
            'school_id' => $this->school_id,
            'student_status_id' => $this->student_status_id,
            'last_password_update_date' => $this->last_password_update_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
                ->andFilterWhere(['like', 'surname', $this->surname])
                ->andFilterWhere(['like', 'middlename', $this->middlename])
                ->andFilterWhere(['like', 'registration_number', $this->registration_number])
                ->andFilterWhere(['like', 'sex', $this->sex])
                ->andFilterWhere(['like', 'admission_date', $this->admission_date])
                ->andFilterWhere(['like', 'completion_date', $this->completion_date])
                ->andFilterWhere(['like', 'profile_picture', $this->profile_picture])
                ->andFilterWhere(['like', 'date_of_birth', $this->date_of_birth])
                ->andFilterWhere(['like', 'place_of_birth', $this->place_of_birth])
                ->andFilterWhere(['like', 'mailing_address', $this->mailing_address])
                ->andFilterWhere(['like', 'telephone_no', $this->telephone_no])
                ->andFilterWhere(['like', 'email_address', $this->email_address])
                ->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'password', $this->password])
                ->andFilterWhere(['like', 'password_hash', $this->is_default_password])
                ->andFilterWhere(['like', 'salutation', $this->salutation])
                ->andFilterWhere(['like', 'auth_key', $this->auth_key])
                ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token]);

        return $dataProvider;
    }

}
