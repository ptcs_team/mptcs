<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MessageFromParents;

/**
 * MessageFromParentsSearch represents the model behind the search form about `app\models\MessageFromParents`.
 */
class MessageFromParentsSearch extends MessageFromParents
{
    public function rules()
    {
        return [
            [['msg_frm_parent_id', 'school_id', 'sent_by', 'receiver', 'status'], 'integer'],
            [['message', 'date_sent'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = MessageFromParents::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'msg_frm_parent_id' => $this->msg_frm_parent_id,
            'school_id' => $this->school_id,
            'sent_by' => $this->sent_by,
            'receiver' => $this->receiver,
            'date_sent' => $this->date_sent,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
