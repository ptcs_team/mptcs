<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_privacy_statement".
 *
 * @property integer $school_privacy_statement _id
 * @property integer $school_id
 * @property string $statement
 * @property string $date_created
 * @property integer $created_by
 * @property integer $is_active
 */
class SchoolPrivacyStatement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_privacy_statement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'statement', 'created_by', 'is_active'], 'required'],
            [['school_id', 'created_by', 'is_active'], 'integer'],
            [['statement'], 'string'],
            [['date_created'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'school_privacy_statement _id' => 'School Privacy Statement  ID',
            'school_id' => 'School ID',
            'statement' => 'Statement',
            'date_created' => 'Date Created',
            'created_by' => 'Created By',
            'is_active' => 'Is Active',
        ];
    }
}
