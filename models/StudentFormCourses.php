<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_form_courses".
 *
 * @property integer $sfc_id
 * @property integer $fy_id
 * @property string $fyos_course_id
 * @property integer $score_type
 * @property double $result
 * @property integer $grade_scored
 * @property integer $student_form_course_status_id
 * @property string $remarks
 * @property double $grade_points
 * @property string $changes_log
 * @property integer $last_updated
 * @property integer $sf_id
 *
 * @property StudentForms $sf
 */
class StudentFormCourses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_form_courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fy_id', 'fyos_course_id', 'score_type', 'result', 'grade_scored', 'student_form_course_status_id', 'remarks', 'grade_points', 'changes_log', 'last_updated', 'sf_id'], 'required'],
            [['fy_id', 'fyos_course_id', 'score_type', 'grade_scored', 'student_form_course_status_id', 'last_updated', 'sf_id'], 'integer'],
            [['result', 'grade_points'], 'number'],
            [['changes_log'], 'string'],
            [['remarks'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sfc_id' => 'Sfc ID',
            'fy_id' => 'Fy ID',
            'fyos_course_id' => 'Fyos Course ID',
            'score_type' => 'Score Type',
            'result' => 'Result',
            'grade_scored' => 'Grade Scored',
            'student_form_course_status_id' => 'Student Form Course Status ID',
            'remarks' => 'Remarks',
            'grade_points' => 'Grade Points',
            'changes_log' => 'Changes Log',
            'last_updated' => 'Last Updated',
            'sf_id' => 'Sf ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSf()
    {
        return $this->hasOne(StudentForms::className(), ['sf_id' => 'sf_id']);
    }
}
