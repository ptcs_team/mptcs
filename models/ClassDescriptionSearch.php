<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassDescription;

/**
 * ClassDescriptionSearch represents the model behind the search form about `app\models\ClassDescription`.
 */
class ClassDescriptionSearch extends ClassDescription
{
    public function rules()
    {
        return [
            [['class_description_id', 'class_number'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ClassDescription::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'class_description_id' => $this->class_description_id,
            'class_number' => $this->class_number,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
