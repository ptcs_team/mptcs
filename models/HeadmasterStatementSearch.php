<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HeadmasterStatement;

/**
 * HeadmasterStatementSearch represents the model behind the search form about `app\models\HeadmasterStatement`.
 */
class HeadmasterStatementSearch extends HeadmasterStatement
{
    public function rules()
    {
        return [
            [['headmaster_statement_id', 'school_id', 'user_id', 'is_active'], 'integer'],
            [['statement', 'date_created'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = HeadmasterStatement::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'headmaster_statement_id' => $this->headmaster_statement_id,
            'school_id' => $this->school_id,
            'user_id' => $this->user_id,
            'date_created' => $this->date_created,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'statement', $this->statement]);

        return $dataProvider;
    }
}
