<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Donors;

/**
 * DonorsSearch represents the model behind the search form about `app\models\Donors`.
 */
class DonorsSearch extends Donors
{
    public function rules()
    {
        return [
            [['donor_id', 'is_active'], 'integer'],
            [['donor_name', 'donor_description', 'address', 'e_mail', 'phone_no', 'fax', 'logo'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Donors::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'donor_id' => $this->donor_id,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'donor_name', $this->donor_name])
            ->andFilterWhere(['like', 'donor_description', $this->donor_description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'e_mail', $this->e_mail])
            ->andFilterWhere(['like', 'phone_no', $this->phone_no])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'logo', $this->logo]);

        return $dataProvider;
    }
}
