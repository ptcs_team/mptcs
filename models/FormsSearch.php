<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Forms;

/**
 * FormsSearch represents the model behind the search form about `app\models\Forms`.
 */
class FormsSearch extends Forms
{
    public function rules()
    {
        return [
            [['form_id', 'admistered_by', 'is_active'], 'integer'],
            [['form_name', 'form_acronym'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Forms::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'form_id' => $this->form_id,
            'admistered_by' => $this->admistered_by,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'form_name', $this->form_name])
            ->andFilterWhere(['like', 'form_acronym', $this->form_acronym]);

        return $dataProvider;
    }
}
