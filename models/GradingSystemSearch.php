<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GradingSystem;

/**
 * GradingSystemSearch represents the model behind the search form about `app\models\GradingSystem`.
 */
class GradingSystemSearch extends GradingSystem
{
    public function rules()
    {
        return [
            [['grading_system_id', 'order', 'grading_system_version_id'], 'integer'],
            [['grade', 'definition', 'grade_points_formula'], 'safe'],
            [['min_score', 'max_score'], 'number'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = GradingSystem::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'grading_system_id' => $this->grading_system_id,
            'order' => $this->order,
            'min_score' => $this->min_score,
            'max_score' => $this->max_score,
            'grading_system_version_id' => $this->grading_system_version_id,
        ]);

        $query->andFilterWhere(['like', 'grade', $this->grade])
            ->andFilterWhere(['like', 'definition', $this->definition])
            ->andFilterWhere(['like', 'grade_points_formula', $this->grade_points_formula]);

        return $dataProvider;
    }
}
