<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MessageFromTeacher;

/**
 * MessageFromTeacherSearch represents the model behind the search form about `app\models\MessageFromTeacher`.
 */
class MessageFromTeacherSearch extends MessageFromTeacher
{
    public function rules()
    {
        return [
            [['msg_frm_teacher_id', 'school_id', 'sent_by', 'receiver', 'is_visible', 'status'], 'integer'],
            [['message', 'date_sent'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = MessageFromTeacher::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'msg_frm_teacher_id' => $this->msg_frm_teacher_id,
            'school_id' => $this->school_id,
            'sent_by' => $this->sent_by,
            'receiver' => $this->receiver,
            'date_sent' => $this->date_sent,
            'is_visible' => $this->is_visible,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
