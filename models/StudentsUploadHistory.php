<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "students_upload_history".
 *
 * @property integer $students_upload_history_id
 * @property integer $user_id
 * @property integer $py_id
 * @property string $ip_address
 * @property string $client_details
 * @property string $uploaded_file
 * @property string $date_uploaded
 * @property string $upload_report
 */
class StudentsUploadHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students_upload_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cy_id','fy_id', 'ip_address', 'client_details', 'uploaded_file', 'upload_report'], 'required'],
            [['user_id', 'py_id'], 'integer'],
            [['date_uploaded'], 'safe'],
            [['upload_report'], 'string'],
            [['ip_address'], 'string', 'max' => 200],
            [['client_details', 'uploaded_file'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'students_upload_history_id' => 'Students Upload History ID',
            'user_id' => 'User ID',
            'cy_id' => 'Class year',
             'fy_id' => 'Form year',
            'ip_address' => 'Ip Address',
            'client_details' => 'Client Details',
            'uploaded_file' => 'Uploaded File',
            'date_uploaded' => 'Date Uploaded',
            'upload_report' => 'Upload Report',
        ];
    }
}
