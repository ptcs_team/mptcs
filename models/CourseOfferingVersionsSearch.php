<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CourseOfferingVersions;

/**
 * CourseOfferingVersionsSearch represents the model behind the search form about `app\models\CourseOfferingVersions`.
 */
class CourseOfferingVersionsSearch extends CourseOfferingVersions
{
    public function rules()
    {
        return [
            [['course_offering_version_id', 'status','school_type_id'], 'integer'],
            [['description'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = CourseOfferingVersions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'course_offering_version_id' => $this->course_offering_version_id,
            'status' => $this->status,
            'school_type_id'=>$this->school_type_id,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
