<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClassYearStatus;

/**
 * ClassYearStatusSearch represents the model behind the search form about `app\models\ClassYearStatus`.
 */
class ClassYearStatusSearch extends ClassYearStatus
{
    public function rules()
    {
        return [
            [['class_year_status_id'], 'integer'],
            [['status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ClassYearStatus::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'class_year_status_id' => $this->class_year_status_id,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
