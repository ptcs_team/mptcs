<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserAuditTrail;

/**
 * UserAuditTrailSearch represents the model behind the search form about `app\models\UserAuditTrail`.
 */
class UserAuditTrailSearch extends UserAuditTrail {

    public function rules() {
        return [
            [['user_audit_trail_id', 'user_id'], 'integer'],
            [['ip_address', 'details', 'datecreated','object','referer','client_details'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition = "1=1") {
        $query = UserAuditTrail::find();
        $query->andWhere($condition);
        $query->orderBy(" user_audit_trail_id  DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_audit_trail_id' => $this->user_audit_trail_id,
            'user_id' => $this->user_id,
            'datecreated' => $this->datecreated,
            'client_details' => $this->client_details,
            'object' => $this->object,
            'referer' => $this->referer,
            
        ]);

        $query->andFilterWhere(['like', 'ip_address', $this->ip_address])
                ->andFilterWhere(['like', 'details', $this->details])
                ->andFilterWhere(['like', 'client_details', $this->client_details])
                ->andFilterWhere(['like', 'referer', $this->referer])
                ->andFilterWhere(['like', 'object', $this->object]);

        return $dataProvider;
    }

}
