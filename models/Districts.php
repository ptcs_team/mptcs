<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "districts".
 *
 * @property integer $district_id
 * @property integer $region_id
 * @property string $district
 * @property integer $is_active
 */
class Districts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'districts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region_id', 'district', 'is_active'], 'required'],
            [['region_id', 'is_active'], 'integer'],
            [['district'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'district_id' => 'District',
            'region_id' => 'Region',
            'district' => 'District',
            'is_active' => 'Is Active',
        ];
    }
    
      
    public static function getDistricts($region_id) {
       $data = self::findBySql(" SELECT districts.district_id AS id, districts.district AS name FROM districts  WHERE  districts.region_id = {$region_id}")->asArray()->all();
       $value = (count($data) == 0) ? ['' => ''] : $data;
       return $value;
   }
}
