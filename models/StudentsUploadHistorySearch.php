<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentsUploadHistory;

/**
 * StudentsUploadHistorySearch represents the model behind the search form about `app\models\StudentsUploadHistory`.
 */
class StudentsUploadHistorySearch extends StudentsUploadHistory {

    public function rules() {
        return [
            [['students_upload_history_id', 'user_id', 'cy_id', 'fy_id'], 'integer'],
            [['ip_address', 'client_details', 'uploaded_file', 'date_uploaded', 'upload_report'], 'safe'],
        ];
    }

    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $condition = "1=1") {
        $query = StudentsUploadHistory::find();
        $query->andWhere($condition);
        $query->orderBy(" students_upload_history_id  DESC ");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'students_upload_history_id' => $this->students_upload_history_id,
            'user_id' => $this->user_id,
            'cy_id' => $this->cy_id,
            'fy_id' => $this->fy_id,
            'date_uploaded' => $this->date_uploaded,
        ]);

        $query->andFilterWhere(['like', 'ip_address', $this->ip_address])
                ->andFilterWhere(['like', 'client_details', $this->client_details])
                ->andFilterWhere(['like', 'uploaded_file', $this->uploaded_file])
                ->andFilterWhere(['like', 'upload_report', $this->upload_report]);

        return $dataProvider;
    }

}
