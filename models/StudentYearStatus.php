<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student_year_status".
 *
 * @property integer $student_year_status_id
 * @property string $status_desc
 */
class StudentYearStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_year_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_desc'], 'required'],
            [['status_desc'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_year_status_id' => 'Student Year Status ID',
            'status_desc' => 'Status Desc',
        ];
    }
}
