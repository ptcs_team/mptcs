<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "message_from_teacher".
 *
 * @property integer $msg_frm_teacher_id
 * @property integer $school_id
 * @property integer $sent_by
 * @property integer $receiver
 * @property string $message
 * @property string $date_sent
 * @property integer $is_visible
 * @property integer $status
 */
class MessageFromTeacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message_from_teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'sent_by', 'receiver', 'message', 'is_visible', 'status'], 'required'],
            [['school_id', 'sent_by', 'receiver', 'is_visible', 'status'], 'integer'],
            [['message'], 'string'],
            [['date_sent'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'msg_frm_teacher_id' => 'Msg Frm Teacher ID',
            'school_id' => 'School ID',
            'sent_by' => 'Sent By',
            'receiver' => 'Receiver',
            'message' => 'Message',
            'date_sent' => 'Date Sent',
            'is_visible' => 'Is Visible',
            'status' => 'Status',
        ];
    }
}
