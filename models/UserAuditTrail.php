<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_audit_trail".
 *
 * @property integer $user_audit_trail_id
 * @property string $user_id
 * @property string $ip_address
 * @property string $details
 * @property string $datecreated
 *
 * @property Users $user
 */
class UserAuditTrail extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user_audit_trail';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['datecreated'], 'safe'],
            [['ip_address'], 'string', 'max' => 50],
            [['details','browser','device','object'], 'string', 'max' => 255],
             [['client_details'], 'string', 'max' => 250],
            [['referer'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_audit_trail_id' => 'User Audit Trail ID',
            'user_id' => 'User ID',
            'ip_address' => 'Ip Address',
            'details' => 'Actions',
            'datecreated' => 'Datecreated',
            'client_details'=>'Client Details',
            'object'=>'Object',
            'referer'=>'Url'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }

    public static function logAudit($action,$objectType, $oldModel = NULL, $newModel = NULL) {
        
        if($oldModel != NULL && $newModel != NULL ){
           $action .= "Old Values: " .static::modelValuesToString($oldModel); 
           $action .= "New Values: " .static::modelValuesToString($newModel); 
        }
        
        $model = new UserAuditTrail();
        $model->user_id = \yii::$app->user->identity->id;
        $model->ip_address = Yii::$app->getRequest()->getUserIP(); 
        $model->client_details = Yii::$app->getRequest()->getUserAgent();
        $model->referer = \Yii::$app->getRequest()->getReferrer();
        $model->object = $objectType;
        $model->details = $action;
        if ($model->save(false)) {
            return true;
        }
        
    }
    
    public static function modelValuesToString($model){
        $valuesInArray = \yii\helpers\ArrayHelper::toArray($model);
        $string = "";
        foreach ($valuesInArray as $key => $value) {
            $string .= $key ."=>".$value."; ";
        }
        return $string;
        
    }
    
}
