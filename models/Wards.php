<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wards".
 *
 * @property integer $ward_id
 * @property string $ward
 * @property integer $district_id
 * @property integer $is_active
 */
class Wards extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ward', 'district_id', 'is_active'], 'required'],
            [['district_id', 'is_active'], 'integer'],
            [['ward'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ward_id' => 'Ward ID',
            'ward' => 'Ward',
            'district_id' => 'District',
            'is_active' => 'Is Active',
        ];
    }
    
    
     public static function getWards($district_id) {
       $data = self::findBySql(" SELECT wards.ward_id AS id, wards.ward AS name FROM wards  WHERE  wards.district_id = {$district_id}")->asArray()->all();
       $value = (count($data) == 0) ? ['' => ''] : $data;
       return $value;
   }
}
