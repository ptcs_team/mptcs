<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\StudentForms;

/**
 * StudentFormsSearch represents the model behind the search form about `app\models\StudentForms`.
 */
class StudentFormsSearch extends StudentForms
{
    public function rules()
    {
        return [
            [['sf_id', 'fy_id', 'olevel_student_year_of_study_id', 'form_year_of_study_number', 'student_form_status'], 'integer'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = StudentForms::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sf_id' => $this->sf_id,
            'fy_id' => $this->fy_id,
            'olevel_student_year_of_study_id' => $this->olevel_student_year_of_study_id,
            'form_year_of_study_number' => $this->form_year_of_study_number,
            'student_form_status' => $this->student_form_status,
        ]);

        return $dataProvider;
    }
}
