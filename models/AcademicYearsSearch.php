<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AcademicYears;

/**
 * AcademicYearsSearch represents the model behind the search form about `app\models\AcademicYears`.
 */
class AcademicYearsSearch extends AcademicYears
{
    public function rules()
    {
        return [
            [['academic_year_id', 'order', 'is_current'], 'integer'],
            [['academic_year'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$condition = "1=1")
    {
        $query = AcademicYears::find();
        $query->andWhere($condition);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'academic_year_id' => $this->academic_year_id,
            'order' => $this->order,
            'is_current' => $this->is_current,
        ]);

        $query->andFilterWhere(['like', 'academic_year', $this->academic_year]);

        return $dataProvider;
    }
}
