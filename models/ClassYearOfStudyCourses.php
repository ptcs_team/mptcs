<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class_year_of_study_courses".
 *
 * @property integer $cyos_course_id
 * @property integer $cy_id
 * @property integer $course_offering_id
 * @property integer $pass_grade_id
 * @property integer $contribute_to_final_result
 * @property integer $is_core
 * @property integer $school_id
 *
 * @property CourseOffering $courseOffering
 */
class ClassYearOfStudyCourses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_year_of_study_courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cy_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core', 'school_id'], 'required'],
            [['cy_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core', 'school_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cyos_course_id' => 'Cyos Course ID',
            'cy_id' => 'Cy ID',
            'course_offering_id' => 'Course Offering ID',
            'pass_grade_id' => 'Pass Grade ID',
            'contribute_to_final_result' => 'Contribute To Final Result',
            'is_core' => 'Is Core',
            'school_id' => 'School ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseOffering()
    {
        return $this->hasOne(CourseOffering::className(), ['course_offering_id' => 'course_offering_id']);
    }
}
