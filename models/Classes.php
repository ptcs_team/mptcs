<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "classes".
 *
 * @property integer $class_id
 * @property string $class_name
 * @property string $class_acronym
 * @property integer $admistered_by
 * @property integer $is_active
 */
class Classes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'classes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_name', 'admistered_by', 'is_active'], 'required'],
            [['admistered_by', 'is_active'], 'integer'],
            [['class_name'], 'string', 'max' => 250],
            [['class_acronym'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'Class ID',
            'class_name' => 'Class Name',
            'class_acronym' => 'Class Acronym',
            'admistered_by' => 'Admistered By',
            'is_active' => 'Is Active',
        ];
    }
}
