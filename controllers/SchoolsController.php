<?php

namespace app\controllers;

use Yii;
use app\models\Schools;
use app\models\SchoolsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use app\models\SchoolPrivacyStatement;
use app\controllers\SchoolPrivacyStatementController;
use app\models\SchoolPrivacyStatementSearch;
use app\models\HeadmasterStatement;
use app\controllers\HeadmasterStatementController;
use app\models\HeadmasterStatementSearch;
use app\models\SchoolBoard;
use app\controllers\SchoolBoardController;
use app\models\SchoolBoardSearch;
use app\models\BoardMembersStatement;
use app\models\BoardMembersStatementSearch;
use app\controllers\BoardMembersStatementController;
use app\models\SchoolSponsors;
use app\controllers\SchoolSponsorsController;
use app\models\SchoolSponsorsSearch;
use app\models\SchoolDonors;
use app\controllers\SchoolDonorsController;
use app\models\SchoolDonorsSearch;
use yii\web\UploadedFile;

/**
 * SchoolsController implements the CRUD actions for Schools model.
 */
class SchoolsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Schools models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SchoolsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Schools model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id, $school_privacy_statement_id = NULL, $subaction = NULL, $headmaster_statement_id = NULL, $school_board_id = NULL, $board_members_statement_id = NULL, $school_sponsor_id = NULL, $school_donor_id = NULL) {
        $model = $this->findModel($id);
        $condition = "school_id = {$id}";
        $model_privacy_statement = new SchoolPrivacyStatement;
        $model_headmaster_statement = new HeadmasterStatement;
        $model_school_board = new SchoolBoard;
        $model_board_statement = new BoardMembersStatement;
        $model_sponsors = new SchoolSponsors;
        $model_donors = new SchoolDonors;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        if ($subaction == 'deactivate' && $school_privacy_statement_id != NULL) {
            $model_privacy_statement = SchoolPrivacyStatement::findOne($school_privacy_statement_id);
            $model_privacy_statement->is_active = 0;
            $model_privacy_statement->save(false);
        }


        if ($subaction == 'activate' && $school_privacy_statement_id != NULL) {
            $model_privacy_statement = SchoolPrivacyStatement::findOne($school_privacy_statement_id);
            $model_privacy_statement->is_active = 1;
            $model_privacy_statement->save(false);
        }

        if ($subaction == 'delete' && $school_privacy_statement_id != NULL) {
            SchoolPrivacyStatement::deleteAll("school_privacy_statement_id = {$school_privacy_statement_id}");
        }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        if ($subaction == 'deactivate' && $headmaster_statement_id != NULL) {
            $model_headmaster_statement = HeadmasterStatement::findOne($headmaster_statement_id);
            $model_headmaster_statement->is_active = 0;
            $model_headmaster_statement->save(false);
        }


        if ($subaction == 'activate' && $headmaster_statement_id != NULL) {
            $model_headmaster_statement = HeadmasterStatement::findOne($headmaster_statement_id);
            $model_headmaster_statement->is_active = 1;
            $model_headmaster_statement->save(false);
        }

        if ($subaction == 'delete' && $headmaster_statement_id != NULL) {
            HeadmasterStatement::deleteAll(" headmaster_statement_id = {$headmaster_statement_id}");
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        if ($subaction == 'deactivate' && school_board_id != NULL) {
            $model_school_board = SchoolBoard::findOne($school_board_id);
            $model_school_board->is_active = 0;
            $model_school_board->save(false);
        }


        if ($subaction == 'activate' && school_board_id != NULL) {
            $model_school_board = SchoolBoard::findOne($school_board_id);
            $model_school_board->is_active = 1;
            $model_school_board->save(false);
        }

        if ($subaction == 'delete' && school_board_id != NULL) {

            if (\app\models\BoardMembers::deleteAll(" school_board_id = {$school_board_id} ")) {
                SchoolBoard::deleteAll(" school_board_id = {$school_board_id}");
            }
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@




        if ($subaction == 'deactivate_board_statement' && $board_members_statement_id != NULL) {
            $model_board_statement = BoardMembersStatement::findOne($board_members_statement_id);
            $model_board_statement->is_active = 0;
            $model_board_statement->save(false);
        }


        if ($subaction == 'activate_board_statement' && $board_members_statement_id != NULL) {
            $model_board_statement = BoardMembersStatement::findOne($board_members_statement_id);
            $model_board_statement->is_active = 1;
            $model_board_statement->save(false);
        }

        if ($subaction == 'delete_board_statement' && $board_members_statement_id != NULL) {
            BoardMembersStatement::deleteAll(" board_members_statement_id = {$board_members_statement_id}");
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        if ($subaction == 'deactivate_sponsor' && $school_sponsor_id != NULL) {
            $model_sponsors = SchoolSponsors::findOne($school_sponsor_id);
            $model_sponsors->is_visible = 0;
            $model_sponsors->save(false);
        }


        if ($subaction == 'activate_sponsor' && $school_sponsor_id != NULL) {
            $model_sponsors = SchoolSponsors::findOne($school_sponsor_id);
            $model_sponsors->is_visible = 1;
            $model_sponsors->save(false);
        }

        if ($subaction == 'delete_sponsor' && $school_sponsor_id != NULL) {
            SchoolSponsors::deleteAll(" school_sponsor_id = {$school_sponsor_id}");
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        if ($subaction == 'deactivate_donor' && $school_donor_id != NULL) {
            $model_donors = SchoolDonors::findOne($school_donor_id);
            $model_donors->is_visible = 0;
            $model_donors->save(false);
        }


        if ($subaction == 'activate_donor' && $school_donor_id != NULL) {
            $model_donors = SchoolDonors::findOne($school_donor_id);
            $model_donors->is_visible = 1;
            $model_donors->save(false);
        }

        if ($subaction == 'delete_donor' && $school_donor_id != NULL) {
            SchoolDonors::deleteAll(" school_donor_id = {$school_donor_id}");
        }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        $searchModel_privacy_statement = new SchoolPrivacyStatementSearch;
        $dataProvider_privacy_statement = $searchModel_privacy_statement->search(Yii::$app->request->getQueryParams(), $condition);


        $searchModel_headmaster_statement = new HeadmasterStatementSearch;
        $dataProvider_headmaster_statement = $searchModel_headmaster_statement->search(Yii::$app->request->getQueryParams(), $condition);


        $searchModel_school_board = new SchoolBoardSearch;
        $dataProvider_school_board = $searchModel_school_board->search(Yii::$app->request->getQueryParams(), $condition);



        $searchModel_board_statement = new BoardMembersStatementSearch;
        $school_board_id_models = SchoolBoard::find()->where(" school_id = {$id} AND is_active = 1")->all();

        $school_board_ids_inclause = "(0";
        foreach ($school_board_id_models as $school_board_id_model) {
            $school_board_ids_inclause = $school_board_ids_inclause . ',' . $school_board_id_model->school_board_id;
        }
        $school_board_ids_inclause = $school_board_ids_inclause . ')';

        $condition2 = "school_board_id IN {$school_board_ids_inclause} ";
        $dataProvider_board_statement = $searchModel_board_statement->search(Yii::$app->request->getQueryParams(), $condition2);



        $searchModel_sponsors = new SchoolSponsorsSearch;
        $dataProvider_sponsors = $searchModel_sponsors->search(Yii::$app->request->getQueryParams(), $condition);


        $searchModel_donors = new SchoolDonorsSearch;
        $dataProvider_donors = $searchModel_donors->search(Yii::$app->request->getQueryParams(), $condition);



        if ($model_privacy_statement->load(Yii::$app->request->post())) {
            $model_privacy_statement->school_id = $id;
            $model_privacy_statement->created_by = Yii::$app->user->id;
            $model_privacy_statement->is_active = 1;
            if ($model_privacy_statement->save(false)) {
                return $this->render('view', ['model' => $model,
                            'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                            'searchModel_privacy_statement' => $searchModel_privacy_statement,
                            'model_privacy_statement' => $model_privacy_statement,
                            'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                            'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                            'model_headmaster_statement' => $model_headmaster_statement,
                            'searchModel_school_board' => $searchModel_school_board,
                            'dataProvider_school_board' => $dataProvider_school_board,
                            'model_school_board' => $model_school_board,
                            'model_board_statement' => $model_board_statement,
                            'searchModel_board_statement' => $searchModel_board_statement,
                            'dataProvider_board_statement' => $dataProvider_board_statement,
                            'model_sponsors' => $model_sponsors,
                            'searchModel_sponsors' => $searchModel_sponsors,
                            'dataProvider_sponsors' => $dataProvider_sponsors,
                            'model_donors' => $model_donors,
                            'searchModel_donors' => $searchModel_donors,
                            'dataProvider_donors' => $dataProvider_donors,
                            'school_id' => $id,
                ]);
            }
        }



        if ($model_headmaster_statement->load(Yii::$app->request->post())) {
            $model_headmaster_statement->school_id = $id;
            $model_headmaster_statement->user_id = Yii::$app->user->id;
            $model_headmaster_statement->is_active = 1;
            if ($model_headmaster_statement->save(false)) {
                return $this->render('view', ['model' => $model,
                            'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                            'searchModel_privacy_statement' => $searchModel_privacy_statement,
                            'model_privacy_statement' => $model_privacy_statement,
                            'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                            'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                            'model_headmaster_statement' => $model_headmaster_statement,
                            'searchModel_school_board' => $searchModel_school_board,
                            'dataProvider_school_board' => $dataProvider_school_board,
                            'model_school_board' => $model_school_board,
                            'model_board_statement' => $model_board_statement,
                            'searchModel_board_statement' => $searchModel_board_statement,
                            'dataProvider_board_statement' => $dataProvider_board_statement,
                            'model_sponsors' => $model_sponsors,
                            'searchModel_sponsors' => $searchModel_sponsors,
                            'dataProvider_sponsors' => $dataProvider_sponsors,
                            'model_donors' => $model_donors,
                            'searchModel_donors' => $searchModel_donors,
                            'dataProvider_donors' => $dataProvider_donors,
                            'school_id' => $id,
                ]);
            }
        }


        if ($model_school_board->load(Yii::$app->request->post())) {
            $model_school_board->school_id = $id;
            $model_school_board->is_active = 1;
            if ($model_school_board->save(false)) {
                return $this->render('view', ['model' => $model,
                            'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                            'searchModel_privacy_statement' => $searchModel_privacy_statement,
                            'model_privacy_statement' => $model_privacy_statement,
                            'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                            'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                            'model_headmaster_statement' => $model_headmaster_statement,
                            'searchModel_school_board' => $searchModel_school_board,
                            'dataProvider_school_board' => $dataProvider_school_board,
                            'model_school_board' => $model_school_board,
                            'model_board_statement' => $model_board_statement,
                            'searchModel_board_statement' => $searchModel_board_statement,
                            'dataProvider_board_statement' => $dataProvider_board_statement,
                            'model_sponsors' => $model_sponsors,
                            'searchModel_sponsors' => $searchModel_sponsors,
                            'dataProvider_sponsors' => $dataProvider_sponsors,
                            'model_donors' => $model_donors,
                            'searchModel_donors' => $searchModel_donors,
                            'dataProvider_donors' => $dataProvider_donors,
                            'school_id' => $id,
                ]);
            }
        }



        if ($model_board_statement->load(Yii::$app->request->post())) {
            $model_board_statement->is_active = 1;
            // $model_board_statement->school_board_id = $id;
            if ($model_board_statement->save(false)) {
                return $this->render('view', ['model' => $model,
                            'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                            'searchModel_privacy_statement' => $searchModel_privacy_statement,
                            'model_privacy_statement' => $model_privacy_statement,
                            'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                            'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                            'model_headmaster_statement' => $model_headmaster_statement,
                            'searchModel_school_board' => $searchModel_school_board,
                            'dataProvider_school_board' => $dataProvider_school_board,
                            'model_school_board' => $model_school_board,
                            'model_board_statement' => $model_board_statement,
                            'searchModel_board_statement' => $searchModel_board_statement,
                            'dataProvider_board_statement' => $dataProvider_board_statement,
                            'model_sponsors' => $model_sponsors,
                            'searchModel_sponsors' => $searchModel_sponsors,
                            'dataProvider_sponsors' => $dataProvider_sponsors,
                            'model_donors' => $model_donors,
                            'searchModel_donors' => $searchModel_donors,
                            'dataProvider_donors' => $dataProvider_donors,
                            'school_id' => $id,
                ]);
            }
        }



        if ($model_sponsors->load(Yii::$app->request->post())) {
            $model_sponsors->school_id = $id;
            if ($model_sponsors->save(false)) {
                return $this->render('view', ['model' => $model,
                            'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                            'searchModel_privacy_statement' => $searchModel_privacy_statement,
                            'model_privacy_statement' => $model_privacy_statement,
                            'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                            'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                            'model_headmaster_statement' => $model_headmaster_statement,
                            'searchModel_school_board' => $searchModel_school_board,
                            'dataProvider_school_board' => $dataProvider_school_board,
                            'model_school_board' => $model_school_board,
                            'model_board_statement' => $model_board_statement,
                            'searchModel_board_statement' => $searchModel_board_statement,
                            'dataProvider_board_statement' => $dataProvider_board_statement,
                            'model_sponsors' => $model_sponsors,
                            'searchModel_sponsors' => $searchModel_sponsors,
                            'dataProvider_sponsors' => $dataProvider_sponsors,
                            'model_donors' => $model_donors,
                            'searchModel_donors' => $searchModel_donors,
                            'dataProvider_donors' => $dataProvider_donors,
                            'school_id' => $id,
                ]);
            }
        }



        if ($model_donors->load(Yii::$app->request->post())) {
            $model_donors->school_id = $id;
            $model_donors->is_visible = 1;
            if ($model_donors->save(false)) {
                return $this->render('view', ['model' => $model,
                            'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                            'searchModel_privacy_statement' => $searchModel_privacy_statement,
                            'model_privacy_statement' => $model_privacy_statement,
                            'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                            'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                            'model_headmaster_statement' => $model_headmaster_statement,
                            'searchModel_school_board' => $searchModel_school_board,
                            'dataProvider_school_board' => $dataProvider_school_board,
                            'model_school_board' => $model_school_board,
                            'model_board_statement' => $model_board_statement,
                            'searchModel_board_statement' => $searchModel_board_statement,
                            'dataProvider_board_statement' => $dataProvider_board_statement,
                            'model_sponsors' => $model_sponsors,
                            'searchModel_sponsors' => $searchModel_sponsors,
                            'dataProvider_sponsors' => $dataProvider_sponsors,
                            'model_donors' => $model_donors,
                            'searchModel_donors' => $searchModel_donors,
                            'dataProvider_donors' => $dataProvider_donors,
                            'school_id' => $id,
                ]);
            }
        }




        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->school_id,
                        'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                        'searchModel_privacy_statement' => $searchModel_privacy_statement,
                        'model_privacy_statement' => $model_privacy_statement,
                        'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                        'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                        'model_headmaster_statement' => $model_headmaster_statement,
                        'searchModel_school_board' => $searchModel_school_board,
                        'dataProvider_school_board' => $dataProvider_school_board,
                        'model_school_board' => $model_school_board,
                        'model_board_statement' => $model_board_statement,
                        'searchModel_board_statement' => $searchModel_board_statement,
                        'dataProvider_board_statement' => $dataProvider_board_statement,
                        'model_sponsors' => $model_sponsors,
                        'searchModel_sponsors' => $searchModel_sponsors,
                        'dataProvider_sponsors' => $dataProvider_sponsors,
                        'model_donors' => $model_donors,
                        'searchModel_donors' => $searchModel_donors,
                        'dataProvider_donors' => $dataProvider_donors,
                        'school_id' => $id,
            ]);
        } else {
            return $this->render('view', ['model' => $model,
                        'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                        'searchModel_privacy_statement' => $searchModel_privacy_statement,
                        'model_privacy_statement' => $model_privacy_statement,
                        'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                        'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                        'model_headmaster_statement' => $model_headmaster_statement,
                        'searchModel_school_board' => $searchModel_school_board,
                        'dataProvider_school_board' => $dataProvider_school_board,
                        'model_school_board' => $model_school_board,
                        'model_board_statement' => $model_board_statement,
                        'searchModel_board_statement' => $searchModel_board_statement,
                        'dataProvider_board_statement' => $dataProvider_board_statement,
                        'model_sponsors' => $model_sponsors,
                        'searchModel_sponsors' => $searchModel_sponsors,
                        'dataProvider_sponsors' => $dataProvider_sponsors,
                        'model_donors' => $model_donors,
                        'searchModel_donors' => $searchModel_donors,
                        'dataProvider_donors' => $dataProvider_donors,
                        'school_id' => $id,
            ]);
        }
    }

    /**
     * Creates a new Schools model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Schools;

        if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $image_instance = $model->image;
             $model->is_active = 1;
            if ($model->validate(['school_type_id','region_id','district_id','ward_id','street_id','school_name','address','phone_no','e_mail','start_year_id','school_motto'])) {
                 
            if ($image_instance != NULL) {
                if ($model->save(false)) {
                    $filepath = 'uploads/school_logos/' . md5($model->school_id) . '.' . $model->image->extension;
                    $model->logo = $filepath;
                    $model->save(false);
                    if ($model->image->saveAs($filepath)) {
                  return $this->redirect(['view', 'id' => $model->school_id]);
                    }
                }
            } elseif ($image_instance == NULL) {
                if ($model->save(false)) {
                  return $this->redirect(['view', 'id' => $model->school_id]);
                }
            }
        }  else {
              return $this->render('create', [
                        'model' => $model,
            ]);
        }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Schools model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $region_id = $model->region_id;
        $district_id = $model->district_id;
        $ward_id = $model->ward_id;
        $street_id = $model->street_id;
        $school_type_id  =  $model->school_type_id;

           if ($model->load(Yii::$app->request->post())) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $image_instance = $model->image;
             $model->region_id = $region_id;
             $model->district_id  = $district_id;
             $model->ward_id  = $ward_id;
             $model->street_id  = $street_id;
             $model->school_type_id  = $school_type_id;
             
            if ($image_instance != NULL) {
                if ($model->save(false)) {
                    $filepath = 'uploads/school_logos/' . md5($model->school_id) . '.' . $model->image->extension;
                    $model->logo = $filepath;
                    $model->save(false);
                    if ($model->image->saveAs($filepath)) {
                  return $this->redirect(['view', 'id' => $model->school_id]);
                    }
                }
            } elseif ($image_instance == NULL) {
                if ($model->save(false)) {
                  return $this->redirect(['view', 'id' => $model->school_id]);
                }
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Schools model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Schools model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Schools the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Schools::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDistricts() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $region_id = $parents[0];
                $out = \app\models\Districts::getDistricts($region_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
    }

    public function actionWards() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $district_id = $parents[0];
                $out = \app\models\Wards::getWards($district_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
    }

    public function actionStreets() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $ward_id = $parents[0];
                $out = \app\models\StreetsVillage::getStreets($ward_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
    }

}
