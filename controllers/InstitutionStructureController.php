<?php

namespace app\controllers;

use Yii;
use app\models\InstitutionStructure;
use app\models\InstitutionStructureSearch;
use app\models\AuthItem;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * InstitutionStructureController implements the CRUD actions for InstitutionStructure model.
 */
class InstitutionStructureController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all InstitutionStructure models.
     * @return mixed
     */
    public function actionIndex() {

        if (!Yii::$app->user->can('/institution-structure/index')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }

        $searchModel = new InstitutionStructureSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single InstitutionStructure model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!Yii::$app->user->can('/institution-structure/view')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->institution_structure_id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new InstitutionStructure model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!Yii::$app->user->can('/institution-structure/create')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = new InstitutionStructure;

        if ($model->load(Yii::$app->request->post())) {
            $model->logo_image = UploadedFile::getInstance($model, 'logo_image');
            $image_instance = $model->logo_image;

            $model->logo2_image = UploadedFile::getInstance($model, 'logo2_image');
            $image_instance2 = $model->logo2_image;

            if ($image_instance == NULL && $image_instance2 == NULL) {
                $model->save(false);
                $objectType = 'InstitutionStructure';
                $action = "Created Institution Structure" . ' ' . $model->institution_name . ' ' . "institution_structure_id =>" . $model->institution_structure_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->redirect(['view', 'id' => $model->institution_structure_id]);
            }

            if ($image_instance2 != NULL) {
                if ($image_instance != NULL) {
                    if ($model->save(false)) {
                        $filepath = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo_image->extension;
                        $model->logo = $filepath;

                        $filepath2 = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo2_image->extension;
                        $model->logo2 = $filepath2;
                        //saving profile picture in the database
                        $model->save(false);
                        if ($model->logo_image->saveAs($filepath) && $model->logo2_image->saveAs($filepath2)) {
                            $objectType = 'InstitutionStructure';
                            $action = "Created Institution Structure" . ' ' . $model->institution_name . ' ' . "institution_structure_id =>" . $model->institution_structure_id;
                            \app\models\UserAuditTrail::logAudit($action, $objectType);
                            return $this->redirect(['view', 'id' => $model->institution_structure_id]);
                        }
                    }
                } else {
                    $model->addError('logo_image', 'Please upload institution logo');
                    $model->addError('logo2_image', 'Please upload Optional institution logo');
                }
            } else {
                if ($image_instance != NULL) {
                    if ($model->save(false)) {
                        $filepath = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo_image->extension;
                        $model->logo = $filepath;
                        //saving profile picture in the database
                        $model->save(false);
                        if ($model->logo_image->saveAs($filepath)) {
                            $objectType = 'InstitutionStructure';
                            $action = "Added Institution Structure" . ' ' . $model->institution_name . ' ' . "institution_structure_id =>" . $model->institution_structure_id;
                            \app\models\UserAuditTrail::logAudit($action, $objectType);

                            return $this->redirect(['view', 'id' => $model->institution_structure_id]);
                        }
                    }
                } else {
                    $model->addError('logo_image', 'Please upload institution logo');
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        if (!Yii::$app->user->can('/institution-structure/update')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);
        $oldModel = $model;

        if ($model->load(Yii::$app->request->post())) {

            $model->logo_image = UploadedFile::getInstance($model, 'logo_image');
            $model->logo2_image = UploadedFile::getInstance($model, 'logo2_image');

            if (($model->logo_image != NULL) && ($model->logo2_image != NULL)) {
                $filepath = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo_image->extension;
                $filepath2 = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo2_image->extension;
                $model->logo = $filepath;
                $model->logo2 = $filepath2;

                if ($model->save(false)) {
                    if ($model->logo_image->saveAs($filepath)) {

                        if ($model->logo2_image->saveAs($filepath2)) {
                            $objectType = 'InstitutionStructure';
                            $action = "Updated InstitutionStructure ";
                            \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                            return $this->redirect(['view', 'id' => $model->institution_structure_id]);
                        }
                    }
                }
            } elseif (($model->logo_image == NULL) && ($model->logo2_image != NULL)) {

                $filepath2 = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo2_image->extension;
                $model->logo2 = $filepath2;
                if ($model->save(false)) {

                    if ($model->logo2_image->saveAs($filepath2)) {
                        $objectType = 'InstitutionStructure';
                        $action = "Updated InstitutionStructure ";
                        \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                        return $this->redirect(['view', 'id' => $model->institution_structure_id]);
                    }
                }
            } elseif (($model->logo_image != NULL) && ($model->logo2_image == NULL)) {

                $filepath3 = 'uploads/institution_logos/' . md5($model->institution_structure_id) . '.' . $model->logo_image->extension;
                $model->logo = $filepath3;
                if ($model->save(false)) {

                    if ($model->logo_image->saveAs($filepath3)) {
                        $objectType = 'InstitutionStructure';
                        $action = "Updated InstitutionStructure ";
                        \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                        return $this->redirect(['view', 'id' => $model->institution_structure_id]);
                    }
                }
            } elseif (($model->logo_image == NULL) && ($model->logo2_image == NULL)) {

                if ($model->save(false)) {
                    $objectType = 'InstitutionStructure';
                    $action = "Updated InstitutionStructure ";
                    \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                    return $this->redirect(['view', 'id' => $model->institution_structure_id]);
                }
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InstitutionStructure model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!Yii::$app->user->can('/institution-structure/delete')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
        $objectType = 'InstitutionStructure';
        $action = "Deleted InstitutionStructure" . ' ' . $model->institution_name . ' ' . "institution_structure_id =>" . $model->institution_structure_id;
        \app\models\UserAuditTrail::logAudit($action, $objectType);
        return $this->redirect(['index']);
    }

    /**
     * Finds the InstitutionStructure model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InstitutionStructure the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = InstitutionStructure::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
