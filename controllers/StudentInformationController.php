<?php

namespace app\controllers;

use Yii;
use app\models\PrimaryStudentYearOfStudy;
use app\models\PrimaryStudentYearOfStudySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\OlevelStudentYearOfStudy;
use app\models\OlevelStudentYearOfStudySearch;

/**
 * PrimaryStudentYearOfStudyController implements the CRUD actions for PrimaryStudentYearOfStudy model.
 */
class StudentInformationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PrimaryStudentYearOfStudy models.
     * @return mixed
     */
    public function actionIndex()
    {
      $user_id = Yii::$app->user->id;
        $school_id = \app\models\Users::findOne($user_id)->school_id;
        $school_type = \app\models\Schools::findOne($school_id)->school_type_id;
        
        if ($school_type == 1) {
              
       $searchModel = new PrimaryStudentYearOfStudySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('primary_students_index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'school_id'=>$school_id,
            'user_id'=>$user_id,
        ]);
           
        }
      if ($school_type == 2) {     
       $searchModel = new OlevelStudentYearOfStudySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('olevel_students_index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
             'school_id'=>$school_id,
             'user_id'=>$user_id,
        ]);
        
    } 
        
   
}


}
