<?php

namespace app\controllers;

use Yii;
use app\models\VwOlevelStudents;
use app\models\VwOlevelStudentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * VwOlevelStudentsController implements the CRUD actions for VwOlevelStudents model.
 */
class VwOlevelStudentsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VwOlevelStudents models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new VwOlevelStudentsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id, $activeTab = 'student-tab', $subaction = NULL, $academic_year_to_delete = NULL) {

        //$model = \app\models\PrimaryStudents::findOne($id);
        if ($subaction == 'delete_student_year_of_study') {
            $cy_id = \app\models\PrimaryStudentYearOfStudy::findOne($academic_year_to_delete)->cy_id;
            \app\models\StudentClassCourses::deleteAll("cy_id = {$cy_id}");
            \app\models\PrimaryStudentYearOfStudy::deleteAll(" primary_student_year_of_study_id = {$academic_year_to_delete}");
            $objectType = 'VwStudents';
            $action = "Deleted Student Year of Study " . ' ' . ' ' . " primary_student_year_of_study_id => " . $academic_year_to_delete;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            $this->redirect([
                'view', 'id' => $id, 'activeTab' => 'Years-tab'
            ]);
        }


        $model_study_yearSearch = new \app\models\OlevelStudentYearOfStudySearch();
        $condition = "user_id = {$id}";
        $model_study_yearDataProvider = $model_study_yearSearch->search(\Yii::$app->request->queryparams, $condition);

        $model = $this->findModel($id);
        $student_yearModel = new \app\models\OlevelStudentYearOfStudy();
        $student_yearModel->user_id = $id;

        if ($student_yearModel->load(Yii::$app->request->post())) {
            // Tracking all programme years for specific student  to enable validation 
            $count_study_years = \app\models\OlevelStudentYearOfStudy::find()->where("user_id = " . $student_yearModel->user_id . " and year_of_study  =  " . $student_yearModel->year_of_study)->count();

            if ($count_study_years > 0) {
                $student_yearModel->addError('year_of_study', "This year of study has been added");
            }

            $query = "select academic_year_id from class_year where cy_id IN (select cy_id from primary_student_year_of_study where user_id = {$id} ) AND academic_year_id = $student_yearModel->academic_year ";
            $count_academic_years = \app\models\ClassYear::findBySql($query)->count();
            if ($count_academic_years > 0) {
                $student_yearModel->addError("academic_year", "This academic year already selected");
            } else {
                $class_year = \app\models\ClassYear::findOne(['academic_year_id' => $student_yearModel->academic_year, 'study_year' => $student_yearModel->year_of_study]);
                if ($class_year == NULL) {
                    $student_yearModel->addError("year_of_study", "This year of study not yet configured");
                }
            }
            $student_yearModel->cy_id = $class_year->cy_id;

            if ($student_yearModel->validate(['year_of_study', 'academic_year'], false)) {
                $student_yearModel->student_year_status_id = 1;
                if ($student_yearModel->save(false)) {
                    $objectType = 'VwStudents';
                    $action = "Added Student Year of Study " . ' ' . ' ' . "student_year_of_study_id =>" . ' ' . $student_yearModel->primary_student_year_of_study_id . ' ' . ' ' . 'cy_id =>' . ' ' . ' ' . $student_yearModel->cy_id;
                    \app\models\UserAuditTrail::logAudit($action, $objectType);
                    $classes = \app\models\ClassYear::findAll(['cy_id' => $student_yearModel->cy_id]);
                    foreach ($classes as $class) {
                        $class_courses = \app\models\ClassYearOfStudyCourses::find()->where("cy_id = {$class->cy_id} and is_core = 1")->all();
                        foreach ($class_courses as $class_course) {
                            $student_class_course = new \app\models\StudentClassCourses();
                            $student_class_course->cy_id = $class_course->cy_id;
                            $student_class_course->cyos_course_id = $class_course->cyos_course_id;
                            $student_class_course->save(false);
                            $objectType = 'VwStudents';
                            $action = "Added Student Semester Course " . ' ' . ' ' . "student_year_of_study_id =>" . ' ' . $student_yearModel->primary_student_year_of_study_id . ' ' . ' ' . 'cy_id =>' . ' ' . ' ' . $student_yearModel->cy_id;
                            \app\models\UserAuditTrail::logAudit($action, $objectType);
                        }

                        return $this->redirect(['view', 'id' => $id, 'activeTab' => 'Years-tab',]);
                    }
                } else {
                    $activeTab = 'Years-tab';
                }
            }
        }
        return $this->render('view', ['model' => $model,
                    'model_study_yearSearch' => $model_study_yearSearch,
                    'model_study_yearDataProvider' => $model_study_yearDataProvider,
                    'student_yearModel' => $student_yearModel,
                    'activeTab' => $activeTab,
                    'user_id' => $id,
        ]);
    }

    /**
     * Creates a new VwPrimaryStudents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        $model = new \app\models\OlevelStudents;

        if ($model->load(Yii::$app->request->post())) {

            $model->image = UploadedFile::getInstance($model, 'image');
            $image_instance = $model->image;
            $model->student_status_id = 1;
            $model->username = $model->registration_number;
            $model->is_active = 1;
            $model->user_type = 3;
            $username_upper_case = strtoupper($model->surname);
            $model->surname = $username_upper_case;
            $password = Yii::$app->getSecurity()->generatePasswordHash($username_upper_case);
            $model->password = $password;
            $model->is_default_password = 1;

            $student_exist = count(\app\models\Users::find()->where("username = :username OR registration_number = :registration_number OR email_address LIKE '{$model->email_address}'", [':username' => $model->registration_number, ':registration_number' => $model->registration_number])->all());

            if ($student_exist >= 1) {
                $model->addError('registration_number', 'Either Reg#,E-mail or Username already taken!');
                return $this->render('create', [
                            'model' => $model,
                ]);
            }

            if ($image_instance != NULL) {

                $folder = 'uploads/profilepicture/';
                $output = static::Path_exist($folder);
                if ($output !== false) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if ($model->save(false)) {
                            $filepath = 'uploads/profilepicture/' . md5($model->user_id) . '.' . $model->image->extension;
                            $model->profile_picture = $filepath;
                            $model->save(false);
                            if ($model->image->saveAs($filepath)) {
                                $year_of_study = $model->year_of_study;
                                $academic_year_id = $model->academic_year;
                                $school_id = $model->school_id;
                                $model = \app\models\OlevelStudents::RegisterStudent($model, $school_id, $year_of_study, $academic_year_id);

                                if ($model->hasErrors()) {
                                    $transaction->rollBack();
                                } else {
                                    $transaction->commit();
                                    $objectType = 'VwStudents';
                                    $action = "Added Student " . ' ' . ' ' . "user_id =>" . $model->user_id;
                                    \app\models\UserAuditTrail::logAudit($action, $objectType);
                                    return $this->redirect(['view', 'id' => $model->user_id]);
                                }
                            }
                        }
                    } catch (\yii\db\Exception $exc) {
                        $model->addError('registration_number', "Error occured " . "$exc");
                        $transaction->rollBack();
                    }
                } else {

                    $model->addError('image', "The image save path do not exist in the System And is not writeable!");
                }
            } else {

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($model->save(false)) {

                        $year_of_study = $model->year_of_study;
                        $academic_year_id = $model->academic_year;
                        $school_id = $model->school_id;
                        $model = \app\models\OlevelStudents::RegisterStudent($model, $school_id, $year_of_study, $academic_year_id);

                        if ($model->hasErrors()) {
                            $transaction->rollBack();
                        } else {
                            $transaction->commit();
                            $objectType = 'VwStudents';
                            $action = "Added Student " . ' ' . ' ' . "user_id =>" . $model->user_id;
                            \app\models\UserAuditTrail::logAudit($action, $objectType);
                            return $this->redirect(['view', 'id' => $model->user_id]);
                        }
                    }
                } catch (\yii\db\Exception $exc) {
                    $model->addError('registration_number', "Error occured " . "$exc");
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing VwPrimaryStudents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id, $year_of_study, $academic_year_id) {


        $model = $this->findModel2($id);
        $school_id = $model->school_id;
        $oldModel = $model;
        if ($model->load(Yii::$app->request->post())) {
            //saving important information such that no user can temper values  ;
            $model->academic_year = $academic_year_id;
            $model->year_of_study = $year_of_study;
            $model->school_id = $school_id;
            $model->student_status_id = 1;
            $model->username = $model->registration_number;
            $model->is_active = 1;
            $model->user_type = 3;

            if (trim($model->email_address) == '') {

                $model->email_address = NULL;
            }


            $imageInstance = UploadedFile::getInstance($model, 'image');

            if ($imageInstance != NULL) {

                $model->image = UploadedFile::getInstance($model, 'image');
                $filepath = 'uploads/profilepicture/' . md5($model->user_id) . '.' . $model->image->extension;
                $model->profile_picture = $filepath;

                if ($model->save(false)) {
                    if ($model->image->saveAs($filepath)) {

                        return $this->redirect(['view', 'id' => $model->user_id,
                                    'academic_year_id' => $academic_year_id,
                                    'year_of_study' => $year_of_study,
                        ]);
                    }
                }
            } else {

                $model->save(false);
                $objectType = 'VwStudents';
                $action = "Updated Student Information ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                return $this->redirect(['view', 'id' => $model->user_id,
                            'academic_year_id' => $academic_year_id,
                            'year_of_study' => $year_of_study,
                ]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'academic_year_id' => $academic_year_id,
                        'year_of_study' => $year_of_study,
            ]);
        }
    }

    static function Path_exist($folder) {
        // Get canonicalized absolute pathname
        $path = realpath($folder);

        // If it exist, check if it's a directory
        return ($path !== false AND is_dir($path)) ? $path : false;
    }

    /**
     * Deletes an existing VwPrimaryStudents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VwPrimaryStudents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return VwPrimaryStudents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = VwOlevelStudents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModel2($id) {
        if (($model = \app\models\OlevelStudents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFormYears() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $academic_year = $parents[0];
                $out = \app\models\FormDescription::getFormYears($academic_year);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
    }

    public function actionAcademicYears() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $school_id = $parents[0];
                $out = \app\models\AcademicYears::getFormAcademicYears($school_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
    }

    public function actionUploadIndex() {

        $condition = " school_type_id = 2 ";
        $searchModel = new \app\models\SchoolsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
        return $this->render('_upload_index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionUploadAcademicYears($school_id) {
        $searchModel = new \app\models\AcademicYearsSearch();
        $condition = " academic_year_id IN (select academic_year_id from form_year where school_id = {$school_id}) ";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
        return $this->render('_upload_students', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'school_id' => $school_id,
        ]);
    }

    public function actionFormYearStudents($fy_id, $school_id) {
        $model = new \app\models\UploadStudents();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = \yii\web\UploadedFile::getInstance($model, 'file');


            if ($model->validate()) {

                $objPHPExcel = \PHPExcel_IOFactory::load($model->file->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $number_of_rows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

                $headerRow = 0;
                $columnPositions = array();
                $validColumns = ['REGISTRATION', 'FIRSTNAME', 'MIDDLENAME', 'SURNAME', 'SEX'];
                foreach ($sheetData as $rowData) {
                    $rowData = array_map('strtoupper', $rowData);
                    ++$headerRow;
                    if (in_array('REGISTRATION', $rowData)) {
                        foreach ($rowData as $key => $cellData) {
                            if (in_array(trim($cellData), $validColumns)) {
                                $columnPositions[trim($cellData)] = $key;
                            }
                        }
                        break;
                    }
                }

                if (count($columnPositions) < count($validColumns)) {
                    $model->addError("file", "Invalid Columns. Required: " . implode(",", $validColumns));
                }

                if ($model->validate(NULL, false)) {




                    $uploadReport = '
                                     <table border = "0">
                        <thead>
                        <tr>
                        <th>SN</th>
                        <th>FIRSTNAME</th>
                        <th>SURNAME</th>
                        <th>MIDDLENAME</th>
                        <th>REGISTRATION</th>
                        <th>SEX</th>
                        <th>REMARKS</th>
                        </tr>
                        </thead>
                        <tbody>';


                    //getting starting row;
                    $startingRow = $headerRow + 1;

                    $sn = 0;
                    for ($i = $startingRow; $i <= $number_of_rows; ++$i) {
                        $rowDataArray = $sheetData[$i];
                        $remarks = "";
                        $sn++;

                        $model_s = new \app\models\OlevelStudents();
                        $model_s->registration_number = $rowDataArray[$columnPositions['REGISTRATION']];
                        $model_s->firstname = $rowDataArray[$columnPositions['FIRSTNAME']];
                        $model_s->middlename = $rowDataArray[$columnPositions['MIDDLENAME']];
                        $model_s->surname = strtoupper($rowDataArray[$columnPositions['SURNAME']]);
                        $model_s->sex = strtoupper($rowDataArray[$columnPositions['SEX']]);
                        $password = Yii::$app->getSecurity()->generatePasswordHash($model_s->surname);
                        $model_s->password = $password;
                        $model_s->is_default_password = 1;
                        $model_s->user_type = 3;

                        if ($model_s->sex == 'M' || $model_s->sex == 'F') {
                            
                        } else {
                            $model_s->addError('sex', 'Sex not specified, Write M for Male and F for Female');
                        }

                        $transaction = Yii::$app->db->beginTransaction();
                        try {

                            $model_fy = \app\models\FormYear::findOne($fy_id);
                            $academic_year_id = $model_fy->academic_year_id;
                            $year_of_study = $model_fy->study_year;
                            $model_s = \app\models\OlevelStudents::RegisterStudent($model_s, $school_id, $year_of_study, $academic_year_id);
                            $objectType = 'Students';
                            $action = "Uploaded Students" . ' ' . 'Academic Year id => ' . ' ' . $academic_year_id . ' ' . 'Year Of Study => ' . ' ' . $year_of_study;
                            \app\models\UserAuditTrail::logAudit($action, $objectType);

                            if ($model_s->hasErrors()) {
                                $transaction->rollBack();
                                $error_string = '';
                                $errors = $model_s->getErrors();
                                foreach ($errors as $error1) {
                                    foreach ($error1 as $error2) {
                                        $error_string .= $error2 . "; ";
                                    }
                                }
                                $remarks = "Not Uploaded" . ' ' . $error_string;
                            } else {
                                $transaction->commit();
                                $remarks = "Uploaded";
                            }
                        } catch (Exception $exc) {
                            $model_s->addError('user_id', $exc->getTraceAsString());
                            $transaction->rollBack();
                        }



                        $uploadReport .= "<tr>
                <td>{$sn}</td>
                <td>$model_s->firstname</td>
                <td>$model->surname</td>
                <td>$model_s->middlename</td>
                <td>$model_s->registration_number</td>
                <td>$model_s->sex</td>
                <td>$remarks</td>
                </tr>";
                    }
                    $uploadReport .= ' </tbody>
                                      </table>';



                    //saveistory

                    $model_upload_history = new \app\models\StudentsUploadHistory();
                    $model_upload_history->user_id = Yii::$app->user->id;
                    $model_upload_history->fy_id = $fy_id;
                    $model_upload_history->ip_address = Yii::$app->getRequest()->getUserIP();
                    $model_upload_history->client_details = Yii::$app->getRequest()->getUserAgent();
                    $model_upload_history->upload_report = $uploadReport;
                    if ($model_upload_history->save(false)) {
                        $model->file = \yii\web\UploadedFile::getInstance($model, 'file');
                        $filepath = 'uploads/students_upload_history/' . md5($model_upload_history->students_upload_history_id) . '.' . $model->file->extension;
                        $model_upload_history->uploaded_file = $filepath;
                        if ($model_upload_history->save(false)) {
                            $model->file->saveAs($filepath);
                            $this->redirect(['/vw-olevel-students/form-year-students', 'fy_id' => $fy_id, 'school_id' => $school_id]);
                        }
                    }
                }
            }
        }
        $searchModel = new \app\models\VwOlevelStudentsSearch();
        $condition = "fy_id = {$fy_id}";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

        return $this->render('_olevel_students', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'fy_id' => $fy_id,
                    'model' => $model,
                    'school_id' => $school_id,
        ]);
    }

    public function actionStudentsUploadExcelTemplate($py_id) {
        if (!Yii::$app->user->can('/students/students-upload-excel-template')) {
            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
        }
        echo $this->renderAjax('_students_upload_excel_template', ['py_id' => $py_id]);
    }

}
