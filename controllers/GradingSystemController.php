<?php

namespace app\controllers;

use Yii;
use app\models\GradingSystem;
use app\models\GradingSystemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\filters\AccessControl;

/**
 * GradingSystemController implements the CRUD actions for GradingSystem model.
 */
class GradingSystemController extends Controller {

    public function behaviors() {
        return [
                                'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view','create','delete','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GradingSystem models.
     * @return mixed
     */
    public function actionIndex($grading_system_version_id) {
//       if (!Yii::$app->user->can('/grading-system/index')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }

        $model_gs = new \app\models\GradingSystem();
        $model_gs->grading_system_version_id = $grading_system_version_id;
        if ($model_gs->load(Yii::$app->request->post()) && $model_gs->save()) {
           $objectType = 'GradingSystem';
           $action = "Added Grading System id =>".' '.$model_gs->grading_system_id.' '."grading_system_version_id =>".$grading_system_version_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['index', 'grading_system_version_id' => $grading_system_version_id,
            ]);
        }

        $searchModel = new GradingSystemSearch;
        $condition = "grading_system_version_id = $grading_system_version_id";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
        if (Yii::$app->request->post('hasEditable') && isset($_POST['GradingSystem'])) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $message = '';
            $out = '';
            $record_id = Yii::$app->request->post('editableKey');
            $model1 = \app\models\GradingSystem::findOne($record_id);
            $oldModel = $model1;
            $post = [];
            $posted = current($_POST['GradingSystem']);
            $post['GradingSystem'] = $posted;

            if ($model1->load($post)) {
                if (!$model1->save(false)) {
                    $message = 'Error occured';
                } else {
                    
                  $objectType = 'GradingSystem';
                 $action = "Updated GradingSystem ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model1);
                    $out = '';
                }
            } else {
                $message = 'Error occured';
            }
            return ['output' => $out, 'message' => $message];
        }

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'grading_system_version_id' => $grading_system_version_id,
                    'model_gs' => $model_gs,
        ]);
    }

    /**
     * Displays a single GradingSystem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//    if (!Yii::$app->user->can('/grading-system/view')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->grading_system_id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new GradingSystem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($grading_system_version_id) {
//       if (!Yii::$app->user->can('/grading-system/create')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = new GradingSystem;
        if ($model->load(Yii::$app->request->post())) {
            $model->grading_system_version_id = $grading_system_version_id;
            if ($model->save()) {
           $objectType = 'GradingSystem';
           $action = "Created GradingSystem id => ".' '.$model->grading_system_id.' '."grading_system_version_id =>".$grading_system_version_id;
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                return $this->redirect(['index', 'grading_system_version_id' => $grading_system_version_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing GradingSystem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
//       if (!Yii::$app->user->can('/grading-system/update')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
        $model = $this->findModel($id);
           $oldModel = $model;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           $objectType = 'GradingSystem';
          $action = "Updated GradingSystem ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->grading_system_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing GradingSystem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
//        if (!Yii::$app->user->can('/grading-system/delete')) {
//             throw  new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator'); 
//        }
          $model=  $this->findModel($id);
         $this->findModel($id)->delete();
           $objectType = 'GradingSystem';
           $action = "Deleted GradingSystem id => ".' '.$model->grading_system_id;
             \app\models\UserAuditTrail::logAudit($action, $objectType);
        return $this->redirect(['index']);
    }

    /**
     * Finds the GradingSystem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GradingSystem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = GradingSystem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
