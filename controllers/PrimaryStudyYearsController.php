<?php

namespace app\controllers;

use Yii;
use app\models\Programmes;
use app\models\ProgrammesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ProgrammeYear;
use app\models\ProgrammeYearSemesters;
use app\models\ProgrammeYearSemesterCourses;
use app\models\InstitutionStructureSearch;
use app\models\StudentSemestersSearch;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use app\models\SchoolType;
use app\models\SchoolTypeSearch;

/**
 * StudyYearsController implements the CRUD actions for Programmes model.
 */
class PrimaryStudyYearsController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'delete', 'update', 'configure-study-year', 'results', 'semesters'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Programmes models.
     * @return mixed
     */
    public function actionIndex() {
        $condition = " school_type_id = 1 ";
        $searchModel = new \app\models\SchoolsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionConfigureStudyYear($academic_year_id, $school_id, $activeTab = 'class_year_details_tab', $cy_id = NULL, $study_year = NULL, $cyos_course_id = NULL, $SemActiveTab = NULL, $subaction = NULL, $autoOpen3 = false) {

        if (isset($_POST['hasEditable']) && $cyos_course_id != NULL) {
            $out = '';
            $message = '';
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            end($_POST);
            $key = key($_POST);
            $model = \app\models\ClassYearOfStudyCourses::findOne($cyos_course_id);
            $oldModel = $model;
            $model->$key = $_POST[$key];
            if ($model->validate([$key])) {
                $model->save(false);
                $objectType = 'primary-study-years';
                $action = "Updated primary study years configuration ";
                \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
                switch ($key) {

                    case 'pass_grade_id':
                        $out = \app\models\GradingSystem::findOne($model->$key)->grade;
                        break;

                    case 'contribute_to_final_result':
                        $out = $model->contribute_to_final_result == 1 ? "Yes" : "No";
                        break;

                    case 'is_core':
                        $out = $model->is_core == 1 ? "Core" : "Option";
                        break;
                    default:
                        break;
                }
            } else {
                $message = $model->getErrors($key);
                $out = $message;
            }

            return ['output' => $out, 'message' => $message];
        }


        if ($subaction == 'delete_course') {
            $oldModel = \app\models\ClassYearOfStudyCourses::findOne($cyos_course_id);
            \app\models\ClassYearOfStudyCourses::deleteAll(" cyos_course_id = :cyos_course_id", [":cyos_course_id" => $cyos_course_id]);
            $objectType = 'primary-study-years';
            $action = "Delete class year course, cyos_course_id =>  " . ' ' . $cyos_course_id;
            \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
        }


        $model_cy = new \app\models\ClassYear();
        if ($model_cy->load(Yii::$app->request->post())) {
            $model_cy->academic_year_id = $academic_year_id;
            $model_cy->year_number = $model_cy->study_year;
            $model_cy->school_id = $school_id;


            if (count(\app\models\ClassYear::find()->where(" academic_year_id = :academic_year_id   AND  study_year = :study_year  AND school_id = :school_id  ", [ ':academic_year_id' => $academic_year_id, ':study_year' => $model_cy->study_year, ':school_id' => $school_id])->all()) > 0) {
                $model_cy->addError('study_year', "This class already exist");
                return $this->render('configure-study-year', [
                            'academic_year_id' => $academic_year_id,
                            'model_cy' => $model_cy,
                            'activeTab' => $activeTab,
                            'SemActiveTab' => $SemActiveTab,
                            'school_id' => $school_id,
                ]);
            }
            if ($model_cy->validate(['academic_year_id', 'year_number', 'grading_system_version_id', 'study_year', 'school_id'])) {
                if ($model_cy->save(false)) {
                    $objectType = 'StudyYear';
                    $action = "Added ProgrammeYear , academic_year_id => " . ' ' . $academic_year_id . ' ' . ' ' . 'py_id => ' . ' ' . $model_cy->cy_id;
                    \app\models\UserAuditTrail::logAudit($action, $objectType);

                    return $this->redirect(['configure-study-year', 'academic_year_id' => $academic_year_id, 'activeTab' => 'cy_' . $model_cy->cy_id, 'school_id' => $school_id]);
                }
            } else {
                $activeTab = 'cy_add_year_tab';
            }
        }

        $model_acy = \app\models\AcademicYears::findOne($academic_year_id);

        $model_cyosc = new \app\models\ClassYearOfStudyCourses();

        if ($model_cyosc->load(Yii::$app->request->post())) {
            $model_cyosc->cy_id = $cy_id;
            $model_cyosc->school_id = $school_id;
            if (\app\models\ClassYearOfStudyCourses::find()->where("cy_id IN (SELECT cy_id FROM class_year WHERE cy_id = :cy_id  AND school_id = :school_id  AND study_year = :study_year ) AND course_offering_id = :course_offering_id", [':course_offering_id' => $model_cyosc->course_offering_id, ':cy_id' => $cy_id, ':school_id' => $school_id, ':study_year' => $study_year])->count() > 0) {
                $model_cyosc->addError("course_offering_id", "Course already exists in this class");
                return $this->render('configure-study-year', [
                            'academic_year_id' => $academic_year_id,
                            'model_acy' => $model_acy,
                            'model_cy' => $model_cy,
                            'activeTab' => $activeTab,
                            'SemActiveTab' => $SemActiveTab,
                            'model_cyosc' => $model_cyosc,
                            'school_id' => $school_id,
                ]);
            }

            if ($model_cyosc->validate(['cy_id', 'course_offering_id', 'pass_grade_id', 'contribute_to_final_result', 'is_core'])) {
                if ($model_cyosc->save(false)) {
                    $objectType = 'Primary-study-years';
                    $action = "Added class  Course , academic_year_id => " . ' ' . $academic_year_id . ' ' . "pintake_id =>" . ' ' . ' ' . 'cy_id => ' . ' ' . $cy_id . ' ' . ' ' . ' ' . 'course_offering_id => ' . ' ' . $model_cyosc->course_offering_id;
                    \app\models\UserAuditTrail::logAudit($action, $objectType);
                    return $this->redirect(['configure-study-year', 'academic_year_id' => $academic_year_id, 'school_id' => $school_id, 'activeTab' => 'cy_' . $cy_id]);
                } else {

                    $SemActiveTab = 'cy_add_year_tab';
                }
            }
        }

        return $this->render('configure-study-year', [
                    'academic_year_id' => $academic_year_id,
                    'model_acy' => $model_acy,
                    'model_cy' => $model_cy,
                    'activeTab' => $activeTab,
                    'SemActiveTab' => $SemActiveTab,
                    'model_cyosc' => $model_cyosc,
                    'school_id' => $school_id,
        ]);
    }

    /**
     * Displays a single Programmes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
//        if (!Yii::$app->user->can('/study-years/view')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->programme_id]);
        } else {
            return $this->render('view', ['model' => $model]);
        }
    }

    /**
     * Creates a new Programmes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//        if (!Yii::$app->user->can('/study-years/create')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        $model = new Programmes;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'Programmes';
            $action = "Added Programme" . ' ' . 'programme_id =>' . ' ' . $model->programme_id;
            \app\models\UserAuditTrail::logAudit($action, $objectType);
            return $this->redirect(['view', 'id' => $model->programme_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Programmes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
//        if (!Yii::$app->user->can('/study-years/update')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        $model = $this->findModel($id);
        $oldModel = $model;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $objectType = 'Programmes';
            $action = "Updated Programme ";
            \app\models\UserAuditTrail::logAudit($action, $objectType, $oldModel, $model);
            return $this->redirect(['view', 'id' => $model->programme_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Programmes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
//        if (!Yii::$app->user->can('/study-years/delete')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        $model = $this->findModel($id);
        $this->findModel($id)->delete();
        $objectType = 'Programmes';
        $action = "Deleted Programme" . ' ' . $model->programme_name . ' ' . "programme_id =>" . $model->programme_id;
        \app\models\UserAuditTrail::logAudit($action, $objectType);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Programmes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Programmes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Programmes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionResults() {
//        if (!Yii::$app->user->can('/study-years/results')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        $searchModel = new InstitutionStructureSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        return $this->render('results', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionSemesters($student_year_of_study_id, $py_id) {
//        if (!Yii::$app->user->can('/study-years/semesters')) {
//            throw new ForbiddenHttpException('You are not allowed to perform this action ! Contact your administrator');
//        }
        $condition2 = " student_year_of_study_id = {$student_year_of_study_id} ";
        $searchModel = new StudentSemestersSearch;
        $dataProvider = $searchModel->search(\yii::$app->request->queryParams, $condition2);
        return $this->render('semesters', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'py_id' => $py_id,
        ]);
    }

}
