<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Users;
use app\models\PasswordForm;
use yii\helpers\Url;
use app\models\Students;
use yii\helpers\Html;
use app\models\DefaultpasswdForm;
use app\models\Schools;
use app\models\SchoolsSearch;
use app\models\SchoolPrivacyStatement;
use app\models\SchoolPrivacyStatementSearch;
use app\models\HeadmasterStatement;
use app\models\HeadmasterStatementSearch;
use app\models\SchoolBoard;
use app\models\SchoolBoardSearch;
use app\models\BoardMembersStatement;
use app\models\BoardMembersStatementSearch;
use app\models\SchoolSponsors;
use app\models\SchoolSponsorsSearch;
use app\models\SchoolDonors;
use app\models\SchoolDonorsSearch;
use app\models\PagesSearch;

date_default_timezone_set('Africa/Nairobi');

class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

        if (!\Yii::$app->user->isGuest) {
            $id = Yii::$app->user->identity->id;
            $modeluser = Users::find()->where([
                        'user_id' => $id
                    ])->one();
            $default_password = $modeluser->is_default_password;
            $oldpasswd = $modeluser->password;
            if ($default_password == 1) {
                $model = new DefaultpasswdForm();
                if ($model->load(Yii::$app->request->post())) {

                    if ((Yii::$app->getSecurity()->validatePassword($model->newpass, $oldpasswd)) != TRUE) {

                        $new_password_hash = Yii::$app->getSecurity()->generatePasswordHash($model->newpass);
                        $modeluser->password = $new_password_hash;
                        $modeluser->is_default_password = 0;
                        if ($modeluser->save(false)) {
                            return $this->redirect(['/site/login']);
                        } else {
                            return $this->render('defaultpassword', ['model' => $model]);
                        }
                    } else {

                        \Yii::$app->session->setFlash('errorMessage', 'Please enter new password!');
                        $model->addError('newpass', 'New Password can not be the same as default password!');
                        return $this->render('defaultpassword', ['model' => $model]);
                    }
                } else {
                    return $this->render('defaultpassword', ['model' => $model]);
                }
            } else {
                return $this->render('index');
            }
        } else {
            //            return $this->redirect(['/site/login']);
            $searchModel = new SchoolsSearch;
            $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
            $this->layout = 'popup_layout';
            return $this->render('index_before_logging', [
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel,
            ]);
        }
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            /* Logs the Logins History */
            $loginsModel = new \app\models\Logins();
            $loginsModel->user_id = \yii::$app->user->identity->id;
            $loginsModel->ip_address = Yii::$app->getRequest()->getUserIP();
            $loginsModel->details = 'User logged into the system successful using browser :- ' . Yii::$app->getRequest()->getUserAgent();
            $loginsModel->save();
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        $userId = \yii::$app->user->identity->id;
        Yii::$app->user->logout();
        /* Logs the Logins History */
        $loginsModel = new \app\models\Logins();
        $loginsModel->user_id = $userId;
        $loginsModel->ip_address = Yii::$app->getRequest()->getUserIP();
        $loginsModel->details = 'User logged out the system successful using browser :- ' . Yii::$app->getRequest()->getUserAgent();
        $loginsModel->save();
        return $this->goHome();
    }

    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionReset() {

        $model = new \app\models\ResetPassword();
        if ($model->load(Yii::$app->request->post())) {
            $email = trim($model->E_mail);
            $phone_number = $model->Phone_number;
            $user_model = Users::find()->where("telephone_no = :phone_no  AND email_address = :email", ['phone_no' => $phone_number, 'email' => $email])->one();
            $id = $user_model->user_id;

            if ($id != NULL) {
                $secretKey = $model->Phone_number;
                $encryptedData = Yii::$app->getSecurity()->encryptByPassword($id, $secretKey);

                $message = $message . ' ' . 'Click this link to reset your Aris password! ' . \yii\helpers\Html::a('Reset', Yii::$app->urlManager->createAbsoluteUrl(
                                        ['site/passwordreset', 'verification' => $encryptedData, 'phone' => $phone_number]
                ));
                $subject = 'Aris: Reset Your Password';
                $mail = Yii::$app->mail->compose()
                        ->setFrom('query@aris.com')
                        ->setTo($model->E_mail)
                        ->setSubject($subject)
                        ->setHtmlBody($message)
                        ->send();

                if ($mail) {
                    \Yii::$app->session->setFlash('flashMessage', 'Check your E-mail!');
                    return $this->redirect(['/site/login']);
                } else {
                    \Yii::$app->session->setFlash('errorMessage', 'Enter correct detail and try again!');
                    $model->addError('Phone_number', 'Error Occured! Confirmation not sent!');
                    return $this->render('reset', [
                                'model' => $model,
                    ]);
                }
            } else {
                \Yii::$app->session->setFlash('failMessage', 'Please Contact Administrator!');
                $model->addError('Phone_number', 'Incorect E-mail Address or Mobile Phone Number!');
                return $this->render('reset', [
                            'model' => $model,
                ]);
            }
        }

        return $this->render('reset', [
                    'model' => $model,
        ]);
    }

    public function actionPasswordreset($verification, $phone) {
        $secretKey = $phone;
        $data = Yii::$app->getSecurity()->decryptByPassword($verification, $secretKey);
        $model = Users::findOne($data);
        if ($model->load(Yii::$app->request->post())) {
            $password = Yii::$app->getSecurity()->generatePasswordHash($model->newpass);
            $model->password = $password;
            $model->is_default_password = 1;
            if ($model->save(false)) {
                $objectType = 'ResetPassword';
                $action = " Changed Password";
                \app\models\UserAuditTrail::logAudit($action, $objectType);
                $date_time = date("l jS  F Y h:i:sa ");
                $subject = 'Aris: Reset Your Password';
                $message = 'Hi,' . $model->firstname . ' ' . $model->middlename . ' ' . $model->surname . ' ' . 'Your successfully changed your Aris Password! on ' . $date_time;
                Yii::$app->mail->compose()
                        ->setFrom('query@aris.com')
                        ->setTo($model->email_address)
                        ->setSubject($subject)
                        ->setTextBody($message)
                        ->send();
                \Yii::$app->session->setFlash('successMessage', ' Now login with your new Password!');
                return $this->redirect(['/site/login']);
            }
        } else {
            return $this->render('passwordreset', [
                        'model' => $model,
                        'id' => $model->user_id,
            ]);
        }
    }

    /**
     * Displays full details of a school and other related school detaisl.
     * @param integer $id
     * @return mixed
     */
    public function actionViewSchoolDetails($id, $school_privacy_statement_id = NULL, $subaction = NULL, $headmaster_statement_id = NULL, $school_board_id = NULL, $board_members_statement_id = NULL, $school_sponsor_id = NULL, $school_donor_id = NULL) {
        $model = Schools::findOne($id);
        $condition = "school_id = {$id}";
        $model_privacy_statement = new SchoolPrivacyStatement;
        $model_headmaster_statement = new HeadmasterStatement;
        $model_school_board = new SchoolBoard;
        $model_board_statement = new BoardMembersStatement;
        $model_sponsors = new SchoolSponsors;
        $model_donors = new SchoolDonors;

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        if ($subaction == 'deactivate' && $school_privacy_statement_id != NULL) {
            $model_privacy_statement = SchoolPrivacyStatement::findOne($school_privacy_statement_id);
            $model_privacy_statement->is_active = 0;
            $model_privacy_statement->save(false);
        }


        if ($subaction == 'activate' && $school_privacy_statement_id != NULL) {
            $model_privacy_statement = SchoolPrivacyStatement::findOne($school_privacy_statement_id);
            $model_privacy_statement->is_active = 1;
            $model_privacy_statement->save(false);
        }

        if ($subaction == 'delete' && $school_privacy_statement_id != NULL) {
            SchoolPrivacyStatement::deleteAll("school_privacy_statement_id = {$school_privacy_statement_id}");
        }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        if ($subaction == 'deactivate' && $headmaster_statement_id != NULL) {
            $model_headmaster_statement = HeadmasterStatement::findOne($headmaster_statement_id);
            $model_headmaster_statement->is_active = 0;
            $model_headmaster_statement->save(false);
        }


        if ($subaction == 'activate' && $headmaster_statement_id != NULL) {
            $model_headmaster_statement = HeadmasterStatement::findOne($headmaster_statement_id);
            $model_headmaster_statement->is_active = 1;
            $model_headmaster_statement->save(false);
        }

        if ($subaction == 'delete' && $headmaster_statement_id != NULL) {
            HeadmasterStatement::deleteAll(" headmaster_statement_id = {$headmaster_statement_id}");
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        if ($subaction == 'deactivate' && school_board_id != NULL) {
            $model_school_board = SchoolBoard::findOne($school_board_id);
            $model_school_board->is_active = 0;
            $model_school_board->save(false);
        }


        if ($subaction == 'activate' && school_board_id != NULL) {
            $model_school_board = SchoolBoard::findOne($school_board_id);
            $model_school_board->is_active = 1;
            $model_school_board->save(false);
        }

        if ($subaction == 'delete' && school_board_id != NULL) {

            if (\app\models\BoardMembers::deleteAll(" school_board_id = {$school_board_id} ")) {
                SchoolBoard::deleteAll(" school_board_id = {$school_board_id}");
            }
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@




        if ($subaction == 'deactivate_board_statement' && $board_members_statement_id != NULL) {
            $model_board_statement = BoardMembersStatement::findOne($board_members_statement_id);
            $model_board_statement->is_active = 0;
            $model_board_statement->save(false);
        }


        if ($subaction == 'activate_board_statement' && $board_members_statement_id != NULL) {
            $model_board_statement = BoardMembersStatement::findOne($board_members_statement_id);
            $model_board_statement->is_active = 1;
            $model_board_statement->save(false);
        }

        if ($subaction == 'delete_board_statement' && $board_members_statement_id != NULL) {
            BoardMembersStatement::deleteAll(" board_members_statement_id = {$board_members_statement_id}");
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        if ($subaction == 'deactivate_sponsor' && $school_sponsor_id != NULL) {
            $model_sponsors = SchoolSponsors::findOne($school_sponsor_id);
            $model_sponsors->is_visible = 0;
            $model_sponsors->save(false);
        }


        if ($subaction == 'activate_sponsor' && $school_sponsor_id != NULL) {
            $model_sponsors = SchoolSponsors::findOne($school_sponsor_id);
            $model_sponsors->is_visible = 1;
            $model_sponsors->save(false);
        }

        if ($subaction == 'delete_sponsor' && $school_sponsor_id != NULL) {
            SchoolSponsors::deleteAll(" school_sponsor_id = {$school_sponsor_id}");
        }


        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        if ($subaction == 'deactivate_donor' && $school_donor_id != NULL) {
            $model_donors = SchoolDonors::findOne($school_donor_id);
            $model_donors->is_visible = 0;
            $model_donors->save(false);
        }


        if ($subaction == 'activate_donor' && $school_donor_id != NULL) {
            $model_donors = SchoolDonors::findOne($school_donor_id);
            $model_donors->is_visible = 1;
            $model_donors->save(false);
        }

        if ($subaction == 'delete_donor' && $school_donor_id != NULL) {
            SchoolDonors::deleteAll(" school_donor_id = {$school_donor_id}");
        }

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        $searchModel_privacy_statement = new SchoolPrivacyStatementSearch;
        $dataProvider_privacy_statement = $searchModel_privacy_statement->search(Yii::$app->request->getQueryParams(), $condition);


        $searchModel_headmaster_statement = new HeadmasterStatementSearch;
        $dataProvider_headmaster_statement = $searchModel_headmaster_statement->search(Yii::$app->request->getQueryParams(), $condition);


        $searchModel_school_board = new SchoolBoardSearch;
        $dataProvider_school_board = $searchModel_school_board->search(Yii::$app->request->getQueryParams(), $condition);



        $searchModel_board_statement = new BoardMembersStatementSearch;
        $school_board_id_models = SchoolBoard::find()->where(" school_id = {$id} AND is_active = 1")->all();

        $school_board_ids_inclause = "(0";
        foreach ($school_board_id_models as $school_board_id_model) {
            $school_board_ids_inclause = $school_board_ids_inclause . ',' . $school_board_id_model->school_board_id;
        }
        $school_board_ids_inclause = $school_board_ids_inclause . ')';

        $condition2 = "school_board_id IN {$school_board_ids_inclause} ";
        $dataProvider_board_statement = $searchModel_board_statement->search(Yii::$app->request->getQueryParams(), $condition2);



        $searchModel_sponsors = new SchoolSponsorsSearch;
        $dataProvider_sponsors = $searchModel_sponsors->search(Yii::$app->request->getQueryParams(), $condition);


        $searchModel_donors = new SchoolDonorsSearch;
        $dataProvider_donors = $searchModel_donors->search(Yii::$app->request->getQueryParams(), $condition);


        $this->layout = 'popup_layout';
        return $this->render('view_school_details', ['model' => $model,
                    'dataProvider_privacy_statement' => $dataProvider_privacy_statement,
                    'searchModel_privacy_statement' => $searchModel_privacy_statement,
                    'model_privacy_statement' => $model_privacy_statement,
                    'dataProvider_headmaster_statement' => $dataProvider_headmaster_statement,
                    'searchModel_headmaster_statement' => $searchModel_headmaster_statement,
                    'model_headmaster_statement' => $model_headmaster_statement,
                    'searchModel_school_board' => $searchModel_school_board,
                    'dataProvider_school_board' => $dataProvider_school_board,
                    'model_school_board' => $model_school_board,
                    'model_board_statement' => $model_board_statement,
                    'searchModel_board_statement' => $searchModel_board_statement,
                    'dataProvider_board_statement' => $dataProvider_board_statement,
                    'model_sponsors' => $model_sponsors,
                    'searchModel_sponsors' => $searchModel_sponsors,
                    'dataProvider_sponsors' => $dataProvider_sponsors,
                    'model_donors' => $model_donors,
                    'searchModel_donors' => $searchModel_donors,
                    'dataProvider_donors' => $dataProvider_donors,
                    'school_id' => $id,
        ]);
    }

    public function actionAboutUs() {
        $condition = "name = 'about_page'";
        $searchModel = new PagesSearch;
        $dataProvider_about_us = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
        $this->layout = 'popup_layout';
        return $this->render('about_us',['dataProvider_about_us' => $dataProvider_about_us]);
    }

}
