<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Donors $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="donors-form">

    <?php 
    $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' => [

'donor_name'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Donor Name...', 'maxlength'=>250]], 

//'donor_description'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Donor Description...','rows'=> 6]], 

'address'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Address...', 'maxlength'=>250]], 

    ]


    ]);
    
     $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'donor_description'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Donor Description...','rows'=> 6]], 


    ]


    ]);
     
      $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 3,
    'attributes' => [


'e_mail'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter E Mail...', 'maxlength'=>250]], 

'phone_no'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Phone No...', 'maxlength'=>250]], 

'fax'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fax...', 'maxlength'=>250]], 


    ]


    ]);
      
         $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' => [

'logo'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Logo...', 'maxlength'=>250]], 

'is_active'=>['type'=> Form::INPUT_CHECKBOX, 'options'=>['placeholder'=>'Enter Is Active...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
