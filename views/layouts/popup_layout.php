<script language="javascript" type="text/javascript">
    function clearText(field)
    {
        if (field.defaultValue == field.value)
            field.value = '';
        else if (field.value == '')
            field.value = field.defaultValue;
    }
</script>

<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;
use yii\helpers\Url;
use app\models\Users;
use kartik\tabs\TabsX;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">   

        <?= Html::csrfMetaTags() ?>
        <title><?php echo Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>

        <?php $this->beginBody() ?>
        <!DOCTYPE html>
    <link href="css/tooplate_style.css" rel="stylesheet" type="text/css" />
    <div id="tooplate_header_wrapper">
        <div id="tooplate_header">

            <div id="site_title">
                <h1><a href="<?php echo Url::to(['/site/index']); ?>" rel="nofollow"><img src="images/tooplate_logo.png" alt="logo" /></a></h1>
            </div> <!-- end of site_title -->

            <div id="header_right">

                <div id="social_box">
                    <ul>
                        <li><a href="#"><img src="images/facebook.png" alt="facebook" /></a></li>
                        <li><a href="#"><img src="images/twitter.png" alt="twitter" /></a></li>
                        <li><a href="#"><img src="images/linkedin.png" alt="linkin" /></a></li>
                        <li><a href="#"><img src="images/technorati.png" alt="technorati" /></a></li>
                        <li><a href="#"><img src="images/myspace.png" alt="myspace" /></a></li>                
                    </ul>
                    <div class="cleaner"></div>
                </div>   

                <!--                <div id="search_box">
                                    <form action="#" method="get">
                                        <input type="text" value="Search" name="q" size="10" id="searchfield" title="searchfield" onfocus="clearText(this)" onblur="clearText(this)" />
                                        <input type="submit" name="Search" value="" id="searchbutton" title="Search" />
                                    </form>
                                </div>-->

            </div>

        </div>
    </div>
    <div id="tooplate_middle_wrapper">
        <div id="tooplate_middle">

            <img src="images/tooplate_image_01.jpg" alt="Image 01" />

            <div id="middle_content">
                <h3>Parent Teacher Communication System|PTCS</h3>
                <p>We bridge the communication gap between parents and teachers. We provide all required information of the student to his /her parent on time. Our goal is to make it as easy as possible for parents to get the information.</p>
                <div class="wwu_btn"><a href="#"></a></div>
            </div>

        </div>
    </div>

    <div id="tooplate_menu">

        <ul>
            <li><a href="<?php echo Url::to(['/site/index']); ?>" class="current">Home</a></li>
            <li><a href="<?php echo Url::to(['/site/about-us']); ?>">About Us</a></li>
            <li><a href="<?php echo Url::to(['site/index']); ?>">Privacy</a></li>
            <li><a href="<?php echo Url::to(['site/index']); ?>">Mission&Vision</a></li>
            <li><a href="<?php echo Url::to(['site/index']); ?>">Gallery</a></li>            
            <li><a href="<?php echo Url::to(['site/index']); ?>">Advertisements</a></li>
            <li><a href="<?php echo Url::to(['site/index']); ?>">Contact</a></li>
            <li><a href="<?php echo Url::to(['/site/login']); ?>">Login</a></li>
        </ul>    	

        <div class="cleaner"></div>
    </div> <!-- end of templatetooplate_menu -->

    <div id="tooplate_content">

            <?= $content ?>

        <div id="services">
            <div class="col_w300">
                <h3 class="service1">Advert One</h3>
                <p>Nunc lobortis, mi a pellentesque posuere, lorem felis vulputate lectus, sit amet bibendum.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="col_w300">
                <h3 class="service2">Advert Two</h3>
                <p>Suspendisse vitae velit augue. Quisque a cursus purus. Suspendisse eros orci bibendum in leo.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="col_w300">
                <h3 class="service2">Advert Three</h3>
                <p>Suspendisse vitae velit augue. Quisque a cursus purus. Suspendisse eros orci bibendum in leo.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="col_w300 last_col">
                <h3 class="service3">Advert Four</h3>
                <p>Maecenas venenatis bibendum metus eget blandit. Nam leo justo, convallis nec aliquet zfsdfsd.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>
            <div class="cleaner"></div>
        </div>

        <?php
        echo TabsX::widget([
            'items' => [
                [
                    'label' => ' ' . 'About us',
                    'content' => app\models\Pages::findOne(['name' => 'about_page'])->body,
                    'options' => ['id' => 'about_us-tab'],
                    'active' => true,
                ],
                [
                    'label' => ' ' . 'Privacy',
                    'content' => app\models\Pages::findOne(['name' => 'privacy_page'])->body,
                    'options' => ['id' => 'privacy-tab'],
                    'active' => false,
                ],
                [
                    'label' => ' ' . 'Mission&Vision',
                    'content' => app\models\Pages::findOne(['name' => 'mission_vision_page'])->body,
                    'options' => ['id' => 'mission_vision-tab'],
                    'active' => false,
                ],
                [
                    'label' => ' ' . 'Gallery',
                    'content' => 'Gallery goes here',
                    'options' => ['id' => 'gallery-tab'],
                    'active' => false,
                ],
            ],
            'bordered' => true,
            'align' => TabsX::ALIGN_LEFT,
            'position' => TabsX::POS_ABOVE,
            'height' => TabsX::SIZE_SMALL,
        ]);
        ?>
        <br/>
        <div class="content_box content_box_last">
            <div class="col_w460">
                <!--                <h3> WHO WE ARE?</h3>-->
                <div class="image_wrapper image_fl"><img src="images/who_we_are.jpg" alt="Who we are?" /></div>

<!--                <p><em>Nam porta mauris nec odio sollicitudin sodales urna in nulla commodo luctus.</em></p>
                <p>Corporate Blue is a free <a rel="nofollow" href="http://www.tooplate.com" target="_parent">website template</a> for everyone and it can be used for any purpose. Credits go to <a rel="nofollow" href="http://www.photovaco.com/" target="_blank">Free Photos</a> for photos and <a rel="nofollow" href="http://www.brusheezy.com/brush/1407-Solid-Tech-Rings" target="_blank">Photoshop Tutorials</a> for Solid Tech Rings Brush.</p>-->

                <div class="cleaner_h20"></div>
                <!--                <ul class="tooplate_list">
                                    <li>Sapien odio iaculis dui</li>
                                    <li>Curabitur ullamcorper</li>
                                    <li>Pellentesque adipiscing</li>
                                    <li>Molestie sit amet nisi</li>
                                </ul>-->
                <div class="btn_more" style="float:right"><a href="#">Who we are?</a></div>
            </div>

            <div class="col_w460">
                <!--                <h3> WHAT WE DO?</h3>-->
                <div class="image_wrapper image_fl"><img src="images/what_we_do.jpg" alt="What we do?" /></div>
<!--                <p><em>Nam porta mauris nec odio sollicitudin sodales urna in nulla commodo luctus.</em></p>
                <p>Corporate Blue is a free <a rel="nofollow" href="http://www.tooplate.com" target="_parent">website template</a> for everyone and it can be used for any purpose. Credits go to <a rel="nofollow" href="http://www.photovaco.com/" target="_blank">Free Photos</a> for photos and <a rel="nofollow" href="http://www.brusheezy.com/brush/1407-Solid-Tech-Rings" target="_blank">Photoshop Tutorials</a> for Solid Tech Rings Brush.</p>-->

                <div class="cleaner_h20"></div>
                <!--                <ul class="tooplate_list">
                                    <li>Sapien odio iaculis dui</li>
                                    <li>Curabitur ullamcorper</li>
                                    <li>Pellentesque adipiscing</li>
                                    <li>Molestie sit amet nisi</li>
                                </ul>-->

                <div class="btn_more" style="float:right"><a href="#">What we Do?</a></div>
            </div>

            <div class="col_w460">
                <!--                <h3> WORK WITH US</h3>-->
                <div class="image_wrapper image_fl"><img src="images/work_with_us.jpg" alt="Work with us" /></div>
<!--                <p><em>Nam porta mauris nec odio sollicitudin sodales urna in nulla commodo luctus.</em></p>
                <p>Corporate Blue is a free <a rel="nofollow" href="http://www.tooplate.com" target="_parent">website template</a> for everyone and it can be used for any purpose. Credits go to <a rel="nofollow" href="http://www.photovaco.com/" target="_blank">Free Photos</a> for photos and <a rel="nofollow" href="http://www.brusheezy.com/brush/1407-Solid-Tech-Rings" target="_blank">Photoshop Tutorials</a> for Solid Tech Rings Brush.</p>-->

                <div class="cleaner_h20"></div>
                <!--                <ul class="tooplate_list">
                                    <li>Sapien odio iaculis dui</li>
                                    <li>Curabitur ullamcorper</li>
                                    <li>Pellentesque adipiscing</li>
                                    <li>Molestie sit amet nisi</li>
                                </ul>-->

                <div class="btn_more" style="float:right"><a href="#">Work with us...</a></div>
            </div>

            <div class="col_w460 last_col">
                <!--                <h3>WHERE WE ARE?</h3>-->
                <div class="image_wrapper"><img src="images/where_we_are.jpg" alt="Where we are?" /></div>
                <!--                <p><em>Nam porta mauris nec odio sollicitudin sodales urna in nulla commodo luctus.</em></p>
                <p>Corporate Blue is a free <a rel="nofollow" href="http://www.tooplate.com" target="_parent">website template</a> for everyone and it can be used for any purpose. Credits go to <a rel="nofollow" href="http://www.photovaco.com/" target="_blank">Free Photos</a> for photos and <a rel="nofollow" href="http://www.brusheezy.com/brush/1407-Solid-Tech-Rings" target="_blank">Photoshop Tutorials</a> for Solid Tech Rings Brush.</p>-->

                <div class="cleaner_h20"></div>
                <!--                <ul class="tooplate_list">
                                    <li>Sapien odio iaculis dui</li>
                                    <li>Curabitur ullamcorper</li>
                                    <li>Pellentesque adipiscing</li>
                                    <li>Molestie sit amet nisi</li>
                                </ul>-->

                <div class="btn_more" style="float:right"><a href="#">Where we are?</a></div>
            </div>

            <div class="cleaner"></div>  
        </div>
        <div id="services">
            <div class="col_w300">
                <h3 class="service1">Advert Five</h3>
                <p>Nunc lobortis, mi a pellentesque posuere, lorem felis vulputate lectus, sit amet bibendum.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="col_w300">
                <h3 class="service2">Advert Six</h3>
                <p>Suspendisse vitae velit augue. Quisque a cursus purus. Suspendisse eros orci bibendum in leo.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="col_w300">
                <h3 class="service2">Advert Seven</h3>
                <p>Suspendisse vitae velit augue. Quisque a cursus purus. Suspendisse eros orci bibendum in leo.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="col_w300 last_col">
                <h3 class="service3">Advert Eight</h3>
                <p>Maecenas venenatis bibendum metus eget blandit. Nam leo justo, convallis nec aliquet zfsdfesa.</p>
                <div class="btn_more" style="float:right"><a href="#">Click Here For More...</a></div>
            </div>

            <div class="cleaner"></div>
        </div>

        <div class="cleaner"></div>
    </div>

    <div id="tooplate_footer_wrapper">

        <div id="tooplate_footer">

            Copyright © <?php echo date('Y'); ?> <a href="#">PTCS</a>

        </div> 
        <!-- end of templatetooplate_footer -->
    </div> 

    <?php
    $this->endBody()
    ?>
</body>
</html>
<?php $this->endPage() ?>
