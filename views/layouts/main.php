<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;
use yii\helpers\Url;
use app\models\Users;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$model = Users::findOne(\yii::$app->user->identity->id);
if ($model->profile_picture == NULL) {
    $picture = 'default-no-image/default-no-profile-image.png';
} else {
    $picture = $model->profile_picture;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>

        <?php $this->beginBody() ?>
        <!DOCTYPE html>

    <link href="css/dashboard.css" rel="stylesheet">
    <script src="js/ie-emulation-modes-warning.js"></script>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Yii::$app->homeUrl; ?>"><b>Parent Teachers Communication System</b></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">

                    <!--                    <li><a href="#">My Profile</a></li>-->
                    <li><a href="#">Help</a></li>
                    <?php
                    if (Yii::$app->user->isGuest) {
                        echo '<li><a href="' . Url::to(['site/login']) . '">Login</a></li>';
                    } else {
                        echo '<li><a href="' . Url::to(['site/logout']) . '">Logout(' . Yii::$app->user->identity->username . ')</a></li>';
                    }
                    ?>
                </ul>
                <!--
                 <form class="navbar-form navbar-right">
                   <input type="text" class="form-control" placeholder="Search...">
                 </form>
                -->
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <?php
//                    if (Yii::$app->user->isGuest) {
//                        echo SideNav::widget([
//                            'type' => SideNav::TYPE_DEFAULT,
//                            //'encodeLabels' => false,
//                            //'heading' => 'Navigation',vw-students-current-year
//                            'items' => [
//
//                                ['label' => 'Login', 'icon' => 'home', 'url' => Url::to(['/site/index']), 'active' => (Yii::$app->controller->id == 'site')],
//                                ['label' => 'Find your nearby garage', 'icon' => 'glyphicon glyphicon-map-marker', 'url' => Url::to(['/garages/index']), 'active' => (Yii::$app->controller->id == 'garages'),
//                                // 'visible' => yii::$app->user->identity->user_type == 1,
//                                ],
//                                ['label' => 'Sign Up', 'icon' => 'glyphicon glyphicon-user', 'url' => Url::to(['/create-account/index']), 'active' => (Yii::$app->controller->id == 'create-account'),
//                                // 'visible' => yii::$app->user->identity->user_type == 1,
//                                ],
//                            ],
//                        ]);
//                    }
                    ?>
                    <?php
                    if ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->is_default_password == 0) && (Yii::$app->user->identity->is_active == 1)) {

                        if ($model->is_active == 1) {
                            
                        }
                        ?>
                        <img  class="img-circle" src=" <?= $picture ?>" width=100 height=100" style="margin-left: 20%" alt=" Upload Profile Photo">

                        <?php
                        if ((!Yii::$app->user->isGuest) && (Yii::$app->user->identity->is_default_password == 0) && (Yii::$app->user->identity->is_active == 1)) {
                            echo SideNav::widget([
                                'type' => SideNav::TYPE_DEFAULT,
                                //'encodeLabels' => false,
                                //'heading' => 'Navigation',vw-students-current-year
                                'items' => [

                                    ['label' => 'Home', 'icon' => 'home', 'url' => Url::to(['/site/index']), 'active' => (Yii::$app->controller->id == 'site')],
                                    ['label' => ' My Profile', 'icon' => 'user', 'url' => Url::to(['/users/profile']), 'active' => (Yii::$app->controller->id == 'users' && (yii::$app->controller->action->id) == 'profile'),
                                    ],
                                    ['label' => 'Manage Students',
                                        'icon' => 'education',
                                        'visible' => yii::$app->user->identity->user_type == 1,
                                        'items' => [
                                            ['label' => 'Primary Students', 'url' => Url::to(['/vw-primary-students/index']), 'active' => (Yii::$app->controller->id == 'vw-primary-students'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                            ['label' => 'Olevel Students', 'url' => Url::to(['/vw-olevel-students/index']), 'active' => (Yii::$app->controller->id == 'vw-olevel-students'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                        ]],
                                    ['label' => 'Schools', 'icon' => 'glyphicon glyphicon-th-large', 'url' => Url::to(['/schools/index']), 'active' => (Yii::$app->controller->id == 'schools'),
                                        'visible' => yii::$app->user->identity->user_type == 1,
                                    ],
                                      ['label' => 'Course Results', 'icon' => 'glyphicon glyphicon-list', 'url' => Url::to(['/student-information/index']), 'active' => (Yii::$app->controller->id == 'student-information'),
                                        'visible' => yii::$app->user->identity->user_type == 3 ||  yii::$app->user->identity->user_type == 5 ,
                                    ],
//                                    ['label' => 'Inbox', 'icon' => 'glyphicon glyphicon-comment', 'url' => Url::to(['/inbox/index']), 'active' => (Yii::$app->controller->id == 'inbox'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'School Board', 'icon' => 'glyphicon glyphicon-tasks', 'url' => Url::to(['/school-board/index']), 'active' => (Yii::$app->controller->id == 'school-board'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'Privacy Statement', 'icon' => ' glyphicon glyphicon-list-alt', 'url' => Url::to(['/school-privacy-statement/index']), 'active' => (Yii::$app->controller->id == 'school-privacy-statement'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'Headmaster Statement', 'icon' => 'glyphicon glyphicon-folder-open', 'url' => Url::to(['/headmaster-statement/index']), 'active' => (Yii::$app->controller->id == 'headmaster-statement'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'Sponsors', 'icon' => 'glyphicon glyphicon-paperclip', 'url' => Url::to(['/school-sponsors/index']), 'active' => (Yii::$app->controller->id == 'school-sponsors'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'Donors', 'icon' => 'glyphicon glyphicon-stats', 'url' => Url::to(['/school-donors/index']), 'active' => (Yii::$app->controller->id == 'school-donors'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'Board Statement', 'icon' => 'glyphicon glyphicon-book', 'url' => Url::to(['/board-members-statement/index']), 'active' => (Yii::$app->controller->id == 'board-members-statement'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
//                                    ['label' => 'Inbox', 'icon' => 'glyphicon glyphicon-comment', 'url' => Url::to(['/inbox/index']), 'active' => (Yii::$app->controller->id == 'inbox'),
//                                        'visible' => yii::$app->user->identity->user_type == 1,
//                                    ],
                                    ['label' => 'Summary',
                                        'icon' => 'refresh',
                                        'visible' => false,
                                        'items' => [
                                            ['label' => 'School Boards', 'icon' => 'glyphicon glyphicon-tasks', 'url' => Url::to(['/school-board/index']), 'active' => (Yii::$app->controller->id == 'school-board'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                            ['label' => 'Privacy Statements', 'icon' => ' glyphicon glyphicon-list-alt', 'url' => Url::to(['/school-privacy-statement/index']), 'active' => (Yii::$app->controller->id == 'school-privacy-statement'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                            ['label' => 'Headmaster Statements', 'icon' => 'glyphicon glyphicon-folder-open', 'url' => Url::to(['/headmaster-statement/index']), 'active' => (Yii::$app->controller->id == 'headmaster-statement'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                            ['label' => 'School Sponsors', 'icon' => 'glyphicon glyphicon-paperclip', 'url' => Url::to(['/school-sponsors/index']), 'active' => (Yii::$app->controller->id == 'school-sponsors'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                            ['label' => 'School Donors', 'icon' => 'glyphicon glyphicon-stats', 'url' => Url::to(['/school-donors/index']), 'active' => (Yii::$app->controller->id == 'school-donors'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                            ['label' => 'Board Statements', 'icon' => 'glyphicon glyphicon-book', 'url' => Url::to(['/board-members-statement/index']), 'active' => (Yii::$app->controller->id == 'board-members-statement'),
                                                'visible' => yii::$app->user->identity->user_type == 1,
                                            ],
                                        ]],
                                    ['label' => 'Correspondences',
                                        'icon' => 'glyphicon glyphicon-comment',
                                        'visible' => yii::$app->user->identity->user_type == 1,
                                        'items' => [

                                            ['label' => 'Compose', 'url' => Url::to(['/inbox/create']), 'active' => ((yii::$app->controller->id) == 'inbox'),
                                            //'visible' => Yii::$app->user->can('/inbox/create'),
                                            ],
                                            ['label' => 'Mailbox', 'url' => Url::to(['/inbox/index']), 'active' => ((yii::$app->controller->id) == 'inbox'),
                                            //'visible' => Yii::$app->user->can('/inbox/index'),
                                            ],
                                        ]],
                                    ['label' => 'Configurations',
                                        'icon' => 'cog',
                                        'visible' => yii::$app->user->identity->user_type == 1,
                                        'items' => [

                                            ['label' => 'Course offered ', 'url' => Url::to(['/course-offering-versions/index']), 'active' => ((yii::$app->controller->id) == 'course-offering-versions'),
                                            //'visible' => Yii::$app->user->can('/course-offering-versions/index'),
                                            ],
                                            ['label' => 'Grading Score System', 'url' => Url::to(['/grading-system-versions/index']), 'active' => ((yii::$app->controller->id) == 'grading-system-versions'),
                                            //'visible' => Yii::$app->user->can('/grading-system-versions/index'),
                                            ],
                                            ['label' => 'Primary Study Years', 'url' => Url::to(['/primary-study-years/index']), 'active' => (yii::$app->controller->id == 'primary-study-years'),
                                            //'visible' => Yii::$app->user->can('/study-years/index'),
                                            ],
                                            ['label' => 'Olevel Study Years', 'url' => Url::to(['/olevel-study-years/index']), 'active' => (yii::$app->controller->id == 'olevel-study-years'),
                                            //'visible' => Yii::$app->user->can('/study-years/index'),
                                            ],
                                        ]],
                                    ['label' => 'System Setup',
                                        'icon' => 'compressed',
                                        'visible' => yii::$app->user->identity->user_type == 1,
                                        'items' => [
                                            ['label' => 'Manage Pages', 'url' => Url::to(['/pages/index']), 'active' => (( Yii::$app->controller->id) == 'pages'),],
                                            ['label' => 'Classes', 'url' => Url::to(['/class-description/index']), 'active' => (( Yii::$app->controller->id) == 'class-description'),],
                                            ['label' => 'Forms', 'url' => Url::to(['/form-description/index']), 'active' => (( Yii::$app->controller->id) == 'form-description'),],
                                            ['label' => 'Academic Years', 'url' => Url::to(['/academic-years/index']), 'active' => (( Yii::$app->controller->id) == 'academic-years'),],
                                            ['label' => 'Courses', 'url' => Url::to(['/courses/index']), 'active' => (( Yii::$app->controller->id) == 'courses'),],
                                            ['label' => 'Regions', 'url' => Url::to(['/regions/index']), 'active' => (( Yii::$app->controller->id) == 'regions'),],
                                            ['label' => 'Districts', 'url' => Url::to(['/districts/index']), 'active' => (( Yii::$app->controller->id) == 'districts'),],
                                            ['label' => 'Wards', 'url' => Url::to(['/wards/index']), 'active' => (( Yii::$app->controller->id) == 'wards'),],
                                            ['label' => 'Streets/Villages', 'url' => Url::to(['/streets-village/index']), 'active' => (( Yii::$app->controller->id) == 'streets-village'),],
                                            ['label' => 'Years', 'url' => Url::to(['/years/index']), 'active' => (( Yii::$app->controller->id) == 'years'),],
                                            ['label' => 'User Types', 'url' => Url::to(['/user-types/index']), 'active' => (( Yii::$app->controller->id) == 'user-types'),],
                                            ['label' => 'School Type', 'url' => Url::to(['/school-type/index']), 'active' => (( Yii::$app->controller->id) == 'school-type'),],
                                            ['label' => 'Sponsors', 'url' => Url::to(['/sponsors/index']), 'active' => (( Yii::$app->controller->id) == 'sponsors'),],
                                            ['label' => 'Donors', 'url' => Url::to(['/donors/index']), 'active' => (( Yii::$app->controller->id) == 'donors'),],
                                            ['label' => 'Staff Positions', 'url' => Url::to(['/staff-positions/index']), 'active' => (( Yii::$app->controller->id) == 'staff-positions'),],
                                        ]],
                                    ['label' => 'User Management',
                                        'icon' => 'cog',
                                        'visible' => yii::$app->user->identity->user_type == 1,
                                        'items' => [
                                            // ['label' => 'User Levels', 'url' => Url::to(['/user-levels/index']),'active' => ((yii::$app->controller->id) == 'user-levels')],
                                            //['label' => 'Students', 'url' => Url::to(['/students-manager/index']), 'active' => ((yii::$app->controller->id) == 'students-manager')],
                                            ['label' => 'Users', 'url' => Url::to(['/user-manager/index']), 'active' => ((yii::$app->controller->id) == 'user-manager' || ( yii::$app->controller->id == 'user-levels'))],
                                            ['label' => 'Login History', 'url' => Url::to(['/logins/index']), 'active' => (yii::$app->controller->id == 'logins')],
                                            ['label' => 'Audit Trail', 'url' => Url::to(['/user-audit-trail/index']), 'active' => (yii::$app->controller->id == 'user-audit-trail')],
                                            //['label' => 'Results Upload History', 'url' => Url::to(['/results-upload-history/index']), 'active' => (yii::$app->controller->id == 'results-upload-history')],
                                            ['label' => 'Login Attempts', 'url' => Url::to(['/login-attempts/index']), 'active' => (yii::$app->controller->id == 'login-attempts')],
                                            ['label' => 'Roles', 'url' => Url::to(['/admin/role']), 'active' => ((yii::$app->controller->id) == 'admin' && (yii::$app->controller->action->id) == 'role')],
                                            ['label' => 'Permission', 'url' => Url::to(['/admin/permission']), 'active' => (yii::$app->controller->id == 'admin' && (yii::$app->controller->action->id) == 'permission')],
                                            ['label' => 'Routes', 'url' => Url::to(['/admin/route']), 'active' => (yii::$app->controller->id == 'admin' && (yii::$app->controller->action->id) == 'route' )],
                                        ]],
                                ],
                            ]);
                        }
                    }
                    ?>
                </ul>

            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<?php
echo Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);

/*
  echo Html::a("Home", Yii::$app->homeUrl);
  foreach ($this->params['breadcrumbs'] as $key => $value) {
  echo " / ";
  if(is_array($value)){

  if(count($value['url'] == 1)){
  $url = [$value['url'][0]];
  } else {

  }
  echo Html::a($value['label'], $url);
  } else {
  echo $value;
  }
  }
  // var_dump($this->params['breadcrumbs']);
 */
?>
                <h1 class="page-header" style="font-size: 1.7em"><?= $this->title ?></h1>
                <?= $content ?>
            </div>
        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
