<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\depdrop\DepDrop;

/**
 * @var yii\web\View $this
 * @var app\models\UserLevels $model
 * @var yii\widgets\ActiveForm $form
 */
?>
<div class="auth-assignment-form">

    <?php
    $form = \kartik\form\ActiveForm::begin([
                'action' => ['index', 'user_id' => $user_id, 'user_details' => $user_details],
                'method' => 'post',
    ]);
    ?>

    <?php
    echo \kartik\builder\Form::widget([
        'model' => $assignmentModel,
        'form' => $form,
        'columns' => 2,
        'attributes' => [
//            'institution_structure_id' => [
//                'type' => Form::INPUT_DROPDOWN_LIST,
//                'label' => 'Level',
//                'items' => ArrayHelper::map(\app\models\InstitutionStructure::find()->asArray()->all(), 'institution_structure_id', 'institution_name'), 'options' => ['prompt' => 'Select User Level'],
//                'columnOptions' => ['id' => 'institution_structure_id']
//            ],
            'item_name' => [
                'type' => Form::INPUT_DROPDOWN_LIST,
                'label' => 'Role',
                'items' => ArrayHelper::map(\app\models\AuthItem::find()->where(['type' => 1])->asArray()->all(), 'name', 'description'), 'options' => ['prompt' => 'Select Access Right'],
                'columnOptions' => ['id' => 'item_name_id']
            ],
        ]
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('SAVE', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php
    \kartik\form\ActiveForm::end();
    ?>   

</div>

