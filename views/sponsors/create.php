<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Sponsors $model
 */

$this->title = 'Add Sponsor';
$this->params['breadcrumbs'][] = ['label' => 'Sponsors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sponsors-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
