<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Sponsors $model
 */

$this->title = 'Update Sponsor: ' . ' ' . $model->sponsor_name;
$this->params['breadcrumbs'][] = ['label' => 'Sponsors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sponsor_name, 'url' => ['view', 'id' => $model->sponsor_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sponsors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
