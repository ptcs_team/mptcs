<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolPrivacyStatement $model
 */

$this->title = 'Create School Privacy Statement';
$this->params['breadcrumbs'][] = ['label' => 'School Privacy Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-privacy-statement-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
