<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\SchoolPrivacyStatementSearch $searchModel
 */

$this->title = 'School Privacy Statements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-privacy-statement-index">
    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

       //'school_privacy_statement _id',
        //'school_id',
        'statement:ntext',
        ['attribute' => 'date_created', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'created_by',
        [
            'attribute' => 'created_by',
            'value' => function ($model) {
                return ucwords(\app\models\Users::findOne($model->created_by)->firstname) . '  ' . ucwords(\app\models\Users::findOne($model->created_by)->middlename) . ' ' . ucwords(\app\models\Users::findOne($model->created_by)->surname) . ' ' . '[' . \app\models\StaffPositions::findOne(\app\models\Users::findOne($model->created_by)->staff_position_id)->position_name . ']';
            }
        ],
//            'is_active', 
        [
            'label' => 'Is Active?',
            'attribute' => 'is_active',
            'vAlign' => 'middle',
            'width' => '5%',
            'value' => function($model) {
                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
            },
            'format' => 'raw', 
                    ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view}',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-privacy-statement/view','id' => $model->school_privacy_statement_id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>' ',
            'type'=>'default',
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
