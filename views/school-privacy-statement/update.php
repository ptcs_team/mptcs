<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolPrivacyStatement $model
 */

$this->title = 'Update School Privacy Statement: ' . ' ' . $model->school_privacy_statement_id;
$this->params['breadcrumbs'][] = ['label' => 'School Privacy Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->school_privacy_statement_id, 'url' => ['view', 'id' => $model->school_privacy_statement_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="school-privacy-statement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
