<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UserTypes $model
 */

$this->title = 'Update User Types: ' . ' ' . $model->user_type_id;
$this->params['breadcrumbs'][] = ['label' => 'User Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_type_id, 'url' => ['view', 'id' => $model->user_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-types-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
