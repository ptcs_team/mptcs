<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ProgrammeYear $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="programme-year-form">

    <?php
    $school_type_id = app\models\Schools::findOne($school_id)->school_type_id;
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [

            //'pintake_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ps ID...']],
            //'academic_year_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Academic Year ID...']],
            'study_year' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>  [1=>'Form One', 2=>'Form Two ', 3=>'Form Three', 4=>'Form Four'],   'options' => ['prompt' => '']],
            //'gpa_to_sit_supp' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Gpa To Sit Supp...']],
            //'max_failed_courses' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Max Failed Courses...']],
           // 'min_gpa_to_carry' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Min Gpa To Carry...']],
            'required_year_units' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Required Year Units...']],
            'grading_system_version_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>  \yii\helpers\ArrayHelper::map(\app\models\GradingSystemVersions::find()->where(" school_type_id = {$school_type_id} ")->all(), 'grading_system_version_id', 'description'), 'options' => ['prompt' => '']],
            //'year_contributes_to_gpa' => ['type' => Form::INPUT_CHECKBOX],
        ]
    ]);
    echo Html::submitButton('Add', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
