

<?php

use app\models\FormYearOfStudySearch;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Json;
use kartik\editable\Editable;
use yii\jui\Dialog;
use app\models\FormYearOfStudyCoursesSearch;
?>


<?php

$smodel = new FormYearOfStudyCoursesSearch;
$condition = " fy_id = {$fy_id}  AND school_id = {$school_id}";
$gridColumns = [

    [
        'label' => 'Course',
        'attribute' => 'course_offering_id',
        'value' => function($model) {
            $course_id = app\models\CourseOffering::findOne($model->course_offering_id)->course_id;
            return \app\models\Courses::findOne($course_id)->course_code . ' - ' . \app\models\Courses::findOne($course_id)->course_title;
        }
    ],
    [
        'attribute' => 'pass_grade_id',
        'value' => function($model) use( $model_fy, $model_fyosc, $school_id, $academic_year_id, $model_acy ) {
            return Editable::widget([
                        'name' => 'pass_grade_id',
                        'attribute' => 'pass_grade_id',
                        'value' => \app\models\GradingSystem::findOne($model->pass_grade_id)->grade,
                        'asPopover' => true,
                        'placement' => kartik\popover\PopoverX::ALIGN_LEFT,
                        'header' => 'Pass Grade',
                        //'format' => Editable::FORMAT_BUTTON,
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\GradingSystem::find()->where("grading_system_version_id = {$model_fy->grading_system_version_id} ")->all(), 'grading_system_id', 'grade'),
                        'options' => ['class' => 'form-control', 'prompt' => ''],
                        'editableValueOptions' => ['class' => 'text-success'],
                        'formOptions' => [
                            'action' => \yii\helpers\Url::to(['olevel-study-years/configure-study-year',
                                'academic_year_id' => $model_fy->academic_year_id,
                                'activeTab' => 'fy_' . $model_fy->fy_id,
                                'SemActiveTab' => 'sem_tab_' . $model_fy->fy_id,
                                'fy_id' => $model_fy->fy_id,
                                'fyos_course_id' => $model->fyos_course_id,
                                'model_fyosc' => $model_fyosc,
                                'school_id' => $school_id,
                                'academic_year_id' => $academic_year_id,
                                'model_acy' => $model_acy,
                            ]),
                        ]
            ]);
        },
                'format' => 'raw',
            ],
            [
                'attribute' => 'is_core',
                'value' => function($model) use( $model_fy, $model_fyosc, $school_id, $academic_year_id, $model_acy) {

                    return Editable::widget([
                                'name' => 'is_core',
                                'attribute' => 'is_core',
                                'value' => $model->is_core == 1 ? "Core" : "Option",
                                'asPopover' => true,
                                'placement' => kartik\popover\PopoverX::ALIGN_LEFT,
                                'header' => 'Core/Option',
                                //'format' => Editable::FORMAT_BUTTON,
                                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                'data' => [1 => 'Core', 0 => 'Option'],
                                'options' => ['class' => 'form-control', 'prompt' => ''],
                                'editableValueOptions' => ['class' => 'text-success'],
                                'formOptions' => [
                                    'action' => \yii\helpers\Url::to(['olevel-study-years/configure-study-year',
                                        'academic_year_id' => $model_fy->academic_year_id,
                                        'activeTab' => 'fy_' . $model_fy->fy_id,
                                        'SemActiveTab' => 'sem_tab_' . $model_fy->fy_id,
                                        'fy_id' => $model_fy->fy_id,
                                        'model_fyosc' => $model_fyosc,
                                        'fyos_course_id' => $model->fyos_course_id,
                                        'model_fyosc' => $model_fyosc,
                                        'school_id' => $school_id,
                                        'academic_year_id' => $academic_year_id,
                                        'model_acy' => $model_acy,
                                    ]),
                                ]
                    ]);
                },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'contribute_to_final_result',
                        'value' => function($model) use($model_fy, $model_fyosc, $school_id, $academic_year_id, $model_acy) {

                            return Editable::widget([
                                        'name' => 'contribute_to_final_result',
                                        'attribute' => 'contribute_to_final_result',
                                        'value' => $model->contribute_to_final_result == 1 ? "Yes" : "No",
                                        'asPopover' => true,
                                        'placement' => kartik\popover\PopoverX::ALIGN_LEFT,
                                        'header' => 'contributes to final result?',
                                        //'format' => Editable::FORMAT_BUTTON,
                                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                        'data' => [1 => 'Yes', 0 => 'No'],
                                        'options' => ['class' => 'form-control', 'prompt' => ''],
                                        'editableValueOptions' => ['class' => 'text-success'],
                                        'formOptions' => [
                                            'action' => \yii\helpers\Url::to(['olevel-study-years/configure-study-year',
                                                'academic_year_id' => $model_fy->academic_year_id,
                                                'activeTab' => 'fy_' . $model_fy->fy_id,
                                                'SemActiveTab' => 'sem_tab_' . $model_fy->fy_id,
                                                'fy_id' => $model_fy->fy_id,
                                                'model_fyosc' => $model_fyosc,
                                                'fyos_course_id' => $model->fyos_course_id,
                                                'model_fyosc' => $model_fyosc,
                                                'school_id' => $school_id,
                                                'academic_year_id' => $academic_year_id,
                                                'model_acy' => $model_acy,
                                            ]),
                                        ]
                            ]);
                        },
                                'format' => 'raw',
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{delete}',
                                'urlCreator' => function($action, $model) use ( $model_fy, $model_fyosc, $school_id, $academic_year_id, $model_acy) {
                                    if ($action == 'delete') {
                                        return \yii\helpers\Url::to(['olevel-study-years/configure-study-year',
                                                    'academic_year_id' => $model_fy->academic_year_id,
                                                    'activeTab' => 'fy_' . $model_fy->fy_id,
                                                    'SemActiveTab' => 'sem_tab_' . $model_fy->fy_id,
                                                    'fy_id' => $model_fy->fy_id,
                                                    'model_fyosc' => $model_fyosc,
                                                    'subaction' => 'delete_course',
                                                    'fyos_course_id' => $model->fyos_course_id,
                                                    'model_fyosc' => $model_fyosc,
                                                    'school_id' => $school_id,
                                                    'academic_year_id' => $academic_year_id,
                                                    'model_acy' => $model_acy,
                                        ]);
                                    }
                                }
                                    ],
                                ];

                                echo \kartik\grid\GridView::widget([
                                    'dataProvider' => $smodel->search($params, $condition),
                                    'filterModel' => $searchModel,
                                    'columns' => $gridColumns,
                                    'pjax' => true,
                                    'pjaxSettings' => [
                                        'neverTimeout' => true,
                                    ],
                                    'id' => 'form_year_courses_' . $model_fy->fy_id,
                                ]);

                                echo $this->render('_form_year_courses_form', [
                                    'model' => $model_fyosc,
                                    'model_fy' => $model_fy,
                                    'school_id' => $school_id,
                                    'model_fy' => $model_fy,
                                    'SemActiveTab' => $SemActiveTab,
                                    'academic_year_id' => $academic_year_id,
                                    'model_acy' => $model_acy,
                                    'activeTab' => $activeTab,
                                      'fy_id'=>$fy_id,
                                       'study_year'=>$study_year,
                                ]);
                                ?>
