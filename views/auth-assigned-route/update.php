<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\AuthAssignedRoute $model
 */

$this->title = 'Update Auth Assigned Route: ' . ' ' . $model->auth_assigned_route_id;
$this->params['breadcrumbs'][] = ['label' => 'Auth Assigned Routes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->auth_assigned_route_id, 'url' => ['view', 'id' => $model->auth_assigned_route_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auth-assigned-route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
