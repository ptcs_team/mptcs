<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\modelsAuthAssignedRouteSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="auth-assigned-route-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'auth_assigned_route_id') ?>

    <?= $form->field($model, 'auth_item_name') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'institution_structure_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
