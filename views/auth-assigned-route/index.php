<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\editable\Editable;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modelsAuthAssignedRouteSearch $searchModel
 */

$this->title = 'Routes that depend on Institution Level';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-assigned-route-index">

    <?php  echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'auth_assigned_route_id',
            'auth_item_name',
            //'description',
            [
             'attribute' => 'description',
             'label'  => 'Action',
             'value' => function($model){
                return $model->description;
             }
            ],
            //'user_id',
//            [
//              'attribute'=> 'institution_structure_id',
//              'value' => function($model){
//                return \app\models\InstitutionStructure::findOne($model->institution_structure_id)->institution_name;
//              }
//            ],
            [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'institution_structure_id',
            'label' => 'Scope',
            'value' => function($model){
                   $modelIns = \app\models\InstitutionStructure::findOne($model->institution_structure_id);
                   return isset($modelIns->institution_name)?$modelIns->institution_name ." [".\app\models\InstitutionType::findOne($modelIns->institution_type_id)->institution_type_name."]":"Click to Select Level";
            
            },
            'editableOptions' => [
                'header' => 'Scope',
                'placement' => \kartik\popover\PopoverX::ALIGN_LEFT,
                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                'data' => \app\models\InstitutionStructure::getInstitutionTree(1),

            ],
            'width' => '600px',
          
        ],


        ],

    ]);  ?>

</div>
