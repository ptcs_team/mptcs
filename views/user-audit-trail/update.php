<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\UserAuditTrail $model
 */

$this->title = 'Update User Audit Trail: ' . ' ' . $model->user_audit_trail_id;
$this->params['breadcrumbs'][] = ['label' => 'User Audit Trails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->user_audit_trail_id, 'url' => ['view', 'id' => $model->user_audit_trail_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-audit-trail-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
