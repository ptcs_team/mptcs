<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystem $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="grading-system-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 6,
        'attributes' => [

            'grade' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Grade...', 'maxlength' => 2]],
            'order'=> ['type' => Form::INPUT_DROPDOWN_LIST, 'items'=>[1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8], 'options'=>['prompt'=>''] ],
            'min_score' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Min Score...']],
            'max_score' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Max Score...']],
            'grade_points_formula' => ['type' => Form::INPUT_TEXT,
                'label'=> 'G/P Formula',
                'options' => ['placeholder' => 'Enter Grade Points Formula...', 'maxlength' => 255]],
                'definition' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Remarks...']],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
