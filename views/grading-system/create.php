<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystem $model
 */

$this->title = 'Create Grading System';
$this->params['breadcrumbs'][] = ['label' => 'Grading Systems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grading-system-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
