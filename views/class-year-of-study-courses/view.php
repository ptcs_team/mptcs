<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudyCourses $model
 */

$this->title = $model->cyos_course_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Year Of Study Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-year-of-study-courses-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'cyos_course_id',
            'cy_id',
            'course_offering_id',
            'pass_grade_id',
            'contribute_to_final_result',
            'is_core',
            'school_id',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->cyos_course_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
