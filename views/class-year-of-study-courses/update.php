<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudyCourses $model
 */

$this->title = 'Update Class Year Of Study Courses: ' . ' ' . $model->cyos_course_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Year Of Study Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cyos_course_id, 'url' => ['view', 'id' => $model->cyos_course_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="class-year-of-study-courses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
