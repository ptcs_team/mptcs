<?php
use kartik\grid\GridView;
use yii\widgets\Pjax;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider_about_us,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Title',
            'attribute' => 'title',
            'value' => function ($model) {
                return $model->title;
            }
        ],
        'body:ntext',
    ],
    'responsive' => true,
    'hover' => true,
    'condensed' => true,
    'floatHeader' => false,
    'panel' => [
        'heading' => ' ',
        'type' => 'default',
        'showFooter' => false
    ],
]);
Pjax::end();
?>



