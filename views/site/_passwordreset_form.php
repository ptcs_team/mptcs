<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-form" style="padding-right: 250px; padding-left: 250px;">

    <?php
   $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL,]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
           'newpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'New Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],
                ],
            'repeatnewpass' => ['type' => Form::INPUT_WIDGET,
                'label' => 'Re-Type Password',
                'widgetClass' => kartik\password\PasswordInput::classname(), 'options' => ['pluginOptions' => [
                        'showMeter' => false,
                        'toggleMask' => false
                    ]],],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Reset'), ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-left']);
    ActiveForm::end();
    ?>

</div>
