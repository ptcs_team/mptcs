
<script>
    function addBoardMembers(school_board_id) {
        //alert(RegSelectedStudentsID);
        var url = "<?= \yii\helpers\Url::to(['board-members/index']) ?>&school_board_id=" + school_board_id;
        $('#board_member-form-dialog-id').attr('src', url);
        $('#form-add-board-members-id').dialog('open');
    }
</script>
<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var app\models\Schools $model
 */
$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
ob_start();
?>
<?php
if ($model->logo == NULL) {
    $logo = 'default-no-image/no-logo.png';
} else {
    $logo = $model->logo;
}
?>
<div class="schools-view">
    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => '&nbsp',
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            [
                'label' => 'School Logo',
                'attribute' => 'logo',
                'value' => $logo,
                'format' => ['image', ['width' => '100', 'height' => '100']],
            ],
            'school_name',
            [
                'attribute' => 'school_type_id',
                'value' => \app\models\SchoolType::findOne($model->school_type_id)->school_type,
            ],
            'school_motto',
            'school_description:ntext',
            'address',
            'phone_no',
            'e_mail',
            'fax',
            [
                'attribute' => 'start_year_id',
                'value' => \app\models\Years::findOne($model->start_year_id)->year,
            ],
            [
                'attribute' => 'region_id',
                'value' => \app\models\Regions::findOne($model->region_id)->region,
            ],
            [
                'attribute' => 'district_id',
                'value' => \app\models\Districts::findOne($model->district_id)->district,
            ],
            [
                'attribute' => 'ward_id',
                'value' => \app\models\Wards::findOne($model->ward_id)->ward,
            ],
            [
                'attribute' => 'street_id',
                'value' => \app\models\StreetsVillage::findOne($model->street_id)->street,
            ],
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->school_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => false,
    ])
    ?>

</div>

<?php
$details = ob_get_contents();
ob_end_clean();
?>

<?php ob_start(); ?>
<?php
if ($model->google_location != NULL) {
    echo $model->google_location;
} else {
    echo ' <div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
         Not Location map yet
   </div>';
}
?>

<?php
$garage_location = ob_get_contents();
ob_end_clean();
?>


<?php ob_start(); ?>

<?php
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider_privacy_statement,
//    'filterModel' => $searchModel_privacy_statement,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'statement:ntext',
//        [
//            'label' => 'Is Active?',
//            'attribute' => 'is_active',
//            'vAlign' => 'middle',
//            'width' => '5%',
//            'value' => function($model) {
//                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
//            },
//            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
//        ],
    ],
    'responsive' => true,
    'hover' => true,
    'condensed' => true,
    'floatHeader' => false,
    'panel' => [
        'heading' => ' ',
        'type' => 'default',
        'showFooter' => false
    ],
]);
Pjax::end();
?>

<?php
$privacy_statement = ob_get_contents();
ob_end_clean();
?>



<?php
ob_start();
?>



<?php
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider_headmaster_statement,
//    'filterModel' => $searchModel_headmaster_statement,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'statement:ntext',
//        [
//            'label' => 'Is Active?',
//            'attribute' => 'is_active',
//            'vAlign' => 'middle',
//            'width' => '5%',
//            'value' => function($model) {
//                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
//            },
//            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
//        ],
    ],
    'responsive' => true,
    'hover' => true,
    'condensed' => true,
    'floatHeader' => false,
    'panel' => [
        'heading' => ' ',
        'type' => 'default',
        'showFooter' => false
    ],
]);
Pjax::end();
?>

<?php
$headmaster_statement = ob_get_contents();
ob_end_clean();
?>




<?php
ob_start();
?>
<?php
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider_school_board,
//    'filterModel' => $searchModel_school_board,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'description:ntext',
        [
            'label' => 'Board Members',
            'attribute' => 'school_board_id',
            'value' => function($model) {
                if ($model->school_board_id == 0) {
                    return '--';
                } else {
                    return '&nbsp' . ' <span class="badge">' . count(app\models\BoardMembers::find()->where("school_board_id = {$model->school_board_id} ")->all()) . '</span>' . '&nbsp' . '&nbsp' . Html::a('[View Members]', '#', ['onclick' => "addBoardMembers({$model->school_board_id})", 'style' => 'margin-left: 20px']);
                }
            },
                    'format' => 'raw',
                ],
//                [
//                    'label' => 'Is Active?',
//                    'attribute' => 'is_active',
//                    'vAlign' => 'middle',
//                    'width' => '5%',
//                    'value' => function($model) {
//                        return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
//                    },
//                    'format' => 'raw',
//                    'filterType' => GridView::FILTER_SELECT2,
//                    'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//                    'filterWidgetOptions' => [
//                        'pluginOptions' => ['allowClear' => true],
//                    ],
//                    'filterInputOptions' => ['placeholder' => 'Search...'],
//                    'format' => 'raw'
//                ],
            ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
            'floatHeader' => false,
            'panel' => [
                'heading' => ' ',
                'type' => 'default',
                'showFooter' => false
            ],
        ]);
        Pjax::end();
        ?>

        <?php
        \yii\jui\Dialog::begin([
            'clientOptions' => [
                'modal' => true,
                'autoOpen' => false,
                'width' => '1000',
                'height' => '450',
            ],
            'options' => [
                'title' => $this->title . ' ' . 'Board Members',
                'id' => 'form-add-board-members-id',
            ]
                ]
        );
        ?>

        <iframe src="" width="100%" height="100%" id="board_member-form-dialog-id">
        </iframe>

        <?php
        \yii\jui\Dialog::end();
        ?>

        <?php
        $school_board = ob_get_contents();
        ob_end_clean();
        ?>

        <?php
        ob_start();
        ?>

        <?php
        Pjax::begin();
        echo GridView::widget([
            'dataProvider' => $dataProvider_board_statement,
//            'filterModel' => $searchModel_board_statement,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'School Board',
                    'attribute' => 'school_board_id',
                    'value' => function ($model) {
                        return app\models\SchoolBoard::findOne($model->school_board_id)->name;
                    }
                ],
                'statement:ntext',
//                [
//                    'label' => 'Is Active?',
//                    'attribute' => 'is_active',
//                    'vAlign' => 'middle',
//                    'width' => '5%',
//                    'value' => function($model) {
//                        return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
//                    },
//                    'format' => 'raw',
//                    'filterType' => GridView::FILTER_SELECT2,
//                    'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//                    'filterWidgetOptions' => [
//                        'pluginOptions' => ['allowClear' => true],
//                    ],
//                    'filterInputOptions' => ['placeholder' => 'Search...'],
//                    'format' => 'raw'
//                ],
            ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
            'floatHeader' => false,
            'panel' => [
                'heading' => ' ',
                'type' => 'default',
                'showFooter' => false
            ],
        ]);
        Pjax::end();
        ?>


        <?php
        $board_statement = ob_get_contents();
        ob_end_clean();
        ?>

        <?php
        ob_start();
        ?>

        <?php
        Pjax::begin();
        echo GridView::widget([
            'dataProvider' => $dataProvider_sponsors,
//            'filterModel' => $searchModel_sponsors,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Sponsor',
                    'attribute' => 'sponsor_id',
                    'value' => function ($model) {
                        return app\models\Sponsors::findOne($model->sponsor_id)->sponsor_name;
                    }
                ],
                [
                    'label' => 'Description',
                    'attribute' => 'what_sponsored',
                    'value' => function ($model) {
                        return $model->what_sponsored;
                    }
                ],
                [

                    'label' => 'Amount Sponsored',
                    'attribute' => 'amount',
                    'hAlign' => 'right',
                    'value' => function ($model) {
                        return $model->amount;
                    },
                    'format' => 'raw',
                    'format' => ['decimal', 2]
                ],
                ['attribute' => 'date_sponsored', 'format' => ['date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//                [
//                    'label' => 'Is Visible?',
//                    'attribute' => 'is_visible',
//                    'vAlign' => 'middle',
//                    'width' => '5%',
//                    'value' => function($model) {
//                        return $model->is_visible == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
//                    },
//                    'format' => 'raw',
//                    'filterType' => GridView::FILTER_SELECT2,
//                    'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//                    'filterWidgetOptions' => [
//                        'pluginOptions' => ['allowClear' => true],
//                    ],
//                    'filterInputOptions' => ['placeholder' => 'Search...'],
//                    'format' => 'raw'
//                ],
            ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
            'floatHeader' => false,
            'panel' => [
                'heading' => ' ',
                'type' => 'default',
                'showFooter' => false
            ],
        ]);
        Pjax::end();
        ?>

        <?php
        $sponsors = ob_get_contents();
        ob_end_clean();
        ?>


        <?php
        ob_start();
        ?>


        <?php
        Pjax::begin();
        echo GridView::widget([
            'export' => true,
            'dataProvider' => $dataProvider_donors,
//            'filterModel' => $searchModel_donors,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Donor',
                    'attribute' => 'donor_id',
                    'value' => function ($model) {
                        return app\models\Donors::findOne($model->donor_id)->donor_name;
                    }
                ],
                [
                    'label' => 'Description',
                    'attribute' => 'what_donated',
                    'value' => function ($model) {
                        return $model->what_donated;
                    }
                ],
                [

                    'label' => 'Amount Donated',
                    'attribute' => 'amount',
                    'hAlign' => 'right',
                    'value' => function ($model) {
                        return $model->amount;
                    },
                    'format' => 'raw',
                    'format' => ['decimal', 2]
                ],
//                [
//                    'label' => 'Is Visible?',
//                    'attribute' => 'is_visible',
//                    'vAlign' => 'middle',
//                    'width' => '5%',
//                    'value' => function($model) {
//                        return $model->is_visible == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
//                    },
//                    'format' => 'raw',
//                    'filterType' => GridView::FILTER_SELECT2,
//                    'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//                    'filterWidgetOptions' => [
//                        'pluginOptions' => ['allowClear' => true],
//                    ],
//                    'filterInputOptions' => ['placeholder' => 'Search...'],
//                    'format' => 'raw'
//                ],
            ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
            'floatHeader' => false,
            'panel' => [
                'heading' => ' ',
                'type' => 'default',
                'showFooter' => false
            ],
        ]);
        Pjax::end();
        ?>

        <?php
        $donors = ob_get_contents();
        ob_end_clean();
        ?>

        <?php
        echo TabsX::widget([
            'items' => [
                [
                    'label' => ' ' . ' School Details',
                    'content' => $details,
                    'options' => ['id' => 'Details-tab'],
                    'active' => ($activeTab == 'Details-tab'),
                ],
                [
                    'label' => ' ' . 'Privacy Statement',
                    'content' => $privacy_statement,
                    'options' => ['id' => 'privacy_statement-tab'],
                    'active' => ($activeTab == 'privacy_statement-tab'),
                ],
                [
                    'label' => ' ' . 'Headmaster Statement',
                    'content' => $headmaster_statement,
                    'options' => ['id' => 'headmaster_statement-tab'],
                    'active' => ($activeTab == 'headmaster_statement-tab'),
                ],
                //[
                   // 'label' => ' ' . 'School Board',
                   // 'content' => $school_board,
                   // 'options' => ['id' => 'school_board-tab'],
                   // 'active' => ($activeTab == 'school_board-tab'),
                //],
                [
                    'label' => ' ' . 'School Board Statement',
                    'content' => $board_statement,
                    'options' => ['id' => 'board_statement-tab'],
                    'active' => ($activeTab == 'board_statement-tab'),
                ],
                [
                    'label' => ' ' . 'School Sponsors',
                    'content' => $sponsors,
                    'options' => ['id' => 'sponsors-tab'],
                    'active' => ($activeTab == 'sponsors-tab'),
                ],
                [
                    'label' => ' ' . 'School Donors',
                    'content' => $donors,
                    'options' => ['id' => 'donors-tab'],
                    'active' => ($activeTab == 'donors-tab'),
                ],
                 //[
                    //'label' => ' ' . 'School Teachers',
                   // 'content' => $donors,
                   // 'options' => ['id' => 'donors-tab'],
                   // 'active' => ($activeTab == 'donors-tab'),
               // ],
                [
                    'label' => ' ' . 'School Location',
                    'content' => $garage_location,
                    'options' => ['id' => 'location-tab'],
                    'active' => ($activeTab == 'location-tab'),
                ],
            ],
            'bordered' => true,
        ]);
        ?>

