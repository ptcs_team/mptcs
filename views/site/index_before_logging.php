<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\SchoolsSearch $searchModel
 */
$title = 'Welcome to Parent Teachers Communication System';
$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schools-index" style="background-size:cover">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'school_name',
            [
                'attribute' => 'school_type_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return \app\models\SchoolType::findOne($model->school_type_id)->school_type;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\SchoolType::find()->orderBy('school_type_id')->all(), 'school_type_id', 'school_type'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'attribute' => 'region_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return \app\models\Regions::findOne($model->region_id)->region;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Regions::find()->orderBy('region_id')->all(), 'region_id', 'region'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'attribute' => 'district_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return \app\models\Districts::findOne($model->district_id)->district;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Districts::find()->orderBy('district_id')->all(), 'district_id', 'district'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'attribute' => 'ward_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return \app\models\Wards::findOne($model->ward_id)->ward;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Wards::find()->orderBy('ward_id')->all(), 'ward_id', 'ward'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'attribute' => 'street_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return app\models\StreetsVillage::findOne($model->street_id)->street;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\StreetsVillage::find()->orderBy('street_id')->all(), 'street', 'street'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'label' => 'Action',
                'value' => function($model) {

                    return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-eye-open"></i> More</span>', Yii::$app->urlManager->createUrl(['site/view-school-details', 'id' => $model->school_id]), [
                                'title' => Yii::t('yii', 'View Details'),
                    ]);
                },
                        'format' => 'raw',
                    ],
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
                'panel' => [
                    'heading' => 'THE LIST OF SCHOOLS IN TANZANIA',
                    'type' => 'default',
                    'showFooter' => true
                ],
            ]);
            Pjax::end();
            ?>

</div>