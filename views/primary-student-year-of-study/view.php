<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\PrimaryStudentYearOfStudy $model
 */

$this->title = $model->primary_student_year_of_study_id;
$this->params['breadcrumbs'][] = ['label' => 'Primary Student Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="primary-student-year-of-study-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'primary_student_year_of_study_id',
            'user_id',
            'cy_id',
            'student_year_status_id',
            'year_of_study',
            'results',
            'year_units',
            'year_grade_points',
            'status',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->primary_student_year_of_study_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
