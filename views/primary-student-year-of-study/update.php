<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\PrimaryStudentYearOfStudy $model
 */

$this->title = 'Update Primary Student Year Of Study: ' . ' ' . $model->primary_student_year_of_study_id;
$this->params['breadcrumbs'][] = ['label' => 'Primary Student Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->primary_student_year_of_study_id, 'url' => ['view', 'id' => $model->primary_student_year_of_study_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="primary-student-year-of-study-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
