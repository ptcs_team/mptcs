<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentStatus $model
 */

$this->title = 'Create Student Status';
$this->params['breadcrumbs'][] = ['label' => 'Student Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-status-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
