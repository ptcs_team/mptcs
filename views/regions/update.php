<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Regions $model
 */

$this->title = 'Update Regions: ' . ' ' . $model->region_id;
$this->params['breadcrumbs'][] = ['label' => 'Regions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->region_id, 'url' => ['view', 'id' => $model->region_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="regions-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
