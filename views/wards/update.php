<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Wards $model
 */

$this->title = 'Update Wards: ' . ' ' . $model->ward_id;
$this->params['breadcrumbs'][] = ['label' => 'Wards', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ward_id, 'url' => ['view', 'id' => $model->ward_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="wards-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
