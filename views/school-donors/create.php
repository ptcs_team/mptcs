<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolDonors $model
 */

$this->title = 'Create School Donors';
$this->params['breadcrumbs'][] = ['label' => 'School Donors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-donors-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
