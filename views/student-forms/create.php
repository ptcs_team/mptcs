<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentForms $model
 */

$this->title = 'Create Student Forms';
$this->params['breadcrumbs'][] = ['label' => 'Student Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-forms-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
