<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentForms $model
 */

$this->title = $model->sf_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-forms-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'sf_id',
            'fy_id',
            'olevel_student_year_of_study_id',
            'form_year_of_study_number',
            'student_form_status',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->sf_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
