<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentForms $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-forms-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'fy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fy ID...']], 

'olevel_student_year_of_study_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Olevel Student Year Of Study ID...']], 

'form_year_of_study_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Form Year Of Study Number...']], 

'student_form_status'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Form Status...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
