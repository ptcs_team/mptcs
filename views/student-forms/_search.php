<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\StudentFormsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-forms-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sf_id') ?>

    <?= $form->field($model, 'fy_id') ?>

    <?= $form->field($model, 'olevel_student_year_of_study_id') ?>

    <?= $form->field($model, 'form_year_of_study_number') ?>

    <?= $form->field($model, 'student_form_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
