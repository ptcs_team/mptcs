<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\OlevelStudentYearOfStudy $model
 */

$this->title = 'Create Olevel Student Year Of Study';
$this->params['breadcrumbs'][] = ['label' => 'Olevel Student Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="olevel-student-year-of-study-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
