<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\OlevelStudentYearOfStudy $model
 */

$this->title = 'Update Olevel Student Year Of Study: ' . ' ' . $model->olevel_student_year_of_study_id;
$this->params['breadcrumbs'][] = ['label' => 'Olevel Student Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->olevel_student_year_of_study_id, 'url' => ['view', 'id' => $model->olevel_student_year_of_study_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="olevel-student-year-of-study-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
