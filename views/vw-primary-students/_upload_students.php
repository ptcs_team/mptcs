<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
$this->title = 'Upload Primary Students';
$this->params['breadcrumbs'][] = ['label' => 'Manage Students', 'url' => ['vw-primary-students/index']];
$this->params['breadcrumbs'][] = ['label' => 'Primary Schools', 'url' => ['vw-primary-students/upload-index']];
$this->params['breadcrumbs'][] = 'Academic Years';
?>
<h4><span class="label label-success">School Name </span>:<span class="label label-info"><?= app\models\Schools::findOne($school_id)->school_name. ' '. \app\models\SchoolType::findOne(app\models\Schools::findOne($school_id)->school_type_id)->school_type?></span> &nbsp;&nbsp; 
</h4><br>

   <?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'value' => function ($model, $key, $index, $column)  {
            return GridView::ROW_COLLAPSED;
    },
            'allowBatchToggle' => true,
            'detail' => function ($model) use ($school_id) {

                return $this->render('_classes', ['school_id' => $school_id,
                    'academic_year_id' => $model->academic_year_id,
        ]);
    },
            'detailOptions' => [
                'class' => 'kv-state-enable',
            ],
        ],
        'academic_year',
    ],
    'responsive' => true,
    'hover' => true,
    'condensed' => true,
    'floatHeader' => true,
]);
?>

