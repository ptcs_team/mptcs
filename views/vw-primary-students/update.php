<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\VwPrimaryStudents $model
 */
$fullname = strtoupper($model->surname).', '.$model->firstname.' '.$model->middlename;
$this->title = 'Update Student: ' . ' ' .$fullname;
$this->params['breadcrumbs'][] = ['label' => 'Primary Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $fullname, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-primary-students-update">
    <?= $this->render('_form_update', [
        'model' => $model,
        'year_of_study'=>$year_of_study,
        'academic_year_id'=>$academic_year_id
    ]) ?>

</div>
