<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\jui\Dialog;


$this->title = 'Upload Primary Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schools-index">
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'programme_sessions_grid_id',
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
             //'school_id',
             //'school_name',
             [
                'attribute' => 'school_name',
                'vAlign' => 'middle',
                'width' => '300px',
                'value' => function ($model) {
                    return $model->school_name;
                },
                ],
 
             [
                'attribute' => 'region_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return \app\models\Regions::findOne($model->region_id)->region;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Regions::find()->orderBy('region_id')->all(), 'region_id', 'region'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'attribute' => 'district_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return \app\models\Districts::findOne($model->district_id)->district;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Districts::find()->orderBy('district_id')->all(), 'district_id', 'district'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'ward', 
            [
                'attribute' => 'ward_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return \app\models\Wards::findOne($model->ward_id)->ward;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Wards::find()->orderBy('ward_id')->all(), 'ward_id', 'ward'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'street', 
            [
                'attribute' => 'street_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return app\models\StreetsVillage::findOne($model->street_id)->street;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\StreetsVillage::find()->orderBy('street_id')->all(), 'street', 'street'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            
//            'school_description:ntext', 
//            'address', 
//            'phone_no', 
//            'e_mail', 
//            'fax', 
//            'logo', 
//            'start_year_id', 
//            'google_location:ntext', 
//            'school_privacy_statement_id', 
//            'school_board_id', 
//            'school_donor_id', 
//            'school_sponsor_id', 
//            'school_gallery_id', 
//            'school_motto', 
//            'headmaster_statement_id', 
//            'msg_frm_teacher_id', 
//            'msg_frm_parent_id', 
//            'inbox_id', 

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template'=>'{view}',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['schools/view','id' => $model->school_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],

            //['attribute' => 'start_date', 'format' => ['date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
              [
                'label' => '',
                'value' => function($model) {
                    return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-upload"></i> Upload</span>', Yii::$app->urlManager->createUrl(['vw-primary-students/upload-academic-years', 'school_id' => $model->school_id, 'upload' => 't']), [
                                'title' => Yii::t('yii', 'upload'),
                    ]);
                },
                        'format' => 'raw',
                    ],
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
//        'panel' => [
//            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> Sessions </h3>',
//            'type'=>'info',
////            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
////            'showFooter'=>false
//        ],
            ]);
            ?>

</div>




