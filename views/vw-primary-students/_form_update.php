
<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\depdrop\DepDrop;
use kartik\file\FileInput;
use kartik\detail\DetailView;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\models\Students $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="students-form">
    <?php
    if ($model->profile_picture == NULL) {
        $picture = 'default-no-image/default-no-profile-image.png';
    } else {
        $picture = $model->profile_picture;
    }
    ?>

    <?php if (!$model->isNewRecord) { ?>

        <div class="row">
            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img src="<?php echo $picture; ?>" alt="Profile Photo">
                </a>
            </div>

        </div>


    <?php } ?>

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [
            'registration_number' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Registration Number...', 'maxlength' => 255]],
            'firstname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Firstname...', 'maxlength' => 20]],
            'middlename' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Middlename...', 'maxlength' => 20]],
            'surname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Surname...', 'maxlength' => 20]],
            'sex' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items' => ['M' => 'Male', 'F' => 'Female'],
                'options' => ['prompt' => '']],
            'date_of_birth' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => kartik\date\DatePicker::classname(), 'options' => ['pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true]],],
            //'options' => ['placeholder' => 'Enter Date Of Birth...', 'maxlength' => 255]],
            'school_id' => ['type' => Form::INPUT_WIDGET,
                'staticValue' => \app\models\Schools::findOne($model->school_id)->school_name,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Primary School',
                'options' => [
                    'data' => \yii\helpers\ArrayHelper::map(\app\models\Schools::find()->where('is_active = 1 AND school_type_id = 1')->asArray()->all(), 'school_id', 'school_name'),
                    'options' => [
                        'prompt' => '',
                     'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
            ],
            'academic_year' => [
                'type' => Form::INPUT_WIDGET,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'label' => 'Academic Year',
                'widgetClass' => DepDrop::className(),
                'options' => [
                'data' => [$academic_year_id => \app\models\AcademicYears::findOne($academic_year_id)->academic_year],
                        'disabled' => $model->isNewrecord ? false : true,
                    'pluginOptions' => [
                        'depends' => ['primarystudents-school_id'],
                        'placeholder' => 'Select',
                        'url' => Url::to(['/vw-primary-students/academic-years']),
                    ],
                    
                ],
                'columnOptions' => ['id' => 'academic_year'],
            ],
            'year_of_study' => [
                'type' => Form::INPUT_WIDGET,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'label' => 'Study Year',
                'widgetClass' => DepDrop::className(),
                'options' => [
                    'data' => [$year_of_study => \app\models\ClassDescription::find()->where(" class_number = {$year_of_study}")->one()->description],
                               'disabled' => $model->isNewrecord ? false : true,
                    'pluginOptions' => [
                        'depends' => ['primarystudents-academic_year'],
                        'placeholder' => 'Select',
                        'url' => Url::to(['/vw-primary-students/study-years']),
                    ],
                ],
                'columnOptions' => ['id' => 'year_of_study'],
            ],
            'place_of_birth' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Place Of Birth...', 'maxlength' => 50]],
            'admission_date' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => kartik\date\DatePicker::classname(), 'options' => ['pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true]],],
//'login_counts'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Login Counts...']], 
//'user_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User Type...']], 
//'staff_position_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Staff Position ID...']], 
//'username'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Username...', 'maxlength'=>200]], 
//'last_login'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
//'salutation'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Salutation...', 'maxlength'=>20]], 
//'completion_date'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATE]], 
//'status_comments'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status Comments...', 'maxlength'=>255]], 
//'password'=>['type'=> Form::INPUT_PASSWORD, 'options'=>['placeholder'=>'Enter Password...', 'maxlength'=>255]], 
//'sponsorship_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Sponsorship Type...', 'maxlength'=>1]], 
            'mailing_address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Mailing Address...', 'maxlength' => 150]],
            //'telephone_no' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Telephone No...', 'maxlength' => 150],],
            'telephone_no' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => yii\widgets\MaskedInput::classname(),
                'options' => [
                    'mask' => '+255999999999',
                    'clientOptions' => [
                        'alias' => 'telephone_no'
                    ],
                    'options' => ['disabled' => false, 'class' => 'form-control']
                ],
            ],
            'email_address' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => yii\widgets\MaskedInput::classname(),
                'options' => [
                    'clientOptions' => [
                        'alias' => 'email'
                    ],
                    'options' => ['disabled' => false, 'class' => 'form-control']
                ],
            ],
            //'email_address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email Address...', 'maxlength' => 100]],
            'image' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => FileInput::className(),
                'label' => $model->isNewRecord ? "Profile Photo" : "Profile Photo (Will replace existing one)",
                ['options' => ['accept' => 'image/*']],
                'pluginOptions' => ['previewFileType' => 'image'],
            ],
        ],
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Register') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>