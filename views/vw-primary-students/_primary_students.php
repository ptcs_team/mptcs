<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
$model_cy = \app\models\ClassYear::findOne($cy_id);
$this->title = \app\models\ClassDescription::find()->where(" class_number = {$model_cy->study_year}")->one()->description.' '." Students, ". ' '. 'Academic Year: '.app\models\AcademicYears::findOne($model_cy->academic_year_id)->academic_year;
$this->params['breadcrumbs'][] = ['label' => 'Manage Students', 'url' => ['vw-primary-students/index']];
$this->params['breadcrumbs'][] = ['label' => 'Primary Schools', 'url' => ['vw-primary-students/upload-index']];
$this->params['breadcrumbs'][] = ['label'=>'Academic Years','url'=>['vw-primary-students/upload-academic-years','school_id'=>$school_id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<h4><span class="label label-success">School Name </span>:<span class="label label-info"><?= app\models\Schools::findOne($school_id)->school_name. ' '. \app\models\SchoolType::findOne(app\models\Schools::findOne($school_id)->school_type_id)->school_type?></span> &nbsp;&nbsp; 
</h4><br>
      <?php
  echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'user_id',
            //'profile_picture',
            //'student_status_id',
            'registration_number',
            'fullname',
             //'sex',
            [
                'attribute'=>'sex',
                'value'=>  function ($model){
                    if ($model->sex == 'M') {
                        return 'Male';
                        
                    }   
                    if ($model->sex == 'F') {
                        return 'Female'; 
                    }  else {
                        return 'Not Specified';
                    }
                }
                
            ],

//            'firstname', 
//            'middlename', 
//            'pintake_id', 
//            'sex', 
//            'date_of_birth', 
//            'place_of_birth', 
//            'mailing_address', 
//            'telephone_no', 
//            'email_address:email', 
//            'admission_date', 
//            'sponsorship_type', 
//            'status_comments', 
//            ['attribute'=>'completion_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']], 
//            'username', 
//            'password', 
//            'is_active', 
//            'login_counts', 
//            ['attribute'=>'last_login','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            'user_type', 
//            'staff_position_id', 
//            'salutation', 
//            'fullname', 
//            'session_description', 
//            'start_month', 
//            'units_required', 
//            'number_of_years', 
//            'programme_code', 
//            'programme_name', 
//            'programme_acronmy', 
//            'study_year', 
//            'programme_year_status_id', 

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template'=>'{view}',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['vw-pystudents/view','id' => $model->user_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//                'urlCreator' => function($action, $model){
//                    if($action == 'view'){
//                        return '#';
//                    }
//                }
//            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>true,



        'panel' => [
            'heading'=> '',
            'type'=>'default',
            'before' => Html::a('<i class = "glyphicon glyphicon-circle-arrow-down"></i> View Students Upload History', '#', ['onclick' => "$('#students_upload_history').dialog('open');return false;", 'class' => 'btn btn-primary', 'style' => 'margin-left: 5px']), 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['programme-year-students','py_id'=>$py_id], ['class' => 'btn btn-info']),
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            //'showFooter'=>false
        ],
    ]); 
      echo $this->render('_form_upload_students',['model'=>$model,'cy_id'=>$cy_id]);  
      
      
      
           \yii\jui\Dialog::begin([
                        'clientOptions' => [
                            'modal' => true,
                            'autoOpen' => $autoOpen3,
                            'height' => '450',
                            'width' => '1000'
                        ],
                        'options' => [
                            'title' => 'students upload history, ' . $this->title,
                            'id' => 'students_upload_history'
                        ]
                            ]
                    );


                    echo $this->render('_students_upload_history', [ 
                      'cy_id'=>$cy_id,
                    ]);

                    \yii\jui\Dialog::end();
      

   ?> 





