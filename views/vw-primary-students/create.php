<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\VwPrimaryStudents $model
 */

$this->title = 'Register Primary Student';
$this->params['breadcrumbs'][] = ['label' => ' Primary Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-primary-students-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
