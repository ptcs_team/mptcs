<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\form\ActiveForm;
use kartik\builder\Form;
?>

<div class="process-results-index">
    <iframe src='<?php echo yii\helpers\Url::to(['/students-upload-history/get-upload-history/',
                       'cy_id'=>$cy_id]); ?>' width="100%" height="350px" >
    </iframe>
</div>
