<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\VwPrimaryStudentsSearch $searchModel
 */
$this->title = 'Primary Students';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-primary-students-index">

    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'user_id',
            //'profile_picture',
            //'student_status_id',
            'registration_number',
            'fullname',
            //'surname',
//            'firstname', 
//            'middlename', 
//            'school_id', 
            [
                'label' => 'School',
                'attribute' => 'school_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function($model) {
                    return \app\models\Schools::findOne($model->school_id)->school_name;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\Schools::find()->where("school_type_id = 1")->orderBy('school_id')->asArray()->all(), 'school_id', 'school_name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'sex', 
//            'date_of_birth', 
//            'place_of_birth', 
//            'mailing_address', 
//            'telephone_no', 
//            'email_address:email', 
//            ['attribute'=>'admission_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']], 
//            'status_comments', 
//            ['attribute'=>'completion_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']], 
            //         'username', 
//            'password', 
//            'is_active', 
//            'login_counts', 
//            ['attribute'=>'last_login','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            'user_type', 
//            'staff_position_id', 
//            'salutation', 
//            'primary_student_year_of_study_id', 
            // 'cy_id', 
//            'year_of_study',
               
               [
                 'label' => 'Class',
                'attribute' => 'year_of_study',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
               return \app\models\ClassDescription::find()->where("class_number = {$model->year_of_study}")->one()->description;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\ClassDescription::find()->all(), 'class_number', 'description'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
                    
                    
//            'results', 
            // 'year_units', 
//            'student_year_status_id',
            [
                'label' => 'Status',
                'attribute' => 'student_year_status_id',
                'value' => function ($model) {
                    if ($model->student_year_status_id = 1) {
                        return 'Inprogress';
                    }
                }
            ],
                                  [
             
                'attribute' => 'academic_year_id',
                'vAlign' => 'middle',
                'width' => '100px',
                'value' => function($model) {
                    return \app\models\AcademicYears::findOne($model->academic_year_id)->academic_year;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\AcademicYears::findBySql("SELECT academic_year,academic_year_id FROM academic_years WHERE academic_year_id  IN (SELECT academic_year_id FROM class_year)")->asArray()->all(), 'academic_year_id', 'academic_year'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'year_grade_points', 
//            'status', 
            [
                'label' => '',
                'value' => function($model) {
                    return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-eye-open"></i> Details</span>', Yii::$app->urlManager->createUrl(['vw-primary-students/view', 'id' => $model->user_id, 'view' => 't']), [
                                'title' => Yii::t('yii', 'View more details'),
                    ]);
                },
                        'format' => 'raw',
                    ],
                    [
                        'label' => '',
                        'value' => function($model) {
                            return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-pencil"></i> Edit</span>', Yii::$app->urlManager->createUrl(['vw-primary-students/update', 'id' => $model->user_id, 'year_of_study'=>$model->year_of_study,'academic_year_id'=>$model->academic_year_id,'Update' => 't']), [
                                        'title' => Yii::t('yii', 'Update'),
                            ]);
                        },
                                'format' => 'raw',
                            ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['vw-primary-students/view','id' => $model->user_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
                        ],
                        'responsive' => true,
                        'hover' => true,
                        'condensed' => true,
                        'floatHeader' => false,
                        'panel' => [
                            'heading' => ' ',
                            'type' => 'default',
                            'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add Student', ['create'], ['class' => 'btn btn-success']) . ' ' . Html::a('<i class="glyphicon glyphicon-upload"></i> Upload Students', ['vw-primary-students/upload-index'], ['class' => 'btn btn-success', 'style' => 'margin-left: 30px']), 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                            'showFooter' => false
                        ],
                    ]);
                    Pjax::end();
                    ?>

</div>
