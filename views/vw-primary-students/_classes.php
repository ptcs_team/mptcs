<?php

$class_years = \app\models\ClassYear::find()->where(" school_id = {$school_id} and academic_year_id = {$academic_year_id} order by study_year ASC ")->all();
?>

<ul class="list-group" style="margin-left: 200px;">
  <?php
  foreach ($class_years as $cyear) {
     echo '<li class="list-group-item">'.yii\helpers\Html::a(strtoupper(\app\models\ClassDescription::find()->where(" class_number = {$cyear->study_year}")->one()->description), \yii\helpers\Url::to(['vw-primary-students/class-year-students','cy_id'=>$cyear->cy_id, 'school_id'=>$school_id])).'<span class="badge pull-right" style="margin-right: 450px;">'.  app\models\PrimaryStudentYearOfStudy::find()->where("cy_id = {$cyear->cy_id}")->count().'</span></li>';
  }
  ?> 
</ul>