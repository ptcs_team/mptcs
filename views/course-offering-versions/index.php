<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\CourseOfferingVersionsSearch $searchModel
 */
$this->title = 'Course Offering Versions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-offering-versions-index">

    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'course_offering_version_id',
            // 'description',
            [
                'attribute' => 'description',
                'vAlign' => 'middle',
                'width' => '350px',
                'value' => $model->description,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(app\models\CourseOfferingVersions::find()->orderBy('course_offering_version_id')->asArray()->all(), 'description', 'description'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            //'status',
            [
                'label' => 'School Type',
                'attribute' => 'school_type_id',
                'value' => function ($model) {
                    return \app\models\SchoolType::findOne($model->school_type_id)->school_type;
                }
            ],
            [
                'attribute' => 'status',
                'vAlign' => 'middle',
                'width' => '220px',
                'value' => function ($model) {
                    if ($model->status == 1) {
                        return 'Active';
                    } else {
                        return 'Inactive';
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ['1' => 'Active', '0' => 'Inactive'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['course-offering-versions/view', 'id' => $model->course_offering_version_id, 'edit' => 't']), [
                                    'title' => Yii::t('yii', 'Edit'),
                        ]);
                    }
                        ],
                    ],
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => true,
                'panel' => [
                    'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>  </h3>',
                    'type' => 'default',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']), 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                    'showFooter' => false
                ],
            ]);
            Pjax::end();
            ?>

</div>
