<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOfferingVersions $model
 */

$this->title = 'Create Course Offering Versions';
$this->params['breadcrumbs'][] = ['label' => 'Course Offering Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-offering-versions-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
