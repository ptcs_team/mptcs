<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOffering $model
 * @var yii\widgets\ActiveForm $form
 */

?>

<div class="course-offering-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 2,
    'attributes' => [
 //'course_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Course ID...', 'maxlength'=>10]], 
         'course_id' => [
                 'type' => Form::INPUT_WIDGET,
                  'widgetClass' => kartik\select2\Select2::className(),
                  'options'=>[
                      'model'=>$model,
                      'attribute'=>'course_id',
                      'data'=>\yii\helpers\ArrayHelper::map(app\models\Courses::findBySql("select *, CONCAT(ifnull(course_code,' '),'-',course_title) as course_title from courses where school_type_id = {$school_type_id} AND course_id  ")->all(),'course_id','course_title'),
//                      'data'=>\yii\helpers\ArrayHelper::map(app\models\Courses::findBySql("select *, CONCAT(ifnull(course_code,' '),'-',course_title) as course_title from courses where school_type_id = {$school_type_id} AND course_id NOT IN (SELECT course_id FROM course_offering WHERE course_offering_version_id = $model->course_offering_version_id  AND school_type_id = {$school_type_id}) ")->all(),'course_id','course_title'),
                  ]
                 ],
//'admistered_by'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Admistered By...', 'maxlength'=>11]], 

          'admistered_by' => [
                 'type' => Form::INPUT_WIDGET,
                  'widgetClass' => kartik\select2\Select2::className(),
                  'options'=>[
                      'model'=>$model,
                      'attribute'=>'admistered_by',
                      'data'=>\yii\helpers\ArrayHelper::map(app\models\Schools::find()->where(" school_type_id = {$school_type_id}")->all(),'school_id','school_name'),
                  ]
                 ],
        
//'course_offering_version_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Course Offering Version ID...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
