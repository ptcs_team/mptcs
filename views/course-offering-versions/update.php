<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOfferingVersions $model
 */

$this->title = 'Update Course Offering Versions: ' . ' ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Course Offering Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->course_offering_version_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-offering-versions-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
