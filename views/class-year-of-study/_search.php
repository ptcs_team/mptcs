<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudySearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="class-year-of-study-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cyos_id') ?>

    <?= $form->field($model, 'cy_id') ?>

    <?= $form->field($model, 'cyos_description') ?>

    <?= $form->field($model, 'cyos_number') ?>

    <?= $form->field($model, 'cyos_status_id') ?>

    <?php // echo $form->field($model, 'approval_status_id') ?>

    <?php // echo $form->field($model, 'publish_status_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
