<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudy $model
 */

$this->title = 'Update Class Year Of Study: ' . ' ' . $model->cyos_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cyos_id, 'url' => ['view', 'id' => $model->cyos_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="class-year-of-study-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
