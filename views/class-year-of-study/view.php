<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYearOfStudy $model
 */

$this->title = $model->cyos_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-year-of-study-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'cyos_id',
            'cy_id',
            'cyos_description',
            'cyos_number',
            'cyos_status_id',
            'approval_status_id',
            'publish_status_id',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->cyos_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
