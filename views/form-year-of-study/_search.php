<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudySearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="form-year-of-study-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fyos_id') ?>

    <?= $form->field($model, 'fy_id') ?>

    <?= $form->field($model, 'fyos_description') ?>

    <?= $form->field($model, 'fyos_number') ?>

    <?= $form->field($model, 'fyos_status_id') ?>

    <?php // echo $form->field($model, 'approval_status_id') ?>

    <?php // echo $form->field($model, 'publish_status_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
