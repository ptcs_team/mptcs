<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudy $model
 */

$this->title = 'Update Form Year Of Study: ' . ' ' . $model->fyos_id;
$this->params['breadcrumbs'][] = ['label' => 'Form Year Of Studies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fyos_id, 'url' => ['view', 'id' => $model->fyos_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-year-of-study-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
