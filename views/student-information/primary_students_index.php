<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\StudentYearOfStudySearch;
use kartik\tabs\TabsX;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\StudentYearOfStudySearch $searchModel
 */
$this->title = ' Full Name: ' . app\models\VwPrimaryStudents::findOne($user_id)->fullname;
$this->params['breadcrumbs'][] = "Course Results";
?>
<h4><span class="label label-success">School Name </span>:<span class="label label-info"><?= app\models\Schools::findOne($school_id)->school_name. ' '. \app\models\SchoolType::findOne(app\models\Schools::findOne($school_id)->school_type_id)->school_type?></span> &nbsp;&nbsp; 
</h4><br>
<div class="student-year-of-study-index">
    <ul class="list-group">
        <li class="list-group-item"><b>Current Student Status:</b>&nbsp;&nbsp;&nbsp;  </li>
        <li class="list-group-item"><b>Note:</b> </li>
        <li class="list-group-item"><b>Remarks: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </li>
        </li>

    </ul>

    <?php
    $student_years_of_study_models = app\models\PrimaryStudentYearOfStudy::find()->where("user_id = {$user_id}")->all();
    $study_years = [];
    foreach ($student_years_of_study_models AS $student_year_of_study_model) {

        $academic_year = app\models\AcademicYears::findOne(app\models\ClassYear::findOne($student_year_of_study_model->cy_id)->academic_year_id)->academic_year;
         $year_of_study = \app\models\ClassDescription::find()->where("class_number = {$student_year_of_study_model->year_of_study}")->one()->description;
         $year_gpa = ' ';
        $arrayItem = [
            'label' => $year_of_study ." - ". $academic_year,
            'content' => '<ul class="nav nav-pills" role="tablist">
  <li role="presentation">' . $year_of_study . ' Marks = <span class="badge">' . $year_gpa . '</span></a></li>
</ul>' . "<br>" . $this->render('_primary_student_course_results', ['cy_id' => $student_year_of_study_model->cy_id, 'user_id' => $user_id,'primary_student_year_of_study_id'=>$student_year_of_study_model->primary_student_year_of_study_id]),
            'id' => $student_year_of_study_model->primary_student_year_of_study_id,
            'activeTab' => ($student_year_of_study_model->primary_student_year_of_study_id == $activeTab),
        ];

        array_push($study_years, $arrayItem);
    }

    echo kartik\tabs\TabsX::widget([
        'items' => $study_years,
        'position' => TabsX::POS_ABOVE,
        'bordered' => true,
        'encodeLabels' => false
    ]);
    ?>
</div>
