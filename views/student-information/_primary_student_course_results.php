<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\data\SqlDataProvider;
?>
<div class="vw-primary-students-index">

    <?php
    $sc_id = app\models\StudentClasses::find()->where(" cy_id = {$cy_id} AND primary_student_year_of_study_id = {$primary_student_year_of_study_id}")->one()->sc_id;
    $searchModel = new app\models\StudentClassCoursesSearch();
    $condition = "sc_id = {$sc_id}";
    $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'cyos_course_id',
            [
                'label' => 'Course Title',
                'attribute' => 'cyos_course_id',
                'value' => function ($model) {
                    return app\models\Courses::findOne(app\models\CourseOffering::findOne(\app\models\ClassYearOfStudyCourses::findOne($model->cyos_course_id)->course_offering_id)->course_id)->course_title;
                }
            ],
            [
                'label' => 'Result',
                'attribute' => 'result',
                'value' => function($model) {
                    if ($model->result == NULL) {
                        return '--';
                    } else {
                        return $model->result;
                    }
                },
            ],
            [
                'label' => 'Grade',
                'attribute' => 'grade_scored',
                'value' => function ($model) {
                    if ($model->result == NULL) {
                        return'--';
                    } else {
                        return \app\models\GradingSystem::findOne($model->grade_scored)->grade;
                    }
                }
            ],
            'grade_points',
            //'student_class_course_status_id',
            'remarks',
//  
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => false,
//        'panel' => [
//            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
//            'type'=>'info',
//            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
//            'showFooter'=>false
//        ],
    ]);
    ?>

</div>
