<?php
use kartik\tabs\TabsX;
?>
<?php
$condition_for_approved_and_published_results = 2;
$py_semesters_model = app\models\ProgrammeYearSemesters::find()->where("py_id={$student_year_of_study_model->py_id} AND publish_status_id = {$condition_for_approved_and_published_results}")->all();

if ($py_semesters_model != NULL) {
    $semesters = [];
foreach ($py_semesters_model AS $py_semester_model) {
    $ss_id=  \app\models\StudentSemesters::find()->where("programme_year_semester_id={$py_semester_model->py_semester_id} and student_year_of_study_id={$student_year_of_study_model->student_year_of_study_id}")->one()->ss_id;
    $arrayItem = [
        'label' => app\models\ProgrammeYearSemesters::getSemesterDesc($py_semester_model->semester_number),
        'content' => $this->render('_student_semester_courses', ['py_semester_model' => $py_semester_model,'student_year_of_study_model'=>$student_year_of_study_model]).'<ul class="nav nav-pills" role="tablist">
  <li role="presentation">'.app\models\ProgrammeYearSemesters::getSemesterDesc($py_semester_model->semester_number)." GPA = ".' <span class="badge">'.app\models\Students::getStudentSemesterGPA($ss_id).'</span></a></li>
</ul>',
        'id' => $py_semester_model->py_semester_id,
        'activeTab' => ($py_semester_model->py_semester_id == $ActiveTab),
    ];

    array_push($semesters, $arrayItem);
}

echo kartik\tabs\TabsX::widget([
    'items' => $semesters,
    'position' => TabsX::POS_ABOVE,
    'bordered' => true,
    'encodeLabels' => false
]);
    
}  else {
    
 echo '<div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Sorry! Results Are Not Yet Published</strong> 
</div>';
    
}


?>