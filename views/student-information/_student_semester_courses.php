<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

//$sys_model = app\models\StudentYearOfStudy::find()->where("user_id={$student_id} and py_id={$py_id}")->one();
//die((string)($py_id));

$student_semester_model = \app\models\StudentSemesters::find()->where("student_year_of_study_id={$student_year_of_study_model->student_year_of_study_id} AND programme_year_semester_id={$py_semester_model->py_semester_id}")->one();
$searchModel = new \app\models\VwRegisteredCourseSearch();
$condition = "ss_id={$student_semester_model->ss_id}";
$dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\VwRegisteredCoursesSearch $searchModel
 */
//$this->title = 'Vw Registered Courses';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-registered-course-index">


    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'ssc_id',
            //'ss_id',
            //'py_semester_course_id',
            //'score_type',
            //'CA_result',
//            'UE_result', 
//            'final_result', 
//            'grade_scored', 
//            'student_course_status_id', 
//            'changes_log:ntext', 
//            'last_updated', 
//            'is_core', 
//            'units', 
//            'pass_grage', 
//            'CA_Weight', 
//            'UE_Weight', 
//            'after_supp_grade_id', 
//            'after_carryover_grade_id', 
//            'minimum_CA', 
//            'minimum_UE', 
//            'allowed_to_supp_grade_id', 
//            'allowed_to_repeat_grade_id', 
//            'semester_number', 
//            'ps_id', 
//            'academic_year_id', 
//            'study_year', 
            'course_code',
            'course_title',
            [
                'label' => 'Grade',
                'attribute' => 'grade_scored',
                'value' => function ($model) {
                    if ($model->grade_scored == 0) {
                        return '--';
                    } else {
                        return app\models\GradingSystem::findOne($model->grade_scored)->grade;
                    }
                }
            ],
            [
                'label' => 'Remarks',
                'attribute' => 'student_course_status_id',
                'value' => function ($model) {

                    if ($model->grade_scored != 0) {
                        if ($model->grade_scored <= $model->pass_grage) {
                            return 'PASS';
                        } else {
                            return 'FAIL';
                        }

                        //return app\models\StudentCourseStatus::findOne($model->student_course_status_id)->course_status;
                    } else {

                        return '--';
                    }
                }
            ],
            [
                'attribute' => 'is_core',
                'label' => 'Core/Optional',
                'value' => function ($model) {
                    if ($model->is_core == 1) {
                        return 'Core';
                    } else {
                        return 'Optional';
                    }
                }
            ],
            'units',
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['vw-registered-course/view','id' => $model->ssc_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => false,
//
//        'panel' => [
//            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> </h3>',
//            'type'=>'DEFAULT',
//            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
//            'showFooter'=>false
//        ],
    ]);
    Pjax::end();
    ?>
</div>






