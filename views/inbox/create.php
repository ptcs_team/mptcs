<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Inbox $model
 */

$this->title = 'Compose new Message';
$this->params['breadcrumbs'][] = ['label' => 'Mailbox', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbox-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
