<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Inbox $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="inbox-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
//            'message_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Message ID...']],
//            'sent_by' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Sent By...']],
//            'received_by' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Received By...']],
//            'school_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter School ID...']],
//            'status' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Status...']],
            'subject'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Message...', 'maxlength'=>250]], 
            'body' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Message Body...', 'rows' => 6]],
            'attachment'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Message Attachment...', 'maxlength'=>250]],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Send') : Yii::t('app', 'Send'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
