<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\InboxSearch $searchModel
 */
$this->title = 'Inboxes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbox-index">

    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'subject',
            'body',
            [
                'label' => 'Sender',
                'attribute' => 'sent_by',
                'value' => function ($model) {
                    return ucwords(\app\models\Users::findOne($model->sent_by)->firstname) . '  ' . ucwords(\app\models\Users::findOne($model->sent_by)->middlename) . ' ' . ucwords(\app\models\Users::findOne($model->sent_by)->surname).' '.'['.\app\models\Users::findOne($model->sent_by)->email_address.']';
                }
            ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                    'update' => function ($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['inbox/view', 'id' => $model->inbox_id, 'edit' => 't']), [
//                                    'title' => Yii::t('yii', 'Edit'),
//                        ]);
//                    }
//                        ],
//                    ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        'floatHeader' => false,
        'panel' => [
            'heading' => '&nbsp',
            'type' => 'default',
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
    ]);
    Pjax::end();
    ?>

</div>
