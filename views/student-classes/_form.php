<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClasses $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-classes-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'cy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Cy ID...']], 

'primary_student_year_of_study_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Primary Student Year Of Study ID...', 'maxlength'=>11]], 

'class_year_of_study_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Class Year Of Study Number...']], 

'student_class_status'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Class Status...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
