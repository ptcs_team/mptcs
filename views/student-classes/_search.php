<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClassesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-classes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'sc_id') ?>

    <?= $form->field($model, 'cy_id') ?>

    <?= $form->field($model, 'primary_student_year_of_study_id') ?>

    <?= $form->field($model, 'class_year_of_study_number') ?>

    <?= $form->field($model, 'student_class_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
