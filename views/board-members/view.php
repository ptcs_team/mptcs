<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\BoardMembers $model
 */

$this->title = $model->board_members_id;
$this->params['breadcrumbs'][] = ['label' => 'Board Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-members-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'board_members_id',
            'school_board_id',
            'user_id',
            'is_active',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->board_members_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
