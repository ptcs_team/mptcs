<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormDescription $model
 */

$this->title = 'Update Form: ' . ' ' . $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->description, 'url' => ['view', 'id' => $model->form_description_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-description-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
