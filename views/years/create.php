<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Years $model
 */

$this->title = 'Create Years';
$this->params['breadcrumbs'][] = ['label' => 'Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="years-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
