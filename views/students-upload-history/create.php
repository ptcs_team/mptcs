<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentsUploadHistory $model
 */

$this->title = 'Create Students Upload History';
$this->params['breadcrumbs'][] = ['label' => 'Students Upload Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-upload-history-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
