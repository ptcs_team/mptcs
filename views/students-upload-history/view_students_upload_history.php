<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\StudentsUploadHistorySearch $searchModel
 */

$this->title = 'Students Upload History';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="students-upload-history-index">
  

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'students_upload_history_id',
//            'user_id',
                [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function ($model) {
                    return app\models\Users::findOne($model->user_id)->firstname . ' ' . app\models\Users::findOne($model->user_id)->middlename . ' ' . app\models\Users::findOne($model->user_id)->surname;
                }
            ],
                    
            //'py_id',
            'ip_address',
            'client_details',
       [
                'label' => 'Download',
                'attribute' => 'uploaded_file',
                'value' => function($model) {
                    if ($model->uploaded_file == NULL) {
                        return '-';
                    } else {
                        return Html::a('Download', '' . $model->uploaded_file);
                    }
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Report',
                'attribute' => 'upload_report',
                'value' => function($model) {
                    if ($model->upload_report == NULL) {
                        return '-';
                    } else {
                        return Html::a('Download', Yii::$app->urlManager->createUrl(['students-upload-history/upload-report', 'students_upload_history_id' => $model->students_upload_history_id,]), [
                        ]);
                        // return Html::a('Download', '' . $model->upload_report);
                    }
                },
                        'format' => 'raw',
                    ],
//            'uploaded_file', 
//            ['attribute'=>'date_uploaded','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
//            'upload_report:ntext', 

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['students-upload-history/view','id' => $model->students_upload_history_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>' ',
            'type'=>'default',
           // 'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
