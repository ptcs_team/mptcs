<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentsUploadHistory $model
 */

$this->title = 'Update Students Upload History: ' . ' ' . $model->students_upload_history_id;
$this->params['breadcrumbs'][] = ['label' => 'Students Upload Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->students_upload_history_id, 'url' => ['view', 'id' => $model->students_upload_history_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="students-upload-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
