<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentsUploadHistory $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="students-upload-history-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'user_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User ID...']], 

'py_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Py ID...']], 

'ip_address'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Ip Address...', 'maxlength'=>200]], 

'client_details'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Client Details...', 'maxlength'=>250]], 

'uploaded_file'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Uploaded File...', 'maxlength'=>250]], 

'upload_report'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Upload Report...','rows'=> 6]], 

'date_uploaded'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
