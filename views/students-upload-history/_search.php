<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\StudentsUploadHistorySearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="students-upload-history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'students_upload_history_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'py_id') ?>

    <?= $form->field($model, 'ip_address') ?>

    <?= $form->field($model, 'client_details') ?>

    <?php // echo $form->field($model, 'uploaded_file') ?>

    <?php // echo $form->field($model, 'date_uploaded') ?>

    <?php // echo $form->field($model, 'upload_report') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
