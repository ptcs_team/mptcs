<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Districts $model
 */

$this->title = 'Update Districts: ' . ' ' . $model->district_id;
$this->params['breadcrumbs'][] = ['label' => 'Districts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->district_id, 'url' => ['view', 'id' => $model->district_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="districts-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
