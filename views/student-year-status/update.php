<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentYearStatus $model
 */

$this->title = 'Update Student Year Status: ' . ' ' . $model->student_year_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Year Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->student_year_status_id, 'url' => ['view', 'id' => $model->student_year_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-year-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
