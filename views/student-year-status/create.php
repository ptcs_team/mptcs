<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentYearStatus $model
 */

$this->title = 'Create Student Year Status';
$this->params['breadcrumbs'][] = ['label' => 'Student Year Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-year-status-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
