<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYear $model
 */

$this->title = 'Update Form Year: ' . ' ' . $model->fy_id;
$this->params['breadcrumbs'][] = ['label' => 'Form Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fy_id, 'url' => ['view', 'id' => $model->fy_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="form-year-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
