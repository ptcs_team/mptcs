<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYear $model
 */

$this->title = 'Create Form Year';
$this->params['breadcrumbs'][] = ['label' => 'Form Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-year-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
