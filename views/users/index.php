<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\UsersSearch $searchModel
 */

$this->title = 'Staff';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* echo Html::a('Create Users', ['create'], ['class' => 'btn btn-success'])*/  ?>
    </p>

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'user_id',
//            'student_status_id',
//            'registration_number',
                                 [
    'attribute'=>'username', 
    'vAlign'=>'middle',
    'width'=>'200px',
    'value'=>$model->username,
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(app\models\Users::find()->orderBy('user_id')->where("user_type = 2")->asArray()->all(), 'username', 'username'), 
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>'Search Any UserName'],
    'format'=>'raw'
],
            //'firstname',
                      [
    'attribute'=>'firstname', 
    'vAlign'=>'middle',
    'width'=>'200px',
    'value'=>$model->firstname,
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(app\models\Users::find()->orderBy('user_id')->where("user_type = 2")->asArray()->all(), 'firstname', 'firstname'), 
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>'Search Any FirstName'],
    'format'=>'raw'
],
            
            //'surname', 
                                [
    'attribute'=>'surname', 
    'vAlign'=>'middle',
    'width'=>'200px',
    'value'=>$model->surname,
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(app\models\Users::find()->orderBy('user_id')->where("user_type = 2")->asArray()->all(), 'surname', 'surname'), 
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>'Search Any SurName'],
    'format'=>'raw'
],          
//            'middlename',
//            'ps_id',
//            'sex',
//            'date_of_birth',
//            'place_of_birth',
//            'mailing_address',
//            'telephone_no',
              'email_address:email',

//            'admission_date',
//            'sponsorship_type',
//            'status_comments',
//            ['attribute'=>'completion_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
            //'username',
//            'password',
           //'is_active',                
        [
    'attribute'=>'is_active', 
    'vAlign'=>'middle',
    'width'=>'100px',
     'value'=>  function ($model){
                    return $model->is_active ==1? Active: Inactive;
                },
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>['1'=>'Active','0'=>'Inactive'],
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>'Search...'],
    'format'=>'raw'
], 
//            'login_counts',
//            ['attribute'=>'last_login','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'user_type',
//            'staff_position_id',
//            'salutation',
//            ['attribute'=>'last_password_update_date','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//            'auth_key',
//            'password_reset_token',
//            ['attribute'=>'created_at','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//            'created_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view} {delete} {deactivate} {activate}',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['users/view','id' => $model->user_id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);},
                                                          
                          'deactivate' => function ($url, $model, $key) {

                        $url = Yii::$app->urlManager->createUrl(['users/index', 'id_to_deactivate' => $model->user_id, 'subaction'=>'deactivate']);
                        if ($model->user_id != 0) {


                            return Html::a('<span class="glyphicon glyphicon-lock">Deactivate</span>', $url);
                        }
                    },
                    'activate' => function ($url, $model, $key) {

                        $url = Yii::$app->urlManager->createUrl(['users/index', 'id_to_activate' => $model->user_id, 'subaction' => 'activate']);
                        if ($model->user_id != 0) {


                            return Html::a('<span class="glyphicon glyphicon-off">Activate</span>', $url);
                        }
                    },

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>'<h3 class="panel-title"> </h3>',
            'type'=>'default',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add Staff', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>