<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$this->title = $model->firstname . ' '. Yii::$app->user->identity->surname . " (" . Yii::$app->user->identity->username . ")";
$this->params['breadcrumbs'][] = ['label' => 'My Profile'];
//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="users-view">
    <?php
    if ($model->profile_picture == NULL) {
    $picture = 'default-no-image/default-no-profile-image.png';
} else {
    $picture = $model->profile_picture;
}
?>

    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => 'view',
        'bordered' => $bordered,
        'striped' => $striped,
        'condensed' => $condensed,
        'responsive' => $responsive,
        'hideIfEmpty' => true,
        'hover' => true,
        'hAlign' => $hAlign,
        'vAlign' => $vAlign,
        'fadeDelay' => $fadeDelay,
        //'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => '&nbsp',
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            [
                'group' => true,
                'label' => 'Profile Photo' . ' ' . "<i class='glyphicon glyphicon-picture'></i>" . Html::a("<i> Attach a photo</i>&nbsp<i class='glyphicon glyphicon-camera'></i>", \yii\helpers\Url::to(['users/updatepic', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right', 'title' => Yii::t('yii', 'Add a photo to your profile')]) . '<span></li>',
                'rowOptions' => ['class' => 'default'],
            ],
            [
                'group' => true,
                'label' => '   <div class="row">
            <div class="col-xs-6 col-md-3">
                <a href="#" class="thumbnail">
                    <img  src="' . $picture. '" width=800 height=900" alt=" Upload Profile Photo">
                </a>
            </div>

        </div>',
            ],
//            [
//                'group' => true,
//                'label' => $model->user_type == 2 ? 'Staff Identification Information' . ' ' . "<i class='glyphicon glyphicon-user'></i>" . Html::a("<i>Update Info</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['users/updateidentification', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right', 'title' => Yii::t('yii', 'Update Your Information')]) . '<span></li>' : 'Student Identification Information' . ' ' . "<i class='glyphicon glyphicon-user'></i>" . Html::a("<i>Update Info</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['users/updateidentification', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right', 'title' => Yii::t('yii', 'Update Your Information')]) . '<span></li>',
//                'rowOptions' => ['class' => 'default'],
//            //'groupOptions'=>['class'=>'text-center']
//            ],
////          'user_id',
//            // 'student_status_id',
//            // 'registration_number',
//            [
//                'attribute' => 'registration_number',
//                'value' => $model->registration_number,
//                'visible' => $model->user_type == 1,
//            ],
//            'username',
//            'salutation',
//             'firstname',
//            'middlename',
//            'surname',
//           
//            // 'ps_id',
//            [
//                'label' => 'Programme Name',
//                'attribute' => 'pintake_id',
//                'value' => app\models\Programmes::findOne(\app\models\ProgrammeIntakes::findOne($model->pintake_id)->programme_id)->programme_name.'('.app\models\Programmes::findOne(\app\models\ProgrammeIntakes::findOne($model->pintake_id)->programme_id)->programme_acronmy.')',
//                'visible' => $model->user_type == 1,
//            ],
//             [
//                'label' => 'Programme Intake',
//                'attribute' => 'pintake_id',
//                'value' => \app\models\Intakes::findOne(\app\models\ProgrammeIntakes::findOne($model->pintake_id)->intake_id)->intake_description.' '.'-'.' '.\app\models\Months::findOne(\app\models\Intakes::findOne(\app\models\ProgrammeIntakes::findOne($model->pintake_id)->intake_id)->start_month)->month,
//                'visible' => $model->user_type == 1,
//            ],
////          'user_id',
//            // 'student_status_id',
//            // 'registration_number',
//            [
//                'attribute' => 'session_id',
//                'label' => 'Programme Session',
//                'value'=>  \app\models\Sessions::findOne($model->session_id)->session_description,
//                'visible' => $model->user_type == 1,
//            ],
            //'sex',
            [
                'attribute' => 'sex',
                'value' => $model->sex == M ? 'Male' : 'Female',
            ],
            'date_of_birth',
            'place_of_birth',
            'mailing_address',
            'telephone_no',
            'email_address:email',
//            'admission_date',
            //           'sponsorship_type',
//            'status_comments',
//            [
//                'attribute' => 'completion_date',
//                'format' => ['date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
//                'type' => DetailView::INPUT_WIDGET,
//                'widgetOptions' => [
//                    'class' => DateControl::classname(),
//                    'type' => DateControl::FORMAT_DATE
//                ]
//            ],
//            'password',
            //'is_active',
            //  'login_counts',
            [
                'attribute' => 'last_login',
                'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A'],
                'type' => DetailView::INPUT_WIDGET,
                'widgetOptions' => [
                    'class' => DateControl::classname(),
                    'type' => DateControl::FORMAT_DATETIME
                ]
            ],
            [
                'group' => true,
                'label' => 'Password' . ' ' . "<i class='glyphicon glyphicon-lock'></i>" . Html::a("<i>Reset</i>&nbsp<i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['users/changepassword', 'id' => $model->user_id]), ['class' => 'btn btn success pull-right', 'title' => Yii::t('yii', 'Reset Your Password')]) . '<span></li>',
                'rowOptions' => ['class' => 'default']
            ],
//            'user_type',
        // 'staff_position_id',
        //'salutation',
//            [
//                'attribute'=>'last_password_update_date',
//                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
//                'type'=>DetailView::INPUT_WIDGET,
//                'widgetOptions'=> [
//                    'class'=>DateControl::classname(),
//                    'type'=>DateControl::FORMAT_DATE
//                ]
//            ],
//            'auth_key',
//            'password_reset_token',
//            [
//                'attribute'=>'created_at',
//                'format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y'],
//                'type'=>DetailView::INPUT_WIDGET,
//                'widgetOptions'=> [
//                    'class'=>DateControl::classname(),
//                    'type'=>DateControl::FORMAT_DATE
//                ]
//            ],
//            'created_by',
        ],
//        'deleteOptions' => [
//            'url' => ['delete', 'id' => $model->user_id],
//            'data' => [
//                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
//                'method' => 'post',
//            ],
//        ],
        'enableEditMode' => false,
    ])
    ?>

</div>