<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 */
$user_model = app\models\Users::findOne($id);
$this->title = 'Password Reset for: ' . ' ' .$user_model->firstname." ".$user_model->surname." (".$user_model->username.")";;
$this->params['breadcrumbs'][] = ['label' => 'Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user_model->firstname." ".$user_model->surname." "."(".$user_model->username.")", 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Reset Password';
?>
<div class="users-update">
    <?= $this->render('_passwordreset_form', [
        'model' => $model,
    ]) ?>

</div>
