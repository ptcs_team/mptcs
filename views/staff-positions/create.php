<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StaffPositions $model
 */

$this->title = 'Add Staff Position';
$this->params['breadcrumbs'][] = ['label' => 'Staff Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-positions-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
