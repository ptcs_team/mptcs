<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StaffPositions $model
 */
$this->title = 'Update Staff Position: ' . ' ' . $model->position_name;
$this->params['breadcrumbs'][] = ['label' => 'Staff Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->position_name, 'url' => ['view', 'id' => $model->staff_position_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="staff-positions-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
