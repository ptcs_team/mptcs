<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\widgets\FileInput;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionStructure $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="institution-structure-form">
    <?php if (!$model->isNewRecord) { ?>

        <table>
            <tr><td>
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <?php
                                if ($model->logo == NULL) {
                                    $logo = 'default-no-image/no-logo.png';
                                } else {
                                    $logo = $model->logo;
                                }
                                ?>
                                <img src="<?php echo $logo; ?>"   style="width: 650px; height: 150px;" alt="Logo">
                            </a>
                        </div>

                    </div>
                </td>
                <td>

                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <a href="#" class="thumbnail">
                                <?php
                                if ($model->logo2 == NULL) {
                                    $logo2 = 'default-no-image/no-logo.png';
                                } else {
                                    $logo2 = $model->logo2;
                                }
                                ?>
                                <img src="<?php echo $logo2; ?>" style="width: 650px; height: 150px;"alt="Logo">
                            </a>
                        </div>

                    </div>  
                </td>
            </tr>
        </table>





    <?php } ?>

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    $inst_items = yii\helpers\ArrayHelper::map(app\models\InstitutionStructure::find()->all(), 'institution_structure_id', 'institution_name');

    $inst_items = [0 => 'No Parent'] + $inst_items;

    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [

            'institution_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Institution Name...', 'maxlength' => 150]],
            'institution_acronym' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Institution Acronym...', 'maxlength' => 70]],
            'parent_institution_structure_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => $inst_items, 'options' => ['prompt' => '']],
            'institution_type_id' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => yii\helpers\ArrayHelper::map(\app\models\InstitutionType::find()->all(), 'institution_type_id', 'institution_type_name'), 'options' => ['prompt' => '']],
            'status' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items' => [1 => 'Active', 0 => 'Inactive'],
                'options' => ['prompt' => 'Select Status...']],
            'phone' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Phone...', 'maxlength' => 100]],
            'fax' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Fax...', 'maxlength' => 100]],
            'email' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email...', 'maxlength' => 100]],
            'website' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Website...', 'maxlength' => 100]],
            'post_office_box' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Post Office Box. eg.P.O Box 77376667', 'maxlength' => 100]],
            'region' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Region...', 'maxlength' => 100]],
            'country' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Country...', 'maxlength' => 100]],
            'logo_image' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => FileInput::className(),
                'label' => $model->isNewRecord ? "Institution Logo" : "Institution Logo (Will replace existing one)",
                ['options' => ['accept' => 'image/*']],
                'pluginOptions' => ['previewFileType' => 'image'],
            ],
            'logo2_image' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => FileInput::className(),
                'label' => $model->isNewRecord ? "Another Institution Logo (Optional)" : "Another Institution Logo (Will replace existing one (Optional))",
                ['options' => ['accept' => 'image/*']],
                'pluginOptions' => ['previewFileType' => 'image'],
            ],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
