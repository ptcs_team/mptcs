<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionStructure $model
 */

$this->title = 'Create Institution Structure';
$this->params['breadcrumbs'][] = ['label' => 'Institution Structure', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-structure-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
