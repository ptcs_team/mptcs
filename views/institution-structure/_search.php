<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionStructureSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="institution-structure-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'institution_structure_id') ?>

    <?= $form->field($model, 'institution_name') ?>

    <?= $form->field($model, 'institution_acronym') ?>

    <?= $form->field($model, 'parent_institution_structure_id') ?>

    <?= $form->field($model, 'institution_type_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
