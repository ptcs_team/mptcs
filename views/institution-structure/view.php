<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\InstitutionStructure $model
 */

$this->title = \app\models\InstitutionStructure::findOne($model->institution_structure_id)->institution_name;
$this->params['breadcrumbs'][] = ['label' => 'Institution Structures', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institution-structure-view">
    
          <?php
                                if ($model->logo == NULL) {
                                    $logo = 'default-no-image/no-logo.png';
                                } else {
                                    $logo = $model->logo;
                                }
                                
                                 if ($model->logo2 == NULL) {
                                    $logo2 = 'default-no-image/no-logo.png';
                                } else {
                                    $logo2 = $model->logo2;
                                }
                                
                                ?>
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=> Html::a("Update <i class='glyphicon glyphicon-pencil'></i>", \yii\helpers\Url::to(['institution-structure/update','id'=>$model->institution_structure_id]), ['class'=>'btn btn success']).'<span></li>',
        
            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
           // 'institution_structure_id',
             [
                 'label'=> 'Logo 1',
                'attribute'=>'logo',
                'value'=>$logo,
                'format' => ['image',['width'=>'100','height'=>'100']],
                
            ],
              [
                 'label'=> 'Logo 2',
                'attribute'=>'logo2',
                'value'=>$logo2,
                'format' => ['image',['width'=>'100','height'=>'100']],
                
            ],
            'institution_name',
            'institution_acronym',
            //'parent_institution_structure_id',
            [
               'attribute'=> 'parent_institution_structure_id',
                'value'=>$model->parent_institution_structure_id == 0 ? 'No Parent':\app\models\InstitutionStructure::findOne($model->parent_institution_structure_id)->institution_name,

              
                
            ],
           // 'institution_type_id',
            [
                'attribute'=>'institution_type_id',
                'value'=>  app\models\InstitutionType::findOne($model->institution_type_id)->institution_type_name,
                
            ],
            //'status',
            [
                'attribute'=> 'status',
                'value'=>$model->status == 1? 'Active':'Not Active',
                
            ],
            'phone',
            'fax',
            'email:email',
            'website',
            'post_office_box',
            'region',
            'country',
           
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->institution_structure_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>false,
    ]) ?>

</div>