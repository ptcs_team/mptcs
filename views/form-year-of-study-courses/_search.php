<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudyCoursesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="form-year-of-study-courses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'fyos_course_id') ?>

    <?= $form->field($model, 'fy_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'course_offering_id') ?>

    <?= $form->field($model, 'pass_grade_id') ?>

    <?php // echo $form->field($model, 'contribute_to_final_result') ?>

    <?php // echo $form->field($model, 'is_core') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
