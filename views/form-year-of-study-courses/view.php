<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearOfStudyCourses $model
 */

$this->title = $model->fyos_course_id;
$this->params['breadcrumbs'][] = ['label' => 'Form Year Of Study Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-year-of-study-courses-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'fyos_course_id',
            'fy_id',
            'school_id',
            'course_offering_id',
            'pass_grade_id',
            'contribute_to_final_result',
            'is_core',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->fyos_course_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
