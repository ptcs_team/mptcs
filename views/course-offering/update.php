<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CourseOffering $model
 */

$this->title = 'Update Course Offering: ' . ' ' . $model->course_offering_id;
$this->params['breadcrumbs'][] = ['label' => 'Course Offerings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->course_offering_id, 'url' => ['view', 'id' => $model->course_offering_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="course-offering-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
