<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentFormCourses $model
 */

$this->title = 'Create Student Form Courses';
$this->params['breadcrumbs'][] = ['label' => 'Student Form Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-form-courses-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
