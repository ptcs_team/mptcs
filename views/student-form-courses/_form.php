<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentFormCourses $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-form-courses-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'fy_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fy ID...']], 

'fyos_course_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Fyos Course ID...', 'maxlength'=>20]], 

'score_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Score Type...']], 

'result'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Result...']], 

'grade_scored'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Grade Scored...']], 

'student_form_course_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Form Course Status ID...']], 

'remarks'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Remarks...', 'maxlength'=>100]], 

'grade_points'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Grade Points...']], 

'changes_log'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Changes Log...','rows'=> 6]], 

'last_updated'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Last Updated...']], 

'sf_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Sf ID...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
