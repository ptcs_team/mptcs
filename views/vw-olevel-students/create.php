<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\VwOlevelStudents $model
 */

$this->title = 'Register Olevel Student';
$this->params['breadcrumbs'][] = ['label' => 'Olevel Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-olevel-students-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
