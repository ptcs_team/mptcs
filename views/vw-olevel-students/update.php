<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\VwOlevelStudents $model
 */
$fullname = strtoupper($model->surname).', '.$model->firstname.' '.$model->middlename;
$this->title = 'Update Olevel Students: ' . ' ' . $fullname;
$this->params['breadcrumbs'][] = ['label' => ' Olevel Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $fullname, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vw-olevel-students-update">
    <?= $this->render('_form_update', [
        'model' => $model,
          'academic_year_id'=>$academic_year_id,
           'year_of_study'=>$year_of_study,
    ]) ?>

</div>
