<?php

$form_years = \app\models\FormYear::find()->where(" school_id = {$school_id} and academic_year_id = {$academic_year_id} order by study_year ASC ")->all();
?>

<ul class="list-group" style="margin-left: 200px;">
  <?php
  foreach ($form_years as $fyear) {
     echo '<li class="list-group-item">'.yii\helpers\Html::a(strtoupper(\app\models\FormDescription::find()->where(" form_number = {$fyear->study_year}")->one()->description), \yii\helpers\Url::to(['vw-olevel-students/form-year-students','fy_id'=>$fyear->fy_id,'school_id'=>$school_id])).'<span class="badge pull-right" style="margin-right: 450px;">'. app\models\OlevelStudentYearOfStudy::find()->where("fy_id = {$fyear->fy_id}")->count().'</span></li>';
  }
  ?> 
</ul>