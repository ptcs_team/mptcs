<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\VwCourseResults $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="vw-course-results-form">


    <?php
    $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => [
                    'enctype' => "multipart/form-data",
                ],
    ]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 4,
        'attributes' => [
            'file' => ['type' => Form::INPUT_FILE, 'options' => ['placeholder' => 'Please browse an excel file']],
            'action1' => ['type' => Form::INPUT_RAW, 'value' => '<br>' . Html::submitButton('<span class="glyphicon glyphicon-upload"></span> Upload Students', ['class' => 'btn btn-primary'])],
            //'action2'=>['type'=>  Form::INPUT_RAW, 'value'=>'<br>'.Html::a('<i class="glyphicon glyphicon-circle-arrow-down"></i> Download Excel Template', ['students-upload-excel-template','py_id'=>$py_id], ['class' => 'btn btn-success', 'style' => 'margin-left: 20px'])],
            'action2' => ['type' => Form::INPUT_RAW, 'value' => '<br>' . Html::a('<i class="glyphicon glyphicon-circle-arrow-down"></i> Download Excel Template', 'upload-templates/grid-students_upload_template.xlsx', ['class' => 'btn btn-success', 'style' => 'margin-left: 20px'])],
        ],
    ]);


    ActiveForm::end();
    ?>
