<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;

/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL,]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [

//'student_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Status ID...']], 
//'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...',
//     'disabled' => $model->isNewrecord ? false : true,
//    ]], 
            'is_active' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Is Active?',
                'options' => [

                    'data' => ['1' => 'Active', '0' => 'InActive'],
                    'options' => [
                        'prompt' => '',
                        'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
            ],
            'user_type' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'User Type',
                'options' => [

                    'data' => ['2' => 'Garage Owner', '3' => 'Normal Customer'],
                    'options' => [
                        'prompt' => '',
                        'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
            ],
            'username' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Username...', 'maxlength' => 200,
                    'disabled' => $model->isNewrecord ? false : true,
                ]],
//'login_counts'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Login Counts...']], 
            //'user_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter User Type...']], 
            'firstname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Firstname...', 'maxlength' => 20]],
            'middlename' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Middlename...', 'maxlength' => 20]],
             'surname' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Surname...', 'maxlength' => 20]],
            'sex' => ['type' => Form::INPUT_DROPDOWN_LIST,
                'items' => ['M' => 'Male', 'F' => 'Female'],
                'options' => ['prompt' => '']],
            'salutation' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Salutation...', 'maxlength' => 20]],
//'completion_date'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATE]], 
//'last_login'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
//'registration_number'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Registration Number...', 'maxlength'=>255]], 
            'date_of_birth' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => kartik\date\DatePicker::classname(), 'options' => ['pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true]],],
//'admission_date'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Admission Date...', 'maxlength'=>255]], 
//'status_comments'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Status Comments...', 'maxlength'=>255]], 
           
//         'sponsorship_type'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Sponsorship Type...', 'maxlength'=>1,
//               'visible' => yii::$app->user->identity->user_type == 1,
//             ]
//           
//             ], 
            'place_of_birth' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Place Of Birth...', 'maxlength' => 50]],
            'mailing_address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Mailing Address...', 'maxlength' => 150]],
            'telephone_no' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => yii\widgets\MaskedInput::classname(),
                'options' => [
                    'mask' => '+255999999999',
                    'clientOptions' => [
                        'alias' => 'telephone_no'
                    ],
                    'options' => ['disabled' => false, 'class' => 'form-control']
                ],
            ],
             
            'email_address' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => yii\widgets\MaskedInput::classname(),
                'options' => [
                    'clientOptions' => [
                        'alias' => 'email'
                    ],
                    'options' => ['disabled' => false, 'class' => 'form-control']
                ],
            ],
        //'telephone_no' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Telephone No...', 'maxlength' => 150]],
        //'email_address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Email Address...', 'maxlength' => 100]],
//'student_id'=>['type'=> TabularForm::INPUT_TEXT, 'options'=>['placeholder'=>'Enter ...']],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
