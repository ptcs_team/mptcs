<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;


/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\LoginsSearch $searchModel
 */
$this->title = 'Logins';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logins-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' =>$dataProvider_logins_History,
        'filterModel' =>$searchModel_logins_History,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'login_id',
            

//            [
//                'attribute' => 'user_id',
//                'vAlign' => 'middle',
//                'width' => '200px',
//                'value' => function ($model) {
//                    return ($model->user->surname . ' ' . $model->user->firstname);
//                },
//               
//            ],
                        
           'details',
                        
         'ip_address',
        

            ['attribute' => 'datecreated', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['logins/view','id' => $model->login_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        //'floatHeader' => true,
        'panel' => [
            'heading' => '<h3 class="panel-title"> </h3>',
            'type' => 'default',
            'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
    ]);
    Pjax::end();
    ?>

</div>
