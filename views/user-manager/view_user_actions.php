<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\UserAuditTrailSearch $searchModel
 */
$this->title = 'User Audit Trails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-audit-trail-index">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider_user_audit_trail,
        'filterModel' => $searchModel_user_audit_trail,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            [
//                'label' => 'User',
//                'attribute' => 'user_id',
//                'value' => function ($model) {
//                    return ($model->user->surname . ' ' . $model->user->firstname);
//                },
//            ],
            'details',
            'ip_address',
            'client_details',
            'object',
             'referer',
            ['attribute' => 'datecreated', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['user-audit-trail/view','id' => $model->user_audit_trail_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
        ],
        'responsive' => true,
        'hover' => true,
        'condensed' => true,
        //'floatHeader' => false,
        'panel' => [
            'heading' => '<h3 class="panel-title"></h3>',
            'type' => 'default',
            //'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter' => false
        ],
    ]);
    Pjax::end();
    ?>

</div>
