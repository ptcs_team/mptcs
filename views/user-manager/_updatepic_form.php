<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\builder\TabularForm;
use kartik\widgets\FileInput;


/**
 * @var yii\web\View $this
 * @var app\models\Users $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="users-form" style="padding-left: 300px; padding-right: 300px;">

    <?php 
  $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]); 
    echo Form::widget([
    'model' => $model,
    'form' => $form,
    'columns' =>1,
    'attributes' => [
   'image' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => FileInput::className(),
                'label' => "Profile Photo (Existing one will be replaced)",
                ['options' => ['accept' => 'image/*']],
                'pluginOptions' => ['previewFileType' => 'image'],
            ],
    ]


    ]);
    echo Html::submitButton('Upload', ['type'=>'button', 'class'=>'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
