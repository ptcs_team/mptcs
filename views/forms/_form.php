<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\Forms $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="forms-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'form_name'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Form Name...', 'maxlength'=>250]], 

'admistered_by'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Admistered By...']], 

'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 

'form_acronym'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Form Acronym...', 'maxlength'=>100]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
