<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StudentCourseStatus $model
 */

$this->title = 'Update Student Course Status: ' . ' ' . $model->student_course_status_id;
$this->params['breadcrumbs'][] = ['label' => 'Student Course Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->student_course_status_id, 'url' => ['view', 'id' => $model->student_course_status_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="student-course-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
