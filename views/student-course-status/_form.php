<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StudentCourseStatus $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-course-status-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'student_course_status_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Student Course Status ID...']], 

'course_status'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Course Status...', 'maxlength'=>30]], 

'abbreviation'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Abbreviation...', 'maxlength'=>10]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
