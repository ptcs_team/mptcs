<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\StudentClassCoursesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="student-class-courses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'scc_id') ?>

    <?= $form->field($model, 'cy_id') ?>

    <?= $form->field($model, 'cyos_course_id') ?>

    <?= $form->field($model, 'score_type') ?>

    <?= $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'grade_scored') ?>

    <?php // echo $form->field($model, 'student_class_course_status_id') ?>

    <?php // echo $form->field($model, 'remarks') ?>

    <?php // echo $form->field($model, 'grade_points') ?>

    <?php // echo $form->field($model, 'changes_log') ?>

    <?php // echo $form->field($model, 'last_updated') ?>

    <?php // echo $form->field($model, 'sc_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
