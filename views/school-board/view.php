<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolBoard $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'School Boards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-board-view">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>


    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>$this->title,
            'type'=>DetailView::TYPE_INFO,
        ],
        'attributes' => [
            'school_board_id',
            'school_id',
            'name',
            'description:ntext',
            'is_active',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->school_board_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
