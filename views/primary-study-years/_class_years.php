<?php

use kartik\tabs\TabsX;

$class_years = app\models\ClassYear::find()->where("cy_id = :cy_id AND  school_id = :school_id ", [':cy_id' => $model_cy->cy_id , ':school_id'=>$school_id])->orderBy("cy_id ASC")->all();
$items = [];
$arrayItem = [
    'label' => 'Year Details',
    'content' => $this->render('class_year_details', ['model' => $model_cy]),
    'id' => 'class_year_details_tab'
];

array_push($items, $arrayItem);

foreach ($class_years as $key => $value) {
    $arrayItem = [
        'label' => \app\models\ClassDescription::find()->where("class_number = {$value->year_number}")->one()->description,
        'content' => $this->render('_class_year_courses', [ 
            'model_cy' => $model_cy,
            'model_cyosc' =>$model_cyosc, 
            'school_id'=>$school_id,
            'SemActiveTab' => $SemActiveTab,
             'academic_year_id' => $academic_year_id,
              'model_acy' => $model_acy,
              'activeTab' => $activeTab,
              'cy_id'=>$value->cy_id,
               'study_year'=>$value->study_year,
                ]),
        'id' => 'sem_tab_' . $value->cy_id,
        'active' => ('sem_tab_' . $value->cy_id == $SemActiveTab)
    ];
    array_push($items, $arrayItem);
}

echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'bordered' => true,
    'encodeLabels' =>false
]);
?>
    