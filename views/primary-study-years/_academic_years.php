<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

?>
<div class="academic-years-index">


    <?php  echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            

          [
              'attribute'=>'academic_year',
              'value'=>function($model) use ($ps_id){
                return Html::a($model->academic_year, yii\helpers\Url::to(['study-years/configure-study-year','academic_year_id'=>$model->academic_year_id, 'ps_id'=>$ps_id]));
              },
              'format'=>'raw',
          ]
            


        ],

    ]); ?>

</div>

