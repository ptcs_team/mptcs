<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\AcademicYearsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="academic-years-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'academic_year_id') ?>

    <?= $form->field($model, 'academic_year') ?>

    <?= $form->field($model, 'order') ?>

    <?= $form->field($model, 'is_current') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
