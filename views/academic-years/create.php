<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\AcademicYears $model
 */

$this->title = 'Create Academic Years';
$this->params['breadcrumbs'][] = ['label' => 'Academic Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-years-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
