<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Logins $model
 */

$this->title = 'Update Logins: ' . ' ' . $model->login_id;
$this->params['breadcrumbs'][] = ['label' => 'Logins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login_id, 'url' => ['view', 'id' => $model->login_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="logins-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
