<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\ClassYear $model
 */

$this->title = 'Update Class Year: ' . ' ' . $model->cy_id;
$this->params['breadcrumbs'][] = ['label' => 'Class Years', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cy_id, 'url' => ['view', 'id' => $model->cy_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="class-year-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
