<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\SchoolSponsorsSearch $searchModel
 */

$this->title = 'School Sponsors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-sponsors-index">

    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'school_sponsor_id',
//            'school_id',
            [
                'label'=>'School',
                'attribute'=>'school_id',
                'value'=>  function ($model){
                    return \app\models\Schools::findOne($model->school_id)->school_name;
        
                }
                
            ],
[
                            'label' => 'Sponsor',
                            'attribute' => 'sponsor_id',
                            'value' => function ($model) {
                                return app\models\Sponsors::findOne($model->sponsor_id)->sponsor_name;
                            }
                        ],
//            'what_sponsored:ntext',
//            'amount',
                        [
                            'label' => 'Description',
                            'attribute' => 'what_sponsored',
                            'value' => function ($model) {
                                return $model->what_sponsored;
                            }
                        ],
                        [

                            'label' => 'Amount Sponsored',
                            'attribute' => 'amount',
                            'hAlign' => 'right',
                            'value' => function ($model) {
                                return $model->amount;
                            },
                            'format' => 'raw',
                            'format' => ['decimal', 2]
                        ],
//            
//            ['attribute'=>'date_created','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
                        ['attribute' => 'date_sponsored', 'format' => ['date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//            'is_visible', 
                        [
                            'label' => 'Is Visible?',
                            'attribute' => 'is_visible',
                            'vAlign' => 'middle',
                            'width' => '5%',
                            'value' => function($model) {
                                return $model->is_visible == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                            },
                            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
                        ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-sponsors/view','id' => $model->school_sponsor_id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>'&nbsp',
            'type'=>'default',
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
