<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolSponsorsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-sponsors-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'school_sponsor_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'sponsor_id') ?>

    <?= $form->field($model, 'what_sponsored') ?>

    <?= $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'date_sponsored') ?>

    <?php // echo $form->field($model, 'is_visible') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
