<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolSponsors $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-sponsors-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_HORIZONTAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 1,
    'attributes' => [

'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 

'sponsor_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Sponsor ID...']], 

'what_sponsored'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter What Sponsored...','rows'=> 6]], 

'amount'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Amount...']], 

'date_sponsored'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATE]], 

'is_visible'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Visible...']], 

'date_created'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
