<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolGallery $model
 */

$this->title = 'Create School Gallery';
$this->params['breadcrumbs'][] = ['label' => 'School Galleries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-gallery-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
