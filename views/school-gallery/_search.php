<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolGallerySearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-gallery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'school_gallery_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'gallery') ?>

    <?= $form->field($model, 'is_visible') ?>

    <?= $form->field($model, 'date_uploaded') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
