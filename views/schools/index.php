<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\SchoolsSearch $searchModel
 */

$this->title = 'Schools';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schools-index">
    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'school_id',
             'school_name',
//            'school_type_id',
                 [
                'attribute' => 'school_type_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return \app\models\SchoolType::findOne($model->school_type_id)->school_type;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\SchoolType::find()->orderBy('school_type_id')->all(), 'school_type_id', 'school_type'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'region_id',
//            'district_id',
//            'ward_id',
//            'street_id', 
             [
                'attribute' => 'region_id',
                'vAlign' => 'middle',
                'width' => '200px',
                'value' => function ($model) {
                    return \app\models\Regions::findOne($model->region_id)->region;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Regions::find()->orderBy('region_id')->all(), 'region_id', 'region'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            [
                'attribute' => 'district_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return \app\models\Districts::findOne($model->district_id)->district;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Districts::find()->orderBy('district_id')->all(), 'district_id', 'district'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'ward', 
            [
                'attribute' => 'ward_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return \app\models\Wards::findOne($model->ward_id)->ward;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Wards::find()->orderBy('ward_id')->all(), 'ward_id', 'ward'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
//            'street', 
            [
                'attribute' => 'street_id',
                'vAlign' => 'middle',
                'width' => '150px',
                'value' => function ($model) {
                    return app\models\StreetsVillage::findOne($model->street_id)->street;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\StreetsVillage::find()->orderBy('street_id')->all(), 'street_id', 'street'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Search...'],
                'format' => 'raw'
            ],
            
//            'school_description:ntext', 
//            'address', 
//            'phone_no', 
//            'e_mail', 
//            'fax', 
//            'logo', 
//            'start_year_id', 
//            'google_location:ntext', 
//            'school_privacy_statement_id', 
//            'school_board_id', 
//            'school_donor_id', 
//            'school_sponsor_id', 
//            'school_gallery_id', 
//            'school_motto', 
//            'headmaster_statement_id', 
//            'msg_frm_teacher_id', 
//            'msg_frm_parent_id', 
//            'inbox_id', 

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template'=>'{view}',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['schools/view','id' => $model->school_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
                                             [
                'label' => '',
                'value' => function($model) {
                
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-eye-open"></i> View</span>', Yii::$app->urlManager->createUrl(['schools/view', 'id'=>$model->school_id]), [
                                    'title' => Yii::t('yii', 'View Details'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],
                                                         [
                'label' => '',
                'value' => function($model) {
                
                        return Html::a('<span class=" label label-info"><i class = "glyphicon glyphicon-pencil"></i> Update</span>', Yii::$app->urlManager->createUrl(['schools/update', 'id'=>$model->school_id]), [
                                    'title' => Yii::t('yii', 'Update'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>'',
            'type'=>'default',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
