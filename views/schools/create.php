<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Schools $model
 */

$this->title = 'Add School';
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schools-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
