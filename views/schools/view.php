
<script>
    function addBoardMembers(school_board_id) {
        //alert(RegSelectedStudentsID);
        var url = "<?= \yii\helpers\Url::to(['board-members/index']) ?>&school_board_id=" + school_board_id;
        $('#board_member-form-dialog-id').attr('src', url);
        $('#form-add-board-members-id').dialog('open');
    }
</script>
<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;
use kartik\tabs\TabsX;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var app\models\Schools $model
 */
$this->title = $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
ob_start();
?>
<?php
if ($model->logo == NULL) {
    $logo = 'default-no-image/no-logo.png';
} else {
    $logo = $model->logo;
}
?>
<div class="schools-view">
    <?=
    DetailView::widget([
        'model' => $model,
        'condensed' => false,
        'hover' => true,
        'mode' => Yii::$app->request->get('edit') == 't' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
        'panel' => [
            'heading' => '&nbsp',
            'type' => DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            //'school_id',
            [
                'label' => 'School Logo',
                'attribute' => 'logo',
                'value' => $logo,
                'format' => ['image', ['width' => '100', 'height' => '100']],
            ],
            'school_name',
//            'school_type_id',
            [
                'attribute' => 'school_type_id',
                'value' => \app\models\SchoolType::findOne($model->school_type_id)->school_type,
            ],
            'school_motto',
            'school_description:ntext',
            'address',
            'phone_no',
            'e_mail',
            'fax',
//            'logo',
//            'start_year_id',
            [
                'attribute' => 'start_year_id',
                'value' => \app\models\Years::findOne($model->start_year_id)->year,
            ],
//            'region_id',
            [
                'attribute' => 'region_id',
                'value' => \app\models\Regions::findOne($model->region_id)->region,
            ],
//            'district_id',
            [
                'attribute' => 'district_id',
                'value' => \app\models\Districts::findOne($model->district_id)->district,
            ],
//            'ward_id',
            [
                'attribute' => 'ward_id',
                'value' => \app\models\Wards::findOne($model->ward_id)->ward,
            ],
//            'street_id',
            [
                'attribute' => 'street_id',
                'value' => \app\models\StreetsVillage::findOne($model->street_id)->street,
            ],
//            'google_location:ntext',
//            'school_privacy_statement_id',
//            'school_board_id',
//            'school_donor_id',
//            'school_sponsor_id',
//            'school_gallery_id',
//            'school_motto',
//            'headmaster_statement_id',
//            'msg_frm_teacher_id',
//            'msg_frm_parent_id',
//            'inbox_id',
        ],
        'deleteOptions' => [
            'url' => ['delete', 'id' => $model->school_id],
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ],
        'enableEditMode' => false,
    ])
    ?>

</div>

<?php
$details = ob_get_contents();
ob_end_clean();
?>

<?php ob_start(); ?>
<?php
if ($model->google_location != NULL) {
    echo $model->google_location;
} else {
    echo ' <div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
         Not Located yet
   </div>';
}
?>

<?php
$garage_location = ob_get_contents();
ob_end_clean();
?>


<?php ob_start(); ?>

<?php
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider_privacy_statement,
    'filterModel' => $searchModel_privacy_statement,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //'school_privacy_statement _id',
        //'school_id',
        'statement:ntext',
        ['attribute' => 'date_created', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'created_by',
        [
            'attribute' => 'created_by',
            'value' => function ($model) {
                return ucwords(\app\models\Users::findOne($model->created_by)->firstname) . '  ' . ucwords(\app\models\Users::findOne($model->created_by)->middlename) . ' ' . ucwords(\app\models\Users::findOne($model->created_by)->surname) . ' ' . '[' . \app\models\StaffPositions::findOne(\app\models\Users::findOne($model->created_by)->staff_position_id)->position_name . ']';
            }
        ],
//            'is_active', 
        [
            'label' => 'Is Active?',
            'attribute' => 'is_active',
            'vAlign' => 'middle',
            'width' => '5%',
            'value' => function($model) {
                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
            },
            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
        ],
                    
                        [
                'label' => '',
                'value' => function($model) use ($school_id) {
                    if ($model->is_active == '1') {
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-ban-circle"></i> Deactivate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_privacy_statement_id' => $model->school_privacy_statement_id, 'subaction' => 'deactivate', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Deactivate'),
                        ]);
                    } else {
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-ok-circle"></i> Activate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_privacy_statement_id' => $model->school_privacy_statement_id, 'subaction' => 'activate', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    }
                },
                        'format' => 'raw',
                    ],
                                 [
                'label' => '',
                'value' => function($model) use ($school_id) {
                
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-trash"></i> Delete</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_privacy_statement_id' => $model->school_privacy_statement_id, 'subaction' => 'delete', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-privacy-statement/view','id' => $model->school_privacy_statement_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
    ],
    'responsive' => true,
    'hover' => true,
    'condensed' => true,
    'floatHeader' => false,
    'panel' => [
        'heading' => ' ',
        'type' => 'default',
        //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
        'showFooter' => false
    ],
]);
Pjax::end();
?>

<?=
$this->render('_form_privacy_statement', [
    'model' => $model_privacy_statement,
])
?>

<?php
$privacy_statement = ob_get_contents();
ob_end_clean();
?>



<?php
ob_start();
?>



<?php
Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider_headmaster_statement,
    'filterModel' => $searchModel_headmaster_statement,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        //'headmaster_statement_id',
        //'school_id',
        // 'user_id',
        'statement:ntext',
        ['attribute' => 'date_created', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'is_active', 
        [
            'label' => 'Created By',
            'attribute' => 'user_id',
            'value' => function ($model) {
                return ucwords(\app\models\Users::findOne($model->user_id)->firstname) . '  ' . ucwords(\app\models\Users::findOne($model->user_id)->middlename) . ' ' . ucwords(\app\models\Users::findOne($model->user_id)->surname) . ' ' . '[' . \app\models\StaffPositions::findOne(\app\models\Users::findOne($model->user_id)->staff_position_id)->position_name . ']';
            }
        ],
        [
            'label' => 'Is Active?',
            'attribute' => 'is_active',
            'vAlign' => 'middle',
            'width' => '5%',
            'value' => function($model) {
                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
            },
            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
        ],
            
                            
                        [
                'label' => '',
                'value' => function($model) use ($school_id) {
                    if ($model->is_active == '1') {
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-ban-circle"></i> Deactivate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'headmaster_statement_id' => $model->headmaster_statement_id, 'subaction' => 'deactivate', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Deactivate'),
                        ]);
                    } else {
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-ok-circle"></i> Activate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'headmaster_statement_id' => $model->headmaster_statement_id, 'subaction' => 'activate', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    }
                },
                        'format' => 'raw',
                    ],
                                 [
                'label' => '',
                'value' => function($model) use ($school_id) {
                
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-trash"></i> Delete</span>', Yii::$app->urlManager->createUrl(['schools/view', 'headmaster_statement_id' => $model->headmaster_statement_id, 'subaction' => 'delete', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],          
                    
                    
//        [
//            'class' => 'yii\grid\ActionColumn',
//            'buttons' => [
//                'update' => function ($url, $model) {
//                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['headmaster-statement/view', 'id' => $model->headmaster_statement_id, 'edit' => 't']), [
//                                'title' => Yii::t('yii', 'Edit'),
//                    ]);
//                }
//                    ],
//                ],
            ],
            'responsive' => true,
            'hover' => true,
            'condensed' => true,
            'floatHeader' => false,
            'panel' => [
                'heading' => ' ',
                'type' => 'default',
                //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                'showFooter' => false
            ],
        ]);
        Pjax::end();
        ?>


        <?=
        $this->render('_form_headmaster_statement', [
            'model' => $model_headmaster_statement,
        ])
        ?>

        <?php
        $headmaster_statement = ob_get_contents();
        ob_end_clean();
        ?>




        <?php
        ob_start();
        ?>
        <?php
        Pjax::begin();
        echo GridView::widget([
            'dataProvider' => $dataProvider_school_board,
            'filterModel' => $searchModel_school_board,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                //'school_board_id',
                // 'school_id',
                'name',
                'description:ntext',
//            'is_active',
                      [
                    'label' => 'Board Members',
                    'attribute' => 'school_board_id',
                    'value' => function($model) {
                        if ($model->school_board_id == 0) {
                            return '--';
                        } else {
                            return '&nbsp' . ' <span class="badge">' . count(app\models\BoardMembers::find()->where("school_board_id = {$model->school_board_id} ")->all()) . '</span>' . '&nbsp' . '&nbsp' . Html::a('[Add/View]', '#', ['onclick' => "addBoardMembers({$model->school_board_id})", 'style' => 'margin-left: 20px']);
                        }
                    },
                            'format' => 'raw',
                        ],
                [
                    'label' => 'Is Active?',
                    'attribute' => 'is_active',
                    'vAlign' => 'middle',
                    'width' => '5%',
                    'value' => function($model) {
                        return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                    },
                    'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
                ],
          
                            
                            
                                            [
                'label' => '',
                'value' => function($model) use ($school_id) {
                    if ($model->is_active == '1') {
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-ban-circle"></i> Deactivate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_board_id' => $model->school_board_id, 'subaction' => 'deactivate', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Deactivate'),
                        ]);
                    } else {
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-ok-circle"></i> Activate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_board_id' => $model->school_board_id, 'subaction' => 'activate', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    }
                },
                        'format' => 'raw',
                    ],
                                 [
                'label' => '',
                'value' => function($model) use ($school_id) {
                
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-trash"></i> Delete</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_board_id' => $model->school_board_id, 'subaction' => 'delete', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-board/view','id' => $model->school_board_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'condensed' => true,
                    'floatHeader' => false,
                    'panel' => [
                        'heading' => ' ',
                        'type' => 'default',
                        //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                        'showFooter' => false
                    ],
                ]);
                Pjax::end();
                ?>

                <?php
                \yii\jui\Dialog::begin([
                    'clientOptions' => [
                        'modal' => true,
                        'autoOpen' => false,
                        'width' => '1000',
                        'height' => '450',
                    ],
                    'options' => [
                        'title' => $this->title . ' ' . 'Board Members',
                        'id' => 'form-add-board-members-id',
                    ]
                        ]
                );
                ?>

                <iframe src="" width="100%" height="100%" id="board_member-form-dialog-id">
                </iframe>

                <?php
                \yii\jui\Dialog::end();
                ?>

                <?=
                $this->render('_form_school_board', [
                    'model' => $model_school_board,
                ])
                ?>

                <?php
                $school_board = ob_get_contents();
                ob_end_clean();
                ?>

                <?php
                ob_start();
                ?>

                <?php
                Pjax::begin();
                echo GridView::widget([
                    'dataProvider' => $dataProvider_board_statement,
                    'filterModel' => $searchModel_board_statement,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'board_members_statement_id',
//            'school_board_id',
                        [
                            'label' => 'School Board',
                            'attribute' => 'school_board_id',
                            'value' => function ($model) {
                                return app\models\SchoolBoard::findOne($model->school_board_id)->name;
                            }
                        ],
                        'statement:ntext',
                        ['attribute' => 'date_created', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'is_active',
                        [
                            'label' => 'Is Active?',
                            'attribute' => 'is_active',
                            'vAlign' => 'middle',
                            'width' => '5%',
                            'value' => function($model) {
                                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                            },
                            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
                        ],
                                    
                      
                                                                              [
                'label' => '',
                'value' => function($model) use ($school_id) {
                    if ($model->is_active == '1') {
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-ban-circle"></i> Deactivate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'board_members_statement_id' => $model->board_members_statement_id, 'subaction' => 'deactivate_board_statement', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Deactivate'),
                        ]);
                    } else {
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-ok-circle"></i> Activate</span>', Yii::$app->urlManager->createUrl(['schools/view', 'board_members_statement_id' => $model->board_members_statement_id, 'subaction' => 'activate_board_statement', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    }
                },
                        'format' => 'raw',
                    ],
                                 [
                'label' => '',
                'value' => function($model) use ($school_id) {
                
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-trash"></i> Delete</span>', Yii::$app->urlManager->createUrl(['schools/view', 'board_members_statement_id' => $model->board_members_statement_id, 'subaction' => 'delete_board_statement', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],
                                    
                                    
                                    
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['board-members-statement/view','id' => $model->board_members_statement_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'condensed' => true,
                    'floatHeader' => false,
                    'panel' => [
                        'heading' => ' ',
                        'type' => 'default',
                        //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                        'showFooter' => false
                    ],
                ]);
                Pjax::end();
                ?>

                <?=
                $this->render('_form_board_satement', [
                    'model' => $model_board_statement,
                     'school_id'=>$school_id,
                ])
                ?>


                <?php
                $board_statement = ob_get_contents();
                ob_end_clean();
                ?>

                <?php
                ob_start();
                ?>

                <?php
                Pjax::begin();
                echo GridView::widget([
                    'dataProvider' => $dataProvider_sponsors,
                    'filterModel' => $searchModel_sponsors,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'school_sponsor_id',
                        //'school_id',
//            'sponsor_id',
                        [
                            'label' => 'Sponsor',
                            'attribute' => 'sponsor_id',
                            'value' => function ($model) {
                                return app\models\Sponsors::findOne($model->sponsor_id)->sponsor_name;
                            }
                        ],
//            'what_sponsored:ntext',
//            'amount',
                        [
                            'label' => 'Description',
                            'attribute' => 'what_sponsored',
                            'value' => function ($model) {
                                return $model->what_sponsored;
                            }
                        ],
                        [

                            'label' => 'Amount Sponsored',
                            'attribute' => 'amount',
                            'hAlign' => 'right',
                            'value' => function ($model) {
                                return $model->amount;
                            },
                            'format' => 'raw',
                            'format' => ['decimal', 2]
                        ],
//            
//            ['attribute'=>'date_created','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
                        ['attribute' => 'date_sponsored', 'format' => ['date', (isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']],
//            'is_visible', 
                        [
                            'label' => 'Is Visible?',
                            'attribute' => 'is_visible',
                            'vAlign' => 'middle',
                            'width' => '5%',
                            'value' => function($model) {
                                return $model->is_visible == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                            },
                            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
                        ],
                                    
                                    
                                                                                                        [
                'label' => '',
                'value' => function($model) use ($school_id) {
                    if ($model->is_visible == '1') {
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-ban-circle"></i> Set Unvisible</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_sponsor_id' => $model->school_sponsor_id, 'subaction' => 'deactivate_sponsor', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Set Notvisible'),
                        ]);
                    } else {
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-ok-circle"></i> Set Visible</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_sponsor_id' => $model->school_sponsor_id, 'subaction' => 'activate_sponsor', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Set Visible'),
                        ]);
                    }
                },
                        'format' => 'raw',
                    ],
                                 [
                'label' => '',
                'value' => function($model) use ($school_id) {
                
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-trash"></i> Delete</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_sponsor_id' => $model->school_sponsor_id, 'subaction' => 'delete_sponsor', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],
                                    
                                    
                                    
                                    
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'buttons' => [
//                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-sponsors/view','id' => $model->school_sponsor_id,'edit'=>'t']), [
//                                                    'title' => Yii::t('yii', 'Edit'),
//                                                  ]);}
//
//                ],
//            ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'condensed' => true,
                    'floatHeader' => false,
                    'panel' => [
                        'heading' => ' ',
                        'type' => 'default',
                        //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                        'showFooter' => false
                    ],
                ]);
                Pjax::end();
                ?>

                <?=
                $this->render('_form_sponsors', [
                    'model' => $model_sponsors,
                ])
                ?>


                <?php
                $sponsors = ob_get_contents();
                ob_end_clean();
                ?>


                <?php
                ob_start();
                ?>


                <?php
                Pjax::begin();
                echo GridView::widget([
                    'dataProvider' => $dataProvider_donors,
                    'filterModel' => $searchModel_donors,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'school_donor_id',
                        //'school_id',
//                        'donor_id',
                        [
                            'label' => 'Donor',
                            'attribute' => 'donor_id',
                            'value' => function ($model) {
                                return app\models\Donors::findOne($model->donor_id)->donor_name;
                            }
                        ],
//                        'what_donated:ntext',
                        [
                            'label' => 'Description',
                            'attribute' => 'what_donated',
                            'value' => function ($model) {
                                return $model->what_donated;
                            }
                        ],
//                        'amount',
                        [

                            'label' => 'Amount Donated',
                            'attribute' => 'amount',
                            'hAlign' => 'right',
                            'value' => function ($model) {
                                return $model->amount;
                            },
                            'format' => 'raw',
                            'format' => ['decimal', 2]
                        ],
//            ['attribute'=>'date_created','format'=>['datetime',(isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']], 
                        //['attribute'=>'date_donated','format'=>['date',(isset(Yii::$app->modules['datecontrol']['displaySettings']['date'])) ? Yii::$app->modules['datecontrol']['displaySettings']['date'] : 'd-m-Y']], 
//                        'is_visible',
                        [
                            'label' => 'Is Visible?',
                            'attribute' => 'is_visible',
                            'vAlign' => 'middle',
                            'width' => '5%',
                            'value' => function($model) {
                                return $model->is_visible == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                            },
                            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
                        ],
                                    
                                               
                                                                                                        [
                'label' => '',
                'value' => function($model) use ($school_id) {
                    if ($model->is_visible == '1') {
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-ban-circle"></i> Set Unvisible</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_donor_id' => $model->school_donor_id, 'subaction' => 'deactivate_donor', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Set Notvisible'),
                        ]);
                    } else {
                        return Html::a('<span class=" label label-primary"><i class = "glyphicon glyphicon-ok-circle"></i> Set Visible</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_donor_id' => $model->school_donor_id, 'subaction' => 'activate_donor', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Set Visible'),
                        ]);
                    }
                },
                        'format' => 'raw',
                    ],
                                 [
                'label' => '',
                'value' => function($model) use ($school_id) {
                
                        return Html::a('<span class=" label label-danger"><i class = "glyphicon glyphicon-trash"></i> Delete</span>', Yii::$app->urlManager->createUrl(['schools/view', 'school_donor_id' => $model->school_donor_id, 'subaction' => 'delete_donor', 'id'=>$school_id]), [
                                    'title' => Yii::t('yii', 'Activate'),
                        ]);
                    
                },
                        'format' => 'raw',
                    ],  
                                    
                                    
                                    
//                        [
//                            'class' => 'yii\grid\ActionColumn',
//                            'buttons' => [
//                                'update' => function ($url, $model) {
//                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['school-donors/view', 'id' => $model->school_donor_id, 'edit' => 't']), [
//                                                'title' => Yii::t('yii', 'Edit'),
//                                    ]);
//                                }
//                                    ],
//                                ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'condensed' => true,
                    'floatHeader' => false,
                    'panel' => [
                        'heading' => ' ',
                        'type' => 'default',
                        //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                        'showFooter' => false
                    ],
                ]);
                Pjax::end();
                ?>

                <?=
                $this->render('_form_donors', [
                    'model' => $model_donors,
                ])
                ?>


                <?php
                $donors = ob_get_contents();
                ob_end_clean();
                ?>

                <?php
                echo TabsX::widget([
                    'items' => [
                        [
                            'label' => ' ' . 'Details',
                            'content' => $details,
                            //'active' => true,
                            'options' => ['id' => 'Details-tab'],
                            'active' => ($activeTab == 'Details-tab'),
                        ],
                        [
                            'label' => ' ' . 'Privacy Statement',
                            'content' => $privacy_statement,
                            //'active' => false,
                            'options' => ['id' => 'privacy_statement-tab'],
                            'active' => ($activeTab == 'privacy_statement-tab'),
                        ],
                        [
                            'label' => ' ' . 'Headmaster Statement',
                            'content' => $headmaster_statement,
                            //'active' => false,
                            'options' => ['id' => 'headmaster_statement-tab'],
                            'active' => ($activeTab == 'headmaster_statement-tab'),
                        ],
                        [
                            'label' => ' ' . 'School Board',
                            'content' => $school_board,
                            //'active' => false,
                            'options' => ['id' => 'school_board-tab'],
                            'active' => ($activeTab == 'school_board-tab'),
                        ],
                        [
                            'label' => ' ' . 'Board Statement',
                            'content' => $board_statement,
                            //'active' => false,
                            'options' => ['id' => 'board_statement-tab'],
                            'active' => ($activeTab == 'board_statement-tab'),
                        ],
                        [
                            'label' => ' ' . 'Sponsors',
                            'content' => $sponsors,
                            //'active' => false,
                            'options' => ['id' => 'sponsors-tab'],
                            'active' => ($activeTab == 'sponsors-tab'),
                        ],
                        [
                            'label' => ' ' . 'Donors',
                            'content' => $donors,
                            //'active' => false,
                            'options' => ['id' => 'donors-tab'],
                            'active' => ($activeTab == 'donors-tab'),
                        ],
                        [
                            'label' => ' ' . 'Location',
                            'content' => $garage_location,
                            //'active' => false,
                            'options' => ['id' => 'location-tab'],
                            'active' => ($activeTab == 'location-tab'),
                        ],
                    ],
                    'bordered' => true,
                ]);
                ?>
