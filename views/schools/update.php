<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Schools $model
 */

$this->title = 'Update School: ' . ' ' . $model->school_name;
$this->params['breadcrumbs'][] = ['label' => 'Schools', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->school_name, 'url' => ['view', 'id' => $model->school_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="schools-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
