<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\BoardMembersStatement $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="board-members-statement-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

//'board_members_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Board Members ID...']], 

            'school_board_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'School Board',
                'options' => [

                    'data' => yii\helpers\ArrayHelper::map(\app\models\SchoolBoard::find()->where("school_id = {$school_id}")->all(), 'school_board_id', 'name'),
                    'options' => [
                        'prompt' => '',
                    ],
                ],
            ],
            'statement' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Board Statement...', 'rows' => 6]],
//'is_active'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Is Active...']], 
//'date_created'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
        ]
    ]);
    echo Html::submitButton(Yii::t('app', 'Add') , ['class' => 'btn btn-success']);
    ActiveForm::end();
    ?>

</div>
