<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use kartik\file\FileInput;

/**
 * @var yii\web\View $this
 * @var app\models\Schools $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="schools-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL, 'method' => 'post', 'options' => ['enctype' => 'multipart/form-data']]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 4,
        'attributes' => [

//'school_type_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Type ID...']], 
            //    'region_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Region ID...']],
//            'district_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter District ID...']],
//            'ward_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ward ID...']],
//            'street_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Street ID...']],
            'region_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Region',
                'options' => [

                    'data' => yii\helpers\ArrayHelper::map(\app\models\Regions::find()->all(), 'region_id', 'region'),
                    'options' => [
                        'prompt' => '',
                    //'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
                'columnOptions' => ['id' => 'region_id'],
            ],
            'district_id' => [
                'type' => Form::INPUT_WIDGET,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'label' => 'District',
                'widgetClass' => DepDrop::className(),
                'options' => [
                    'data' => [$model->district_id => \app\models\Districts::findOne($model->district_id)->district],
                    'disabled' => $model->isNewrecord ? false : true,
                    'pluginOptions' => [
                        'depends' => ['schools-region_id'],
                        'placeholder' => 'Select',
                        'url' => Url::to(['/schools/districts']),
                    ],
                ],
                'columnOptions' => ['id' => 'district_id'],
            ],
            // 'district_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ward ID...']],
            //'ward_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Ward ID...']],
            'ward_id' => [
                'type' => Form::INPUT_WIDGET,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'label' => 'Ward',
                'widgetClass' => DepDrop::className(),
                'options' => [
                    'data' => [$model->ward_id => \app\models\Wards::findOne($model->ward_id)->ward],
                    'disabled' => $model->isNewrecord ? false : true,
                    'pluginOptions' => [
                        'depends' => ['schools-district_id'],
                        'placeholder' => 'Select',
                        'url' => Url::to(['/schools/wards']),
                    ],
                ],
                'columnOptions' => ['id' => 'ward_id'],
            ],
            //'street_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Street ID...']],
            'street_id' => [
                'type' => Form::INPUT_WIDGET,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'label' => 'Street',
                'widgetClass' => DepDrop::className(),
                'options' => [
                    'data' => [$model->street_id => \app\models\StreetsVillage::findOne($model->street_id)->street],
                    'disabled' => $model->isNewrecord ? false : true,
                    'pluginOptions' => [
                        'depends' => ['schools-ward_id'],
                        'placeholder' => 'Select',
                        'url' => Url::to(['/schools/streets']),
                    ],
                ],
                'columnOptions' => ['id' => 'street_id'],
            ],
            'school_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter School Name...', 'maxlength' => 300]],
              'school_type_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'School Type',
                'options' => [

                    'data' => yii\helpers\ArrayHelper::map(\app\models\SchoolType::find()->all(), 'school_type_id', 'school_type'),
                      'disabled' => $model->isNewrecord ? false : true,
                    'options' => [
                        'prompt' => '',
                    //'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
               
            ],
            'address' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Address...', 'maxlength' => 250]],
            'phone_no' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Phone No...', 'maxlength' => 250]],
            'e_mail' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter E Mail...', 'maxlength' => 250]],
            'fax' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Fax...', 'maxlength' => 250]],
//'logo'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Logo...', 'maxlength'=>250]], 
            //'start_year_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Start Year ID...']],
              'start_year_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Start Year',
                'options' => [

                    'data' => yii\helpers\ArrayHelper::map(\app\models\Years::find()->all(), 'year_id', 'year'),
                    'options' => [
                        'prompt' => '',
                    //'disabled' => $model->isNewrecord ? false : true,
                    ],
                ],
               
            ],
         'school_motto'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Motto ...']]
//'google_location'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter Google Location...','rows'=> 6]], 
//'school_privacy_statement_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Privacy Statement ID...']], 
//
//'school_board_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Board ID...']], 
//
//'school_donor_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Donor ID...']], 
//
//'school_sponsor_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Sponsor ID...']], 
//
//'school_gallery_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Gallery ID...']], 
//
//'school_motto_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School Motto ID...']], 
//
//'headmaster_statement_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Headmaster Statement ID...']], 
//
//'msg_frm_teacher_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Msg Frm Teacher ID...']], 
//
//'msg_frm_parent_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Msg Frm Parent ID...']], 
//
//'inbox_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Inbox ID...']], 
        ]
    ]);

    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [
            'school_description' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter School Description...', 'rows' => 6,]],
            'google_location' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter Google Location...', 'rows' => 6,]],
//            'logo' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Logo...', 'maxlength' => 250]],
              'image' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => FileInput::className(),
                'label' => $model->isNewRecord ? "School Logo" : " School Logo (Will replace existing one)",
                ['options' => ['accept' => 'image/*']],
                'pluginOptions' => ['previewFileType' => 'image'],
            ],
        ]
    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end();
    ?>

</div>
