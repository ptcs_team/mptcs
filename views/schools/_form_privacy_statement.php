<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolPrivacyStatement $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-privacy-statement-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [

//'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 

            'statement' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter School Privacy Statement...', 'rows' => 6]],
//'created_by'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Created By...']], 
//'is_active'=>['type'=> Form::INPUT_CHECKBOX, 'options'=>['placeholder'=>'Enter Is Active...']], 
//'date_created'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
        ]
    ]);
    echo Html::submitButton(Yii::t('app', 'Add') , ['class' => 'btn btn-success']);
    ActiveForm::end();
    ?>

</div>
