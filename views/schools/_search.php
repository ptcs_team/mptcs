<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="schools-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'school_type_id') ?>

    <?= $form->field($model, 'region_id') ?>

    <?= $form->field($model, 'district_id') ?>

    <?= $form->field($model, 'ward_id') ?>

    <?php // echo $form->field($model, 'street_id') ?>

    <?php // echo $form->field($model, 'school_name') ?>

    <?php // echo $form->field($model, 'school_description') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'phone_no') ?>

    <?php // echo $form->field($model, 'e_mail') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'start_year_id') ?>

    <?php // echo $form->field($model, 'google_location') ?>

    <?php // echo $form->field($model, 'school_privacy_statement_id') ?>

    <?php // echo $form->field($model, 'school_board_id') ?>

    <?php // echo $form->field($model, 'school_donor_id') ?>

    <?php // echo $form->field($model, 'school_sponsor_id') ?>

    <?php // echo $form->field($model, 'school_gallery_id') ?>

    <?php // echo $form->field($model, 'school_motto_id') ?>

    <?php // echo $form->field($model, 'headmaster_statement_id') ?>

    <?php // echo $form->field($model, 'msg_frm_teacher_id') ?>

    <?php // echo $form->field($model, 'msg_frm_parent_id') ?>

    <?php // echo $form->field($model, 'inbox_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
