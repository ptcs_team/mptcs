<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\SchoolSponsors $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="school-sponsors-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 3,
        'attributes' => [

//'school_id'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter School ID...']], 
     'sponsor_id' => ['type' => Form::INPUT_WIDGET,
                'widgetClass' => \kartik\select2\Select2::className(),
                'label' => 'Sponsor',
                'options' => [

                    'data' => yii\helpers\ArrayHelper::map(\app\models\Sponsors::find()->all(), 'sponsor_id', 'sponsor_name'),
                    'options' => [
                        'prompt' => '',
                    ],
                ],
            ],
          //  'sponsor_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Sponsor ID...']],
//'what_sponsored'=>['type'=> Form::INPUT_TEXTAREA, 'options'=>['placeholder'=>'Enter What Sponsored...','rows'=> 6]], 
            'amount' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Enter Amount...']],
            'date_sponsored' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => kartik\date\DatePicker::classname(), 'options' => [ ]],
//'is_visible'=>['type'=> Form::INPUT_CHECKBOX, 'options'=>['placeholder'=>'Enter Is Visible...']], 
//'date_created'=>['type'=> Form::INPUT_WIDGET, 'widgetClass'=>DateControl::classname(),'options'=>['type'=>DateControl::FORMAT_DATETIME]], 
        ]
    ]);


    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([

        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
        'what_sponsored' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Enter What Sponsored...', 'rows' => 6]],
        ]
    ]);
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_VERTICAL]);
    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'is_visible' => ['type' => Form::INPUT_CHECKBOX, 'options' => ['placeholder' => 'Enter Is Visible...']],
        ]
    ]);
    echo Html::submitButton(Yii::t('app', 'Add') , ['class' => 'btn btn-success']);
    ActiveForm::end();
    ?>

</div>
