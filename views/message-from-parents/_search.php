<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\MessageFromParentsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="message-from-parents-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'msg_frm_parent_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'sent_by') ?>

    <?= $form->field($model, 'receiver') ?>

    <?= $form->field($model, 'message') ?>

    <?php // echo $form->field($model, 'date_sent') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
