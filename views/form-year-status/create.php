<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\FormYearStatus $model
 */

$this->title = 'Create Form Year Status';
$this->params['breadcrumbs'][] = ['label' => 'Form Year Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-year-status-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
