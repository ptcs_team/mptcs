<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\BoardMembersStatement $model
 */

$this->title = 'Update Board Members Statement: ' . ' ' . $model->board_members_statement_id;
$this->params['breadcrumbs'][] = ['label' => 'Board Members Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->board_members_statement_id, 'url' => ['view', 'id' => $model->board_members_statement_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="board-members-statement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
