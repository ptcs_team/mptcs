<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\BoardMembersStatement $model
 */

$this->title = 'Create Board Members Statement';
$this->params['breadcrumbs'][] = ['label' => 'Board Members Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-members-statement-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
