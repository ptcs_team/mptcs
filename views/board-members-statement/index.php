<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\BoardMembersStatementSearch $searchModel
 */

$this->title = 'Board Members Statements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="board-members-statement-index">
    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'board_members_statement_id',
//            'school_board_id',
            [
                            'label' => 'School Board',
                            'attribute' => 'school_board_id',
                            'value' => function ($model) {
                                return app\models\SchoolBoard::findOne($model->school_board_id)->name;
                            }
                        ],
                        'statement:ntext',
                        ['attribute' => 'date_created', 'format' => ['datetime', (isset(Yii::$app->modules['datecontrol']['displaySettings']['datetime'])) ? Yii::$app->modules['datecontrol']['displaySettings']['datetime'] : 'd-m-Y H:i:s A']],
//            'is_active',
                        [
                            'label' => 'Is Active?',
                            'attribute' => 'is_active',
                            'vAlign' => 'middle',
                            'width' => '5%',
                            'value' => function($model) {
                                return $model->is_active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>";
                            },
                            'format' => 'raw',
//            'filterType' => GridView::FILTER_SELECT2,
//            'filter' => ['1' => 'Visible', '0' => 'Invisible'],
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['allowClear' => true],
//            ],
//            'filterInputOptions' => ['placeholder' => 'Search...'],
//            'format' => 'raw'
                        ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'buttons' => [
                'update' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['board-members-statement/view','id' => $model->board_members_statement_id,'edit'=>'t']), [
                                                    'title' => Yii::t('yii', 'Edit'),
                                                  ]);}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>'&nbsp',
            'type'=>'default',
            //'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),                                                                                                                                                          'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
