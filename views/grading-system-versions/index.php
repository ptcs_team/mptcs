<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\GradingSystemVersionsSearch $searchModel
 */
$this->title = 'Grade Score System';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grading-system-versions-index">
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
       'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'grading_system_version_id',
                'description',
               [
                'label' => 'School Type',
                'attribute' => 'school_type_id',
                'value' => function ($model) {
                    return \app\models\SchoolType::findOne($model->school_type_id)->school_type;
                }
            ],
     
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl(['grading-system-versions/view', 'id' => $model->grading_system_version_id, 'edit' => 't']), [
                                    'title' => Yii::t('yii', 'Edit'),
                        ]);
                    }
                        ],
                        'urlCreator' => function($action, $model) {
                    if ($action == 'view') {
                        return \yii\helpers\Url::to(['grading-system/index', 'grading_system_version_id' => $model->grading_system_version_id]);
                    }
                }
                    ],
                ],
                'responsive' => true,
                'hover' => true,
                'condensed' => true,
                'floatHeader' => false,
                'panel' => [
                    //'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Html::encode($this->title).' </h3>',
                    'type' => 'default',
                    'before' => Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']), 'after' => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
                    'showFooter' => false
                ],
            ]);
            // echo Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']);
            ?>



</div>
