<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystemVersions $model
 */

$this->title = 'Update Grading System Versions: ' . ' ' . $model->grading_system_version_id;
$this->params['breadcrumbs'][] = ['label' => 'Grading System Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->grading_system_version_id, 'url' => ['view', 'id' => $model->grading_system_version_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="grading-system-versions-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
