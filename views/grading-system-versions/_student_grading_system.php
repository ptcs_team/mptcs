<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\StudentYearOfStudySearch;
use app\models\GradingSystemVersionsSearch;
use kartik\tabs\TabsX;
use app\models\GradingSystemSearch;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\StudentYearOfStudySearch $searchModel
 * 
 * 
 */

$stdnt_year_of_study_model = \app\models\StudentYearOfStudy::find()->where("user_id = {$user_id}")->one();
$py_id = $stdnt_year_of_study_model->py_id;
$programme_year_model = \app\models\ProgrammeYear::find()->where("py_id = {$py_id}")->one();
$pintake_id = $programme_year_model->pintake_id;
$pintake_model = app\models\ProgrammeIntakes::findOne($pintake_id);
$pgm_id = $pintake_model->programme_id;
$programmes_model = \app\models\Programmes::find()->where("programme_id = {$pgm_id}")->one();
$pgm_name = $programmes_model->programme_name;
$pgm_code =$programmes_model->programme_code; 

$this->title = ' Programme Name: ' .$pgm_name.' '.'('.$pgm_code.')';
$this->params['breadcrumbs'][] = "Grading System";
?>
<div class="student-year-of-study-index">
    <?php
    $model_stdnt_yr_of_studys = \app\models\StudentYearOfStudy::find()->where("user_id = {$user_id}")->all();


    $array_stdnt_yr_of_studys = [];

    foreach ($model_stdnt_yr_of_studys as $model_stdnt_yr_of_study) {

        $academic_year = app\models\AcademicYears::findOne(app\models\ProgrammeYear::findOne($model_stdnt_yr_of_study->py_id)->academic_year_id)->academic_year;

        $model_programme_year = \app\models\ProgrammeYear::find()->where("py_id = {$model_stdnt_yr_of_study->py_id}")->one();
        $searchModel = new GradingSystemSearch;
        $condition = "grading_system_version_id = {$model_programme_year->grading_system_version_id}";
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), $condition);
        $grading_system_description = \app\models\GradingSystemVersions::findOne($model_programme_year->grading_system_version_id)->description;

        if ($model_stdnt_yr_of_study->year_of_study == 1) {
            $year_of_study = 'First year';
        } elseif ($model_stdnt_yr_of_study->year_of_study == 2) {
            $year_of_study = 'Second year';
        } elseif ($model_stdnt_yr_of_study->year_of_study == 3) {
            $year_of_study = 'Second year';
        } elseif ($model_stdnt_yr_of_study->year_of_study == 4) {
            $year_of_study = 'Fourth year';
        }



        $array_stdnt_yr_of_studys_items = [
            'label' => $year_of_study . ':' . ' ' . $academic_year . '<br> ' . $grading_system_description,
            'content' => $this->render('studentgs', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]),
        ];

        array_push($array_stdnt_yr_of_studys, $array_stdnt_yr_of_studys_items);
    }


    echo TabsX::widget([
        'items' => $array_stdnt_yr_of_studys,
        'position' => TabsX::POS_ABOVE,
        'bordered' => true,
        'encodeLabels' => false
    ]);
    ?>
</div>
