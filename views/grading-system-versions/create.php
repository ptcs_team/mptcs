<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\GradingSystemVersions $model
 */

$this->title = 'Create Grading System Version';
$this->params['breadcrumbs'][] = ['label' => 'Grading System Versions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grading-system-versions-create">
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
