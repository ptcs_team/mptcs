<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StreetsVillage $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="streets-village-form">

    <?php $form = ActiveForm::begin(['type'=>ActiveForm::TYPE_VERTICAL]); echo Form::widget([

    'model' => $model,
    'form' => $form,
    'columns' => 3,
    'attributes' => [
        
 'street'=>['type'=> Form::INPUT_TEXT, 'options'=>['placeholder'=>'Enter Street...', 'maxlength'=>100]], 
        
'ward_id'=>['type'=> Form::INPUT_DROPDOWN_LIST, 
    'items'=>  yii\helpers\ArrayHelper::map(\app\models\Wards::find()->all(), 'ward_id','ward'),
    'options'=>['prompt'=>' ']], 



'is_active'=>['type'=> Form::INPUT_CHECKBOX, 'options'=>['placeholder'=>'Enter Is Active...']], 

    ]


    ]);
    echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
    ActiveForm::end(); ?>

</div>
