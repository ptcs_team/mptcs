<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\StreetsVillage $model
 */

$this->title = 'Add Street/Village';
$this->params['breadcrumbs'][] = ['label' => 'Streets Villages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="streets-village-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
