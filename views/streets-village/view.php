<?php

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\datecontrol\DateControl;

/**
 * @var yii\web\View $this
 * @var app\models\StreetsVillage $model
 */

$this->title = $model->street;
$this->params['breadcrumbs'][] = ['label' => 'Streets Villages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="streets-village-view">
    <?= DetailView::widget([
            'model' => $model,
            'condensed'=>false,
            'hover'=>true,
            'mode'=>Yii::$app->request->get('edit')=='t' ? DetailView::MODE_EDIT : DetailView::MODE_VIEW,
            'panel'=>[
            'heading'=>'&nbsp',
            'type'=>DetailView::TYPE_DEFAULT,
        ],
        'attributes' => [
            'street_id',
            'ward_id',
            'street',
            'is_active',
        ],
        'deleteOptions'=>[
        'url'=>['delete', 'id' => $model->street_id],
        'data'=>[
        'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?'),
        'method'=>'post',
        ],
        ],
        'enableEditMode'=>true,
    ]) ?>

</div>
