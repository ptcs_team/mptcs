<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\HeadmasterStatement $model
 */

$this->title = 'Update Headmaster Statement: ' . ' ' . $model->headmaster_statement_id;
$this->params['breadcrumbs'][] = ['label' => 'Headmaster Statements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->headmaster_statement_id, 'url' => ['view', 'id' => $model->headmaster_statement_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="headmaster-statement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
