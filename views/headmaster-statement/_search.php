<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\HeadmasterStatementSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="headmaster-statement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'headmaster_statement_id') ?>

    <?= $form->field($model, 'school_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'statement') ?>

    <?= $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
